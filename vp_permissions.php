<?php

require_once 'vp_articles.php';

class VPPermissions
{

    private $permissionList = null;

    private function __construct()
    {
    }

    public static function FromUserID($userID): VPPermissions
    {
        $userID = VPDatabaseConn::EscapeSQLString($userID);
        $sql = "SELECT `Permissions` FROM `vp_users` WHERE `ID` = '$userID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);
        if ($result == null) {
            return null;
        } else {
            return VPPermissions::FromPermissionList(utf8_encode($result['Permissions']));
        }
    }

    public static function FromUserData($userData): VPPermissions
    {
        return VPPermissions::FromPermissionList($userData->Permissions);
    }

    public static function FromPermissionList($permissionList): VPPermissions
    {
        $permissionList = trim($permissionList);

        $perm = new VPPermissions;
        if (strlen($permissionList) == 0) {
            $perm->permissionList = array();
        } else {
            $perm->permissionList = explode(";", $permissionList);
        }

        return $perm;
    }

    public static function NoPermissionMessage()
    {
        echo '<p><b style="color: red;">' . VPLocale::Get("authentication.no-permission") . '</b></p>';
    }

    public function CheckPermission($permission)
    {
        // the required permission as a permission that every user has
        if (strlen($permission) > 2 && substr($permission, 0, 2) == "u_") return true;

        $permission = strtolower($permission);
        foreach ($this->permissionList as $p) {
            $p = strtolower($p);

            // admin can do everything
            if ($p == "_admin") return true;

            // the exact permission is there
            if ($p == $permission) return true;

            // the user has every moderator permission
            if ($p == "_moderator") {
                // the required permission as a moderator permission
                if (strlen($permission) > 2 && substr($permission, 0, 2) == "m_") return true;
                if (strlen($permission) > 2 && substr($permission, 0, 2) == "a_") return true;
            }

            // the user has every author permission
            if ($p == "_author") {
                // the required permission as an author permission
                if (strlen($permission) > 2 && substr($permission, 0, 2) == "a_") return true;
            }
        }
        return false;
    }

    private function EchoAdminPermissions()
    {
        $this->EchoPermission("x_addUser");
        $this->EchoPermission("x_viewUserList");
        $this->EchoPermission("x_editAllUsers");
        $this->EchoPermission("x_viewLogbook");
        $this->EchoPermission("x_manageAPIKeys");
        $this->EchoPermission("x_deleteMessages");
        $this->EchoPermission("x_querySQL");
    }

    private function EchoModeratorPermissions()
    {
        $this->EchoPermission("m_editAllArticles");
        $this->EchoPermission("m_editAllPhotos");
        $this->EchoPermission("m_postAllTags");
        $this->EchoPermission("m_createTags");
        $this->EchoPermission("m_editAllComments");
        $this->EchoPermission("m_readInbox");
        $this->EchoPermission("m_createCalendars");
        $this->EchoPermission("m_editAllCalendars");
        $this->EchoPermission("m_editAllGalleries");
    }

    private function EchoAuthorPermissions()
    {
        $this->EchoPermission("a_createArticles");
        $this->EchoPermission("a_uploadPhotos");
        $this->EchoPermission("a_viewCommentList");
        $this->EchoPermission("a_viewCalendarList");
        $this->EchoPermission("a_createGalleries");
    }

    private function EchoUserPermissions()
    {
        $this->EchoPermission("u_viewArticleList");
        $this->EchoPermission("u_editOwnArticles");
        $this->EchoPermission("u_viewPhotoList");
        $this->EchoPermission("u_editOwnPhotos");
        $this->EchoPermission("u_writeComments");
        $this->EchoPermission("u_editOwnComments");
        $this->EchoPermission("u_editOwnCalendars");
        $this->EchoPermission("u_viewGalleryList");
        $this->EchoPermission("u_editOwnGalleries");
    }

    public function EchoPermissionTable()
    {
        echo '<table border="1"><tr><th>' . VPLocale::Get("permissions.permission") . '</th><th>' . VPLocale::Get("permissions.description") . '</th></tr>';
        for ($i = 0; $i < sizeof($this->permissionList); $i++) {
            $p = $this->permissionList[$i];
            if (strlen($p) == 0) continue;

            if ($p == "_admin") {
                echo '<tr><td><b>' . $p . '</b></td><td>' . VPLocale::Get("permissions.user-level._admin") . '<br><br>';
                echo '<table border="1"><tr><th>' . VPLocale::Get("permissions.permission") . '</th><th>' . VPLocale::Get("permissions.description") . '</th></tr>';
                $this->EchoAdminPermissions();
                $this->EchoModeratorPermissions();
                $this->EchoAuthorPermissions();
                echo '</table>';
            } else if ($p == "_moderator") {
                echo '<tr><td><b>' . $p . '</b></td><td>' . VPLocale::Get("permissions.user-level._moderator") . '<br><br>';
                echo '<table border="1"><tr><th>' . VPLocale::Get("permissions.permission") . '</th><th>' . VPLocale::Get("permissions.description") . '</th></tr>';
                $this->EchoModeratorPermissions();
                $this->EchoAuthorPermissions();
                echo '</table>';
            } else if ($p == "_author") {
                echo '<tr><td><b>' . $p . '</b></td><td>' . VPLocale::Get("permissions.user-level._author") . '<br><br>';
                echo '<table border="1"><tr><th>' . VPLocale::Get("permissions.permission") . '</th><th>' . VPLocale::Get("permissions.description") . '</th></tr>';
                $this->EchoAuthorPermissions();
                echo '</table>';
            } else {
                echo '<tr><td>' . $p . '</td><td>';
                echo $this->GetPermissionDescription($p);
            }
            echo '</td></tr>';
        }

        $this->EchoUserPermissions();

        echo '<tr><td></td><td>' . VPLocale::Get("permissions.editOwnProfile") . '</td></tr>';
        echo '<tr><td></td><td>' . VPLocale::Get("permissions.editOwnPassword") . '</td></tr>';

        echo '</table>';
    }

    private function EchoPermission($permission)
    {
        echo '<tr><td>' . $permission . '</td><td>' . $this->GetPermissionDescription($permission) . '</td></tr>';
    }

    private function StartsWith($a, $b)
    {
        return strlen($a) >= strlen($b) && substr($a, 0, strlen($b)) == $b;
    }

    private function GetPermissionDescription($p)
    {
        if ($this->StartsWith($p, "editUser_")) return VPLocale::Get("permissions.editUser", array(substr($p, 9)));
        if ($this->StartsWith($p, "editPhoto_")) return VPLocale::Get("permissions.editPhoto", array(substr($p, 10)));
        if ($this->StartsWith($p, "editArticle_")) return VPLocale::Get("permissions.editArticle", array(substr($p, 12)));
        if ($this->StartsWith($p, "editGallery_")) return VPLocale::Get("permissions.editGallery", array(substr($p, 12)));
        if ($this->StartsWith($p, "editCalendar_")) return VPLocale::Get("permissions.editCalendar", array(substr($p, 13)));
        if ($this->StartsWith($p, "postTag_")) return VPLocale::Get("permissions.postTag", array(VPArticle::GetTagName(substr($p, 8))));

        return VPLocale::Get("permissions.$p");
    }

    public function CanViewArticleList()
    {
        return $this->CheckPermission("u_viewArticleList");
    }

    public function CanCreateArticles()
    {
        return $this->CheckPermission("a_createArticles");
    }

    public function CanEditArticle($articleID)
    {
        if ($this->CheckPermission("m_editAllArticles")) {
            // moderators can edit all articles
            return true;
        }

        if ($this->CheckPermission("editArticle_$articleID")) {
            // user has explicit permission to edit this article
            return true;
        }

        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `Author` FROM `vp_articles` WHERE `ID` = '$articleID'");
        if ($result == null) {
            // article does not exist
            return false;
        } else {
            $ownerID = $result['Author'];
            if ($ownerID == VPLogin::LoggedInUserID()) {
                // the user is the owner of this article and has the permission to edit their own articles
                return $this->CheckPermission("u_editOwnArticles");
            } else {
                // the user is not the owner of this article and may not edit this article
                return false;
            }
        }
    }

    public function CanUseArticleTag($tagID)
    {
        if ($this->CheckPermission("m_postAllTags")) {
            // moderators can use all tags
            return true;
        }

        if ($this->CheckPermission("postTag_$tagID")) {
            // user has explicit permission to use this tag
            return true;
        }
    }

    public function CanCreateArticleTags()
    {
        return $this->CheckPermission("m_createTags");
    }

    public function CanViewPhotoList()
    {
        return $this->CheckPermission("u_viewPhotoList");
    }

    public function CanUploadPhotos()
    {
        return $this->CheckPermission("a_uploadPhotos");
    }

    public function CanEditPhoto($imageID)
    {
        if ($this->CheckPermission("m_editAllPhotos")) {
            // moderators can edit all photos
            return true;
        }

        if ($this->CheckPermission("editPhoto_$imageID")) {
            // user has explicit permission to edit this photo
            return true;
        }

        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `Owner` FROM `vp_images` WHERE `ID` = '$imageID'");
        if ($result == null) {
            // image does not exist
            return false;
        } else {
            $ownerID = $result['Owner'];
            if ($ownerID == VPLogin::LoggedInUserID()) {
                // the user is the owner of this photo and has the permission to edit their own photos
                return $this->CheckPermission("u_editOwnPhotos");
            } else {
                // the user is not the owner of this photo and may not edit this photo
                return false;
            }
        }
    }

    public function CanViewUserList()
    {
        return $this->CheckPermission("x_viewUserList");
    }

    public function CanAddUser()
    {
        return $this->CheckPermission("x_addUser");
    }

    public function CanEditUser($userID)
    {
        // admins can edit all users but every admin automatically has
        // every permission possible including the one below
        return $this->CheckPermission("x_editAllUsers") || $this->CheckPermission("editUser_$userID");
    }

    public function CanViewCommentList()
    {
        return $this->CheckPermission("a_viewCommentList");
    }

    public function CanCreateComment()
    {
        return $this->CheckPermission("u_writeComments");
    }

    public function CanEditComment($commentID)
    {
        if ($this->CheckPermission("m_editAllComments")) {
            // moderator can edit all comments
            return true;
        }

        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `UserID` FROM `vp_comments` WHERE `ID` = '$commentID'");
        if ($result == null) {
            // comment does not exist
            return false;
        } else {
            $ownerID = $result['UserID'];
            if ($ownerID == VPLogin::LoggedInUserID()) {
                // the user is the owner of this comment and has the permission to edit their own comments
                return $this->CheckPermission("u_editOwnComments");
            } else {
                // the user is not the owner of this comment and may not edit this photo
                return false;
            }
        }
    }

    public function CanViewInbox()
    {
        return $this->CheckPermission("m_readInbox");
    }

    public function CanViewCalendarList()
    {
        return $this->CheckPermission("a_viewCalendarList");
    }

    public function CanCreateCalendars()
    {
        return $this->CheckPermission("m_createCalendars");
    }

    public function CanEditCalendar($calendarID)
    {
        if ($this->CheckPermission("m_editAllCalendars")) {
            // moderator can edit all calendars
            return true;
        }

        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `OwnerID` FROM `vp_calendars` WHERE `ID` = '$calendarID'");
        if ($result == null) {
            // calendar does not exist
            return false;
        } else {
            $ownerID = $result['OwnerID'];
            if ($ownerID == VPLogin::LoggedInUserID()) {
                // the user is the owner of this calendar and has the permission to edit their own calendars
                return $this->CheckPermission("u_editOwnCalendars");
            } else {
                // the user is not the owner of this calendar
                // therefore we check for explicit permission to edit this calendar
                return $this->CheckPermission("editCalendar_$calendarID");
            }
        }
    }

    public function CanViewLogbook()
    {
        return $this->CheckPermission("x_viewLogbook");
    }

    public function CanViewGalleryList()
    {
        return $this->CheckPermission("u_viewGalleryList");
    }

    public function CanEditGallery($galleryID)
    {
        if ($this->CheckPermission("m_editAllGalleries")) {
            // moderators can edit all galleries
            return true;
        }

        if ($this->CheckPermission("editGallery_$galleryID")) {
            // gallery has explicit permission to edit this photo
            return true;
        }

        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `Owner` FROM `vp_galleries` WHERE `ID` = '$galleryID'");
        if ($result == null) {
            // gallery does not exist
            return false;
        } else {
            $ownerID = $result['Owner'];
            if ($ownerID == VPLogin::LoggedInUserID()) {
                // the user is the owner of this gallery and has the permission to edit their own galleries
                return $this->CheckPermission("u_editOwnGalleries");
            } else {
                // the user is not the owner of this galleries and may not edit this gallery
                return false;
            }
        }
    }

    public function CanCreateGallery()
    {
        return $this->CheckPermission("a_createGalleries");
    }

    public function CanManageAPIKeys()
    {
        return $this->CheckPermission("x_manageAPIKeys");
    }

    public function CanDeleteMessages()
    {
        return $this->CheckPermission("x_deleteMessages");
    }

    public function CanExecuteSQL()
    {
        return $this->CheckPermission("x_querySQL");
    }

    public function CanChangeArticleTimestamps()
    {
        return $this->CheckPermission("m_changeArticleTimestamps");
    }


}