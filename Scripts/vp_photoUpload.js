function vp_UpdateUploadInfo(info) {
    var newNode = document.createElement('p');
    newNode.className = 'vp_info_box';
    newNode.innerHTML = info;

    var pNode = document.createElement('div');
    pNode.appendChild(newNode);

    var containerDiv = document.getElementById('vp_uploaded_files');
    containerDiv.appendChild(pNode);
}

Dropzone.options.photoUploadDropzone = {
    init: function (file) {
        this.on("success", function (file) {
            vp_UpdateUploadInfo(document.getElementById("vp_text_imageUploaded").innerHTML.replace("{0}", file.name))
        });
    }
};

Dropzone.prototype.defaultOptions.acceptedFiles = "image/*";
Dropzone.prototype.defaultOptions.maxFilesize = 20;