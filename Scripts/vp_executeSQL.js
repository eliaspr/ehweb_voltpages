function vp_executeSQLButton() {
    vp_executeSQLButton(false);
}

function vp_executeSQLButton(backOnDenial) {
    let input = document.getElementById("vp_sqlQuery").value.trim().toUpperCase();
    let execute = true;

    if (input.startsWith("DELETE"))
        execute = confirm("Die folgende SQL-Anfrage wirklich ausführen? Es könnten Werte gelöscht werden!\n" + input);
    if (input.startsWith("DROP"))
        execute = confirm("Die folgende SQL-Anfrage wirklich ausführen? Es könnten Werte gelöscht werden!\n" + input);

    if (execute)
        document.forms['vp_sqlForm'].submit();
    else if (backOnDenial)
        window.history.back();
}

function vp_formExecute() {
    let input = document.getElementById("vp_sqlQuery").value.trim().toUpperCase();
    let execute = true;

    if (input.startsWith("DELETE"))
        execute = confirm("Die folgende SQL-Anfrage wirklich ausführen? Es könnten Werte gelöscht werden!\n" + input);
    if (input.startsWith("DROP"))
        execute = confirm("Die folgende SQL-Anfrage wirklich ausführen? Es könnten Werte gelöscht werden!\n" + input);

    return execute;
}