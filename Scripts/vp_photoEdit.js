var unsavedChanges = false;
var saveButtonClicked = false;

function vp_imgInput() {
    unsavedChanges = true;
}

function vp_imgInfoInput() {
    var length = document.getElementById('vp_img_info').value.length;
    var remaining = (1995 - length);

    if (remaining >= 0) {
        var displayText = document.getElementById("vp_text_remaningChars").innerHTML;
        displayText = displayText.replace("{0}", length).replace("{1}", 2000);
        document.getElementById('img_info_charLimit').innerHTML = displayText;

        var textArea = document.getElementById('vp_img_info');
        if (textArea.classList.contains("vp_textarea_error"))
            textArea.classList.remove("vp_textarea_error");
    }
    else {
        var displayText = document.getElementById("vp_text_tooManyChars").innerHTML;
        displayText = displayText.replace("{0}", -remaining);
        document.getElementById('img_info_charLimit').innerHTML = displayText;

        var textArea = document.getElementById('vp_img_info');
        if (!textArea.classList.contains("vp_textarea_error"))
            textArea.classList.add("vp_textarea_error");
    }
}

function vp_imgDelete() {
    if (confirm(document.getElementById("vp_text_deleteWarning").innerHTML)) {
        targetURL = document.getElementById("vp_delete_redirect").innerHTML;
        window.location.replace(targetURL);
    }
}

function vp_imgSave() {
    saveButtonClicked = true;
    document.getElementById("vp_imgForm").submit();
}

function vp_img_transformButton(type) {
    if (unsavedChanges) {
        alert(document.getElementById("vp_text_saveFirst").innerText);
        return;
    }
    document.forms["form_" + type].submit();
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};

document.getElementById("vp_input_photograph").oninput = vp_imgInput;
document.getElementById('vp_img_info').onkeyup = function () {
    vp_imgInput();
    vp_imgInfoInput();
};

vp_imgInfoInput();