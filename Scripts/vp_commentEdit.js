var unsavedChanges = false;
var saveButtonClicked = false;

var simplemde = new SimpleMDE({
    spellChecker: false
});

function vp_commentInput() {
    unsavedChanges = true;
}

function vp_commentTextInput() {
    var length = simplemde.value().length;
    var remaining = (4995 - length);

    if (remaining >= 0) {
        percent = length / 5000.0 * 100.0;

        var displayText = document.getElementById("vp_text_remaningChars").innerHTML;
        displayText = displayText.replace("{0}", length).replace("{1}", 10000).replace("{2}", percent);
        document.getElementById('vp_comment_charLimit').innerHTML = displayText;

        var textAreaWrap = document.getElementById('vp_comment_text_wrap');
        if (textAreaWrap.classList.contains("vp_textarea_error"))
            textAreaWrap.classList.remove("vp_textarea_error");
    }
    else {
        var displayText = document.getElementById("vp_text_tooManyChars").innerHTML;
        displayText = displayText.replace("{0}", -remaining);
        document.getElementById('vp_comment_charLimit').innerHTML = displayText;

        var textAreaWrap = document.getElementById('vp_comment_text_wrap');
        if (!textAreaWrap.classList.contains("vp_textarea_error"))
            textAreaWrap.classList.add("vp_textarea_error");
    }
}

function vp_commentDelete() {
    if (confirm(document.getElementById("vp_text_deleteWarning").innerHTML)) {
        targetURL = document.getElementById("vp_delete_redirect").innerHTML;
        window.location.replace(targetURL);
    }
}

function vp_commentSave() {
    saveButtonClicked = true;
    document.getElementById("vp_commentForm").submit();
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};

simplemde.codemirror.on("keyup", function () {
    vp_commentInput();
    vp_commentTextInput();
});

vp_commentTextInput();