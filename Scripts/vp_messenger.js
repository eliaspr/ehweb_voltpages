function vp_openMessagePage(messageID) {
    let baseRedirect = document.getElementById("vp_messageViewRedirect").innerHTML;
    window.location = baseRedirect + messageID;
}

function vp_deleteMessageButton() {
    let text = document.getElementById("vp_confirmMsgDelete").innerText;
    if (confirm(text)) {
        let url = document.getElementById("vp_redirectMsgDelete").innerText;
        window.location.replace(url);
    }
}