function vp_createGallery() {
    result = confirm(document.getElementById("vp_text_newGallery").innerHTML);
    if (result) {
        redirectURL = document.getElementById("vp_newGalleryRedirect").innerHTML;
        window.location.href = redirectURL;
    }
}