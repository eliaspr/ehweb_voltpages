function vp_calendarCreateNew() {
    let name = prompt(document.getElementById("vp_newCalendarPrompt").innerHTML);
    if(name != null && name.length > 0) {
        let destURL = document.getElementById("vp_newCalendarURL").innerHTML + "?name=" + name;
        window.location.replace(destURL);
    }
}