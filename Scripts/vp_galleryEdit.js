var unsavedChanges = false;
var saveButtonClicked = false;

function vp_galleryInput() {
    unsavedChanges = true;
}

function vp_galleryInfoInput() {
    var length = document.getElementById('vp_gallery_info').value.length;
    var remaining = (1995 - length);

    if (remaining >= 0) {
        var displayText = document.getElementById("vp_text_remaningChars").innerHTML;
        displayText = displayText.replace("{0}", length).replace("{1}", 2000);
        document.getElementById('gallery_info_charLimit').innerHTML = displayText;

        var textArea = document.getElementById('vp_gallery_info');
        if (textArea.classList.contains("vp_textarea_error"))
            textArea.classList.remove("vp_textarea_error");
    }
    else {
        var displayText = document.getElementById("vp_text_tooManyChars").innerHTML;
        displayText = displayText.replace("{0}", -remaining);
        document.getElementById('gallery_info_charLimit').innerHTML = displayText;

        var textArea = document.getElementById('vp_gallery_info');
        if (!textArea.classList.contains("vp_textarea_error"))
            textArea.classList.add("vp_textarea_error");
    }
}

function vp_galleryDelete() {
    if (confirm(document.getElementById("vp_text_deleteWarning").innerHTML)) {
        targetURL = document.getElementById("vp_delete_redirect").innerHTML;
        window.location.replace(targetURL);
    }
}

function vp_gallerySave() {
    saveButtonClicked = true;
    document.getElementById("vp_galleryForm").submit();
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};

document.getElementById('vp_gallery_info').onkeyup = function () {
    vp_galleryInput();
    vp_galleryInfoInput();
};

vp_galleryInfoInput();

function vp_galleryRemoveImage(index) {
    var warning = document.getElementById("vp_text_removeImageWarning").innerHTML;
    if (confirm(warning)) {
        targetURL = document.getElementById("vp_removeImgRedirect").innerHTML + index;
        window.location.replace(targetURL);
    }
}