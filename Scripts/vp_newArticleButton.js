function vp_createArticle() {
    result = confirm(document.getElementById("vp_text_newArticle").innerHTML);
    if (result) {
        redirectURL = document.getElementById("vp_newArticleRedirect").innerHTML;
        window.location.href = redirectURL;
    }
}