var unsavedChanges = false;
var saveButtonClicked = false;

function vp_dateSubmitButton() {
    saveButtonClicked = true;
    document.forms["vp_editDateForm"].submit();
}

function vp_dateDeleteButton() {
    window.location.href = document.getElementById("vp_dateDeleteURL").innerHTML;
}

function vp_dateInput() {
    unsavedChanges = true;
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};