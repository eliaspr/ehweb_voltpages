function vp_articleNewTag() {
    var name = prompt(document.getElementById("vp_text_newTagName").innerHTML);
    if (name != null && name.length > 0) {
        var redirectURL = document.getElementById("vp_newtag_redirect").innerHTML;
        window.location.replace(redirectURL + name);
    }
}