function vp_createUser() {
    result = confirm(document.getElementById("vp_text_newUser").innerHTML);
    if (result) {
        redirectURL = document.getElementById("vp_newUserRedirect").innerHTML;
        window.location.href = redirectURL;
    }
}