var unsavedChanges = false;
var saveButtonClicked = false;

function vp_userdataInput() {
    unsavedChanges = true;
}

function vp_userdataDelete() {
    if (confirm(document.getElementById("vp_text_deleteUser1").innerHTML)) {
        if (confirm(document.getElementById("vp_text_deleteUser2").innerHTML)) {
            targetURL = document.getElementById("vp_delete_redirect").innerHTML;
            window.location.replace(targetURL);
        }
    }
}

function vp_userdataSave() {
    saveButtonClicked = true;
    document.getElementById("vp_userdataForm").submit();
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};

// permission stuff below

function vp_addPermission(newPerm) {
    var permissionList = document.getElementById("vp_permissionList").value;
    var perms = permissionList.length === 0 ? [] : permissionList.split(";");
    for (n = 0; n < perms.length; n++) {
        if (perms[n] == newPerm) return;
    }

    perms[perms.length] = newPerm;
    vp_userdataPermissionsFromArray(perms);
}

function vp_userdataAddPermission() {
    var permissions = document.getElementById("vp_newPermission").value;
    document.getElementById("vp_newPermission").value = "";

    permissions = permissions.trim();
    var newPermissions = permissions.split(" ");
    for (var i = 0; i < newPermissions.length; i++) {
        var newPerm = newPermissions[i].trim();
        if (newPerm.length > 0) {
            vp_addPermission(newPerm);
        }
    }
}

function vp_userdataRemovePermission(i) {
    var permissionList = document.getElementById("vp_permissionList").value;
    var perms = permissionList.split(";");
    if (i >= perms.length) return;

    var newperms = [];
    var k = 0;
    for (n = 0; n < perms.length; n++) {
        if (n != i) {
            newperms[k++] = perms[n];
        }
    }

    vp_userdataPermissionsFromArray(newperms);
}

function vp_userdataPermissionsFromArray(newperms) {
    var newPermList = "";
    for (n = 0; n < newperms.length; n++) {
        newPermList += newperms[n];
        if (n < newperms.length - 1) {
            newPermList += ";";
        }
    }

    document.getElementById("vp_permissionList").value = newPermList;
    vp_userdataUpdatePermissionDisplay();
    vp_userdataInput();
}

function vp_userdataUpdatePermissionDisplay() {
    var permissionList = document.getElementById("vp_permissionList").value;
    var perms = permissionList.split(";");

    var display = "";
    var iconPath = document.getElementById("vp_deleteIconPath").innerHTML;
    if (permissionList.length > 0 && perms.length > 0) {
        for (i = 0; i < perms.length; i++) {
            display += perms[i];
            display += '<img style="margin-left: 10px; width: 11px; height: 11px; cursor: pointer;" onclick="vp_userdataRemovePermission(' + i + ')" src="' + iconPath + '"/>';
            display += "<br>";
        }
    } else {
        display = "<em>" + document.getElementById("vp_text_noPermissions").innerHTML + "</em>";
    }
    document.getElementById("vp_permissionDisplay").innerHTML = display;
}

vp_userdataUpdatePermissionDisplay();