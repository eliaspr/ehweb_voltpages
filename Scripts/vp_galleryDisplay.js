function vp_gallery_next(galleryID) {
    vp_gallery_change(galleryID, 1);
}

function vp_gallery_previous(galleryID) {
    vp_gallery_change(galleryID, -1);
}

function vp_gallery_change(galleryID, change) {
    var current = parseInt(document.getElementById("gallery_" + galleryID + "_current").innerText);
    current += change;
    var count = document.getElementById("gallery_" + galleryID + "_count").innerText;
    if (current < 0) current = 0;
    if (current >= count) current = count - 1;

    document.getElementById("gallery_" + galleryID + "_current").innerText = current;
    document.getElementById("gallery_" + galleryID + "_currIndex").innerText = current + 1;

    var imgData = document.getElementById("gallery_" + galleryID + "_img_" + current).innerText;
    var res = imgData.split(";");

    var photographerID = res[0];
    var photographerName = document.getElementById("photographer_" + photographerID).innerText;
    document.getElementById("gallery_" + galleryID + "_photographer").innerText = photographerName;

    document.getElementById("gallery_" + galleryID + "_display").src = res[1];
}