var unsavedChanges = false;
var saveButtonClicked = false;

var simplemde = new SimpleMDE({
    spellChecker: false
});

function vp_copyClipboard(htmlID) {
    var tb = document.getElementById(htmlID);
    tb.disabled = false;
    tb.select();
    document.execCommand("copy");
    tb.disabled = true;
}

function vp_articleInput() {
    unsavedChanges = true;
}

function vp_articleDelete() {
    if (confirm(document.getElementById("vp_text_deleteWarning").innerHTML)) {
        targetURL = document.getElementById("vp_delete_redirect").innerHTML;
        window.location.replace(targetURL);
    }
}

function vp_articleSave() {
    saveButtonClicked = true;
    document.getElementById("vp_articleForm").submit();
}

function vp_articleChooseThumbnail(articleID) {
    if (unsavedChanges) {
        alert(document.getElementById("vp_text_saveChanges").innerHTML);
    } else {
        targetURL = document.getElementById("vp_redirectURL").innerHTML + "/photos?for=article&articleID=" + articleID;
        window.location.href = targetURL;
    }
}

function vp_articleRemoveThumbnail(articleID) {
    if (unsavedChanges) {
        alert(document.getElementById("vp_text_saveChanges").innerHTML);
    } else {
        targetURL = document.getElementById("vp_redirectURL").innerHTML + "/articles/edit/" + articleID + "?thumbnailID=remove";
        window.location.href = targetURL;
    }
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};

document.getElementById("vp_input_author").oninput = vp_articleInput;

simplemde.codemirror.on("keyup", function () {
    vp_articleInput();
});

function vp_articleEditDate() {
    document.getElementById("articleTimestampDisplay").style.display = "none";
    document.getElementById("articleTimestampEdit").style.display = "inline-block";
    document.getElementById("articleTimestampBtn").style.display = "none";
    unsavedChanges = true;
}

// tag stuff below

function vp_articleGetTagArray() {
    var tagList = document.getElementById("vp_article_tags").value;
    tagList = tagList.trim();
    tagList = tagList.substring(1, tagList.length - 1);
    var tags = tagList.length > 0 ? tagList.split(";") : [];
    return tags;
}

function vp_articleUpdateTagArray(tags) {
    var tagList = ";";
    for (n = 0; n < tags.length; n++) {
        tagList += tags[n];
        if (n < tags.length - 1) {
            tagList += ";";
        }
    }
    tagList += ";";
    document.getElementById("vp_article_tags").value = tagList;
}

function vp_articleRemoveTag(deleteID) {
    var tags = vp_articleGetTagArray();
    var newtags = [];
    for (n = 0; n < tags.length; n++) {
        var tagID = tags[n];
        if (tagID != deleteID) {
            newtags[newtags.length] = tagID;
        }
    }

    vp_articleUpdateTagArray(newtags);
    vp_articleUpdateTagDisplay();
    vp_articleInput();
}

function vp_articles_addTagFromDropdown() {
    var selectElement = document.getElementById("vp_tag_selector");
    var selectedOption = selectElement.options[selectElement.selectedIndex];
    if (selectedOption != null) {
        var tagID = selectedOption.value;
        var tagName = selectedOption.text;

        var tags = vp_articleGetTagArray();
        for (n = 0; n < tags.length; n++) {
            if (tags[n] == tagID) {
                return;
            }
        }
        tags[tags.length] = tagID;

        if (!document.getElementById("vp_tagName_" + tagID)) {
            var span = document.createElement("span");
            span.style.display = "none";
            span.id = "vp_tagName_" + tagID;
            span.innerHTML = tagName;
            document.getElementById("vp_tagHiddenData").appendChild(span);
        }

        vp_articleUpdateTagArray(tags);
        vp_articleUpdateTagDisplay();
        vp_articleInput();
    }
}

function vp_articleUpdateTagDisplay() {
    var tags = vp_articleGetTagArray();

    if (tags.length == 0) {
        document.getElementById("vp_article_tagDisplay").innerHTML = "<em>" + document.getElementById("vp_text_noTags").innerHTML + "</em>";
    } else {
        var deleteImageURL = document.getElementById("delete_image_url").innerHTML;
        generatedHTML = "";
        for (n = 0; n < tags.length; n++) {
            var tagID = tags[n];
            var tagName = document.getElementById("vp_tagName_" + tagID).innerHTML;

            generatedHTML += '<span style="margin-right: 8px; padding: 3px; font-size: 15px; height: 18px;">';
            generatedHTML += tagName;
            generatedHTML += '<img onClick="vp_articleRemoveTag(' + tagID + ')" style="width: 10px; height: 10px; margin-left: 5px; cursor: pointer;" src="' + deleteImageURL + '"/>';
            generatedHTML += '</span>';
        }
        document.getElementById("vp_article_tagDisplay").innerHTML = generatedHTML;
    }
}

vp_articleUpdateTagDisplay();