var unsavedChanges = false;
var saveButtonClicked = false;

function vp_myprofileInput() {
    unsavedChanges = true;
}

function vp_myprofileSave() {
    saveButtonClicked = true;
    document.getElementById("vp_myprofileForm").submit();
}

function vp_myprofileChangePW() {
    window.location.href = document.getElementById("vp_changePwURL").innerText;
}

window.onbeforeunload = function () {
    return (!saveButtonClicked && unsavedChanges) ? document.getElementById("vp_text_unsavedChanges").innerHTML : null;
};