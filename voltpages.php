<?php

require_once 'vp_articles.php';
require_once 'vp_comments.php';
require_once 'vp_calendars.php';
require_once 'vp_images.php';
require_once 'vp_login.php';
require_once 'vp_galleries.php';
require_once 'vp_messages.php';
require_once 'vp_permissions.php';
require_once 'vp_search.php';

require_once 'API/vp_api.php';
require_once 'Plugins/vp_pluginManager.php';

class VoltPages
{
    static $VP_VERSION = "1.7.0";

    private static $pluginManager = null;

    public static function GetPluginManager(): VPPluginManager
    {
        return self::$pluginManager != null ? self::$pluginManager : (self::$pluginManager = new VPPluginManager());
    }
}

class VPAdmin
{

    public static function ShowAdminPage($path)
    {
        require 'Page/vp_header.php';

        if (VPLogin::IsUserLoggedIn()) {
            VPLogin::CheckTimeout();

            $userID = VPLogin::LoggedInUserID();
            if (sizeof($path) == 0) {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/dashboard");
                exit;
            } else {
                $page = $path[0];
                if ($page == "logout") {
                    VPLogin::AttemptLogout();
                } else {
                    $userData = VPUserData::GetUserData($userID);
                    if ($userData->ResetPassword) {
                        require 'Page/vp_resetPW.php';
                    } else {
                        if (!VPAdmin::ShowPageInternal($page, array_slice($path, 1), $userData)) {
                            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/dashboard");
                            exit;
                        }
                    }
                }
            }
        } else {
            if (sizeof($path) == 0 || $path[0] != "login") {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/login");
                exit;
            } else {
                require 'Page/vp_login.php';
            }
        }

        require 'Page/vp_footer.html';
    }

    private static function UpdateUserOnlineTime($userID)
    {
        $now = intval(time());
        if (!isset($_SESSION['vp_lastTime'])) {
            $_SESSION['vp_lastTime'] = $now;
            return;
        }

        $timeSeconds = $now - $_SESSION['vp_lastTime'];
        if ($timeSeconds > 8 * 60) $timeSeconds = 8 * 60;
        $currentValue = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT TimeOnline FROM vp_users WHERE ID = $userID")['TimeOnline'];
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_users SET TimeOnline = " . intval($currentValue + $timeSeconds) . " WHERE ID = $userID");
        $_SESSION['vp_lastTime'] = $now;
    }

    private static function NavBarButton($link, $text, $rawtext = false)
    {
        $link = VPConfig::$VP_REDIRECT_URL . '/' . $link;
        if (!$rawtext) $text = VPLocale::Get($text);
        echo '<span style="margin-left: 20px;"><a href="' . $link . '">' . $text . '</a></span>';
    }

    private static function ShowPageInternal($page, $path, $user)
    {
        $permissions = VPPermissions::FromUserID($user->ID);

        if ($page == "change_pw") {
            require 'Page/vp_changePW.php';
            return true;
        }

        self::UpdateUserOnlineTime($user->ID);

        echo '<div class="vp_navigation_bar">';
        {
            echo '<div class="vp_nav_bar_container">';

            $targetURL = VPConfig::$VP_REDIRECT_URL . '/search';
            $searchQuery = ($page == "search" && isset($_GET['query'])) ? htmlspecialchars($_GET['query']) : '';
            echo '<form class="nav_bar_search" method="get" action="' . $targetURL . '">';
            echo '<input name="query" placeholder="' . VPLocale::Get("search-page.search-bar") . '" value="' . $searchQuery . '">';
            echo '<button></button>';
            echo '</form>';

            self::NavBarButton("dashboard", "dashboard.sidebar.home");

            if ($permissions->CanViewInbox()) {
                self::NavBarButton("inbox", "dashboard.sidebar.inbox");
            }

            echo '<div style="display: inline; float: right">';
            self::NavBarButton("profile", VPUserData::GetUserName(VPLogin::LoggedInUserID()), true);
            self::NavBarButton("logout", "dashboard.sidebar.logout");
            echo '</div>';

            echo '</div>';
        }
        echo '</div><div class="vp_nav_bar_spacer"></div>';
        echo '<div class="vp_main_content">';

        if ($page == "dashboard") {
            require 'Page/vp_dashboard.php';
        } else if ($page == "inbox") {
            require_once 'Page/vp_messages.php';
            VPMessagesPage::ShowMessagesPage($path);
        } else if ($page == "search") {
            require_once 'Page/vp_searchPage.php';
            VPSearchPage::ShowSearchPage($path);
        } else if ($page == "photos") {
            require_once 'Page/vp_photos.php';
            VPPhotosPage::ShowPhotosPage($path);
        } else if ($page == "articles") {
            require_once 'Page/vp_articles.php';
            VPArticlesPage::ShowArticlesPage($path);
        } else if ($page == "users") {
            require_once 'Page/vp_users.php';
            VPUsersPage::ShowUsersPage($path);
        } else if ($page == "profile") {
            require_once 'Page/vp_profile.php';
            VPProfilePage::ShowProfilePage($path);
        } else if ($page == "comments") {
            require_once 'Page/vp_comments.php';
            VPCommentsPage::ShowCommentsPage($path);
        } else if ($page == "calendars") {
            require_once 'Page/vp_calendar.php';
            VPCalendarPage::ShowCalendarPage($path);
        } else if ($page == "log") {
            require_once 'Page/vp_logbook.php';
            VPLogbook::ShowLogbookPage($path);
        } else if ($page == "galleries") {
            require_once 'Page/vp_galleries.php';
            VPGalleriesPage::ShowGalleriesPage($path);
        } else if ($page == "database") {
            require_once 'Page/vp_databasePage.php';
            VPDatabasePage::ShowDatabasePage($path);
        } else if ($page == "plugins") {
            require_once 'Page/vp_pluginList.php';
            VPPluginList::ShowPluginList();
        } else if ($page == "plugin") {
            VoltPages::GetPluginManager()->ProcessPageRequest($path);
        } else {
            return false;
        }

        echo '<div class="vp_footer">';
        echo '<table style="width: 100%;"><tr>';
        {
            echo '<td style="width: 33%; vertical-align: top;"><p>';
            echo '<strong>VoltPages v' . VoltPages::$VP_VERSION . '<br></strong><br>';

            echo '<a href="' . VPConfig::$VP_INSTALL_DIR . 'CHANGELOG.txt" target="_blank">Changelog</a><br>';
            echo '<a href="https://www.github.com/eliaspr/VoltPages" target="_blank">GitHub</a><br><br>';

            if ($permissions->CheckPermission("_admin")) {
                $count = VoltPages::GetPluginManager()->GetPluginCount();
                $pluginsURL = VPConfig::$VP_REDIRECT_URL . '/plugins';
                echo '<a href="' . $pluginsURL . '" style="color: inherit;">';
                echo $count == 1 ? VPLocale::Get("plugins.plugin-active") : VPLocale::Get("plugins.plugins-active", [$count]);
                echo '</a><br><br>';
            }

            echo '© Elias Hörner 2019';
            echo '</p></td>';
        }
        {
            echo '<td style="width: 34%; vertical-align: top;"><p>';
            echo '<strong>' . VPUserData::GetUserName(VPLogin::LoggedInUserID()) . '<br></strong><br>';
            echo 'angemeldet bis ' . date("H:i:s", $_SESSION['vp_Timeout']).'<br>';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/profile">Benutzerprofil</a><br>';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/change_pw">Passwort ändern</a><br>';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/logout">Abmelden</a><br>';
            echo '</p></td>';
        }
        {
            echo '<td style="width: 33%; vertical-align: top;"><p>';
            echo '<strong>' . VPConfig::$VP_HTML_TITLE . '<br></strong><br>';
            echo '<a href="' . VPConfig::$VP_PUBLIC_WEBSITE_URL . '">'.VPConfig::$VP_PUBLIC_WEBSITE_URL.'</a><br>';
            echo '</p></td>';
        }
        echo '</tr></table></div></div>';
        return true;
    }

}