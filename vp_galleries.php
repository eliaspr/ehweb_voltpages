<?php

class VPGallery
{
    private $GalleryFound;

    public $ID;
    public $Title;
    public $Info;
    public $Owner;
    public $Timestamp;
    public $TimestampFormatted;
    public $ImageIDs = array();

    private function LoadFromDatabase($galleryID)
    {
        $galleryID = VPDatabaseConn::EscapeSQLString($galleryID);
        $sql = "SELECT * FROM `vp_galleries` WHERE `ID` = '$galleryID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->GalleryFound = false;
        } else {
            $this->GalleryFound = true;
            $imageRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($imageRow);
        }
    }

    private function LoadFromRow($imageRow)
    {
        $this->ID = $imageRow['ID'];
        $this->Title = utf8_encode($imageRow['Title']);
        $this->Info = utf8_encode($imageRow['Info']);
        $this->Owner = $imageRow['Owner'];

        $this->Timestamp = $imageRow['Timestamp'];
        $time = strtotime($this->Timestamp);
        $this->TimestampFormatted = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $time));

        $sql = "SELECT ID FROM vp_images WHERE GalleryID = " . $this->ID . " ORDER BY GalleryIndex ASC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result))
            $this->ImageIDs[] = $row['ID'];
    }

    public function WasGalleryFound()
    {
        return $this->GalleryFound;
    }

    public static function GetGalleryData($galleryID)
    {
        $gallery = new VPGallery();
        $gallery->LoadFromDatabase($galleryID);
        return $gallery;
    }

    public static function GetGalleryDataFromRow($galleryRow)
    {
        $gallery = new VPGallery();
        $gallery->LoadFromRow($galleryRow);
        return $gallery;
    }

    public function CalculateTotalFileSize(): int
    {
        $bytes = 0;
        foreach ($this->ImageIDs as $id) {
            $sql = "SELECT `File` FROM vp_images WHERE `ID` = '$id'";
            $filename = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['File'];
            $bytes += filesize($filename);
        }
        return $bytes;
    }

}