<?php

require_once 'vp_database.php';
require_once 'vp_login.php';

class VPComment
{

    private $CommentFount;
    private $UserName = null;

    public $ID;
    public $UserID;
    public $ArticleID;
    public $Text;
    public $ReplyTo;
    public $Created;
    public $CreatedFormatted;
    public $CreatedFormattedLong;
    public $LastEdit;
    public $Revisions;

    private function LoadFromDatabase($commentID)
    {
        $commentID = VPDatabaseConn::EscapeSQLString($commentID);
        $sql = "SELECT * FROM `vp_comments` WHERE `ID` = '$commentID' LIMIT 1";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->CommentFount = false;
        } else {
            $this->CommentFount = true;
            $commentRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($commentRow);
        }
    }

    private function LoadFromRow($commentRow)
    {
        $this->ID = $commentRow['ID'];
        $this->UserID = $commentRow['UserID'];
        $this->ArticleID = $commentRow['ArticleID'];
        $this->Text = utf8_encode($commentRow['Text']);
        $this->ReplyTo = $commentRow['ReplyTo'];
        $this->Revisions = $commentRow['Revisions'];

        $this->Created = $commentRow['Created'];
        $time = strtotime($this->Created);
        $this->CreatedFormatted = trim(strftime(VPConfig::$VP_DATE_FORMAT, $time));
        $this->CreatedFormattedLong = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $time));

        $this->LastEdit = $commentRow['LastEdit'];
    }

    public function GetUserName()
    {
        if ($this->UserName == null) {
            return $this->UserName = VPUserData::GetUserName($this->UserID);
        } else {
            return $this->UserName;
        }
    }

    public function WasCommentFound()
    {
        return $this->CommentFount;
    }

    private static function GetCommentList($sql, $limit, $offset, $dateAscending)
    {
        $sql .= " ORDER BY `Created` " . ($dateAscending ? "ASC" : "DESC");
        if ($limit > 0)
            $sql .= " LIMIT " . VPDatabaseConn::EscapeSQLString($limit);
        if ($offset > 0)
            $sql .= " OFFSET " . VPDatabaseConn::EscapeSQLString($offset);

        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        $commentArray = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $commentData = new VPComment();
            $commentData->LoadFromRow($row);
            $commentArray[] = $commentData;
        }
        return $commentArray;
    }

    public static function GetCommentsOnArticle($articleID, $limit = 0, $offset = 0, $dateAscending = false)
    {
        $articleID = VPDatabaseConn::EscapeSQLString($articleID);
        $sql = "SELECT * FROM `vp_comments` WHERE `ArticleID` = '$articleID'";
        return VPComment::GetCommentList($sql, $limit, $offset, $dateAscending);
    }

    public static function GetCommentCountOnArticle($articleID)
    {
        $articleID = VPDatabaseConn::EscapeSQLString($articleID);
        $sql = "SELECT `ID` FROM `vp_comments` WHERE `ArticleID` = '$articleID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        return mysqli_num_rows($result);
    }

    public static function GetRepliesTo($commentID, $limit = 0, $offset = 0, $dateAscending = false)
    {
        $commentID = VPDatabaseConn::EscapeSQLString($commentID);
        $sql = "SELECT * FROM `vp_comments` WHERE AND `ReplyTo` = '$commentID'";
        return VPComment::GetCommentList($sql, $limit, $offset, $dateAscending);
    }

    public static function GetReplyCount($commentID)
    {
        $commentID = VPDatabaseConn::EscapeSQLString($commentID);
        $sql = "SELECT `ID` FROM `vp_comments` WHERE AND `ReplyTo` = '$commentID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        return mysqli_num_rows($result);
    }

    public static function GetComment($commentID)
    {
        $comment = new VPComment();
        $comment->LoadFromDatabase($commentID);
        return $comment;
    }

    public static function GetCommentFromRow($commentRow)
    {
        $comment = new VPComment();
        $comment->LoadFromRow($commentRow);
        return $comment;
    }

}