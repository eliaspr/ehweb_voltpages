<?php

require_once __DIR__ . '/../vp_login.php';

class VPAppInterface
{

    const SUCCESS = 100;

    const ERROR_NO_API_KEY = 200;
    const ERROR_UNKNOWN_API_KEY = 201;
    const ERROR_EXPIRED_API_KEY = 202;
    const ERROR_REQUEST_TYPE_NOT_SET = 203;
    const ERROR_NO_TARGET_ID = 204;
    const ERROR_NOT_PUBLIC = 205;
    const ERROR_INVALID_ID = 206;
    const ERROR_UNKNOWN_FORMAT = 207;
    const ERROR_UNKNOWN_TYPE = 208;
    const ERROR_NO_QUERY = 209;

    public static function ProcessRequest()
    {
        if (!isset($_GET['key']))
            self::ReturnError(self::ERROR_NO_API_KEY, "No API key provided");

        require_once 'vp_apiKey.php';
        $apiKey = new VPApiKey($_GET['key']);

        if (!$apiKey->exists())
            self::ReturnError(self::ERROR_UNKNOWN_API_KEY, "Unknown API key");
        if ($apiKey->expired())
            self::ReturnError(self::ERROR_EXPIRED_API_KEY, "Expired API key");

        if (!isset($_GET['type']))
            self::ReturnError(self::ERROR_REQUEST_TYPE_NOT_SET, "Request type not set");
        $type = $_GET['type'];

        switch ($type) {
            case 'article':
                self::ProcessArticleRequest();
                break;
            case 'image':
                self::ProcessImageRequest();
                break;
            case 'gallery':
                self::ProcessGalleryRequest();
                break;
            case 'calendar':
                self::ProcessCalendarRequest();
                break;
            case 'search':
                self::ProcessSearchRequest();
                break;
            default:
                self::ReturnError(self::ERROR_UNKNOWN_TYPE, "Unknown request type: '$type'");
        }
    }

    private static function ReturnError(int $code, string $message)
    {
        $result = array();
        $result['code'] = $code;
        $result['error'] = $message;
        echo json_encode($result);
        header("Content-Type: application/json");
        exit;
    }

    private static function ReturnSuccess(array $json)
    {
        $json['code'] = self::SUCCESS;
        echo json_encode($json);
        header("Content-Type: application/json");
        exit;
    }

    private static function GetID(): string
    {
        if (!isset($_GET['id']))
            self::ReturnError(self::ERROR_NO_TARGET_ID, "No target ID provided");
        return $_GET['id'];
    }

    private static function ProcessArticleRequest()
    {
        $targetID = self::GetID();
        require_once __DIR__ . '/../vp_articles.php';
        $data = VPArticle::GetArticle($targetID);

        if (!$data->WasArticleFound())
            self::ReturnError(self::ERROR_INVALID_ID, "Target article does not exist");

        if (!$data->Published)
            self::ReturnError(self::ERROR_NOT_PUBLIC, "Target article not published");

        $result = array();
        $result['header'] = $data->Title;
        $result['author'] = array("id" => $data->DisplayAuthorID, "name" => VPUserData::GetUserName($data->DisplayAuthorID));
        $result['timestamp'] = $data->Timestamp;
        $result['thumbnailID'] = $data->ThumbnailID;

        $tags = array();
        foreach ($data->TagIDs as $tid) {
            $tags[] = VPArticle::GetTagName($tid);
        }
        $result['tags'] = $tags;

        $textFormat = "html-light";
        if (isset($_GET['format']))
            $textFormat = $_GET['format'];

        switch ($textFormat) {
            case 'html':
                $result['text'] = $data->ProcessText(true);
                break;
            case 'html-light':
                $result['text'] = $data->ProcessText(true, new VPAppInterfaceArticleRenderer());
                break;
            case 'markdown':
                $result['text'] = $data->Text;
                break;
            default:
                self::ReturnError(self::ERROR_UNKNOWN_FORMAT, "Invalid format '$textFormat'");
        }

        if (isset($_GET['comments']) && $_GET['comments'] == 'true') {
            $comments = array();

            require_once __DIR__ . '/../vp_comments.php';
            $clist = VPComment::GetCommentsOnArticle($targetID, 0, 0, true);
            foreach ($clist as $c) {
                $cj = array();

                $cj['id'] = $c->ID;
                $cj['author'] = array("id" => $c->UserID, "name" => VPUserData::GetUserName($c->UserID));
                $cj['text'] = $c->Text;
                if ($c->ReplyTo != null) $cj['replyTo'] = $c->ReplyTo;
                $cj['timestamp'] = $c->Created;

                $comments[] = $cj;
            }

            $result['comments'] = $comments;
        }

        self::ReturnSuccess($result);
    }

    private static function ProcessImageRequest()
    {
        $targetID = self::GetID();
        require_once __DIR__ . '/../vp_images.php';
        $data = VPImage::GetImageData($targetID);

        if (!$data->WasImageFound())
            self::ReturnError(self::ERROR_INVALID_ID, "Target image does not exist");

        $result = array();

        $result['title'] = $data->Title;
        $result['description'] = $data->Info;
        $result['photographer'] = array("id" => $data->Photographer, "name" => VPUserData::GetUserName($data->Photographer));
        $result['gallery'] = strlen($data->GalleryID) == 0 ? false : intval($data->GalleryID);
        if (strlen($data->GalleryID) > 0) $result['galleryIndex'] = $data->GalleryIndex;
        $result['timestamp'] = $data->Timestamp;
        $result['files'] = array("preview" => $data->PreviewFile, "full" => $data->File);

        self::ReturnSuccess($result);
    }

    private static function ProcessGalleryRequest()
    {
        $targetID = self::GetID();
        require_once __DIR__ . '/../vp_galleries.php';
        $data = VPGallery::GetGalleryData($targetID);

        if (!$data->WasGalleryFound())
            self::ReturnError(self::ERROR_INVALID_ID, "Target gallery does not exist");

        $result = array();
        $result['title'] = $data->Title;
        $result['description'] = $data->Info;
        $result['timestamp'] = $data->Timestamp;

        $images = array();
        foreach ($data->ImageIDs as $img)
            $images[] = intval($img);
        $result['images'] = $images;

        self::ReturnSuccess($result);
    }

    private static function ProcessCalendarRequest()
    {
        $targetID = self::GetID();
        require_once __DIR__ . '/../vp_calendars.php';
        $calendar = VPCalendar::GetCalendar($targetID);

        if (!$calendar->WasCalendarFound())
            self::ReturnError(self::ERROR_INVALID_ID, "Target calendar does not exist");

        $result = array();
        $result['name'] = $calendar->Name;

        if (isset($_GET['year'])) {
            $year = $_GET['year'];
            $dates = $calendar->GetDates($year . "-01-01 00:00:00", $year . "-12-31 23:59:59", "ASC");
            $datesJSON = array();
            foreach ($dates as $d) {
                $k = array();

                $k['name'] = $d->Name;
                $k['info'] = $d->Info;
                $k['timestamp'] = strftime("%Y-%m-%d %H:%M:%S", $d->Timestamp);

                $datesJSON[] = $k;
            }
            $result['dates'] = $datesJSON;
        }

        self::ReturnSuccess($result);
    }

    private static function ProcessSearchRequest()
    {
        if (!isset($_GET['query']))
            self::ReturnError(self::ERROR_NO_QUERY, "No search query provided");

        require_once __DIR__ . '/../vp_search.php';
        $params = new VPSearchParameters;
        $params->SearchUsers = false;
        $searchResults = VPSearchEngine::ExecuteSearch($_GET['query'], $params);
        usort($searchResults, "VPAPI_CompareResults");

        $resultsJSON = array();
        foreach ($searchResults as $result) {
            $json = array();

            $json['type'] = $result->Type;
            $json['id'] = $result->ObjectID;
            $json['info'] = $result->Preview;

            $resultsJSON[] = $json;
        }
        self::ReturnSuccess(array("results" => $resultsJSON));
    }

}

function VPAPI_CompareResults(VPSearchResult $a, VPSearchResult $b): int
{
    $sa = $a->Type == VPSearchResult::TYPE_COMMENT ? VPLocale::Get("search-page.comment") : $a->Preview;
    $sb = $b->Type == VPSearchResult::TYPE_COMMENT ? VPLocale::Get("search-page.comment") : $b->Preview;
    return strcmp($sa, $sb);
}

class VPAppInterfaceArticleRenderer extends VPArticleRenderer
{
    public function OnPhoto($photoData): string
    {
        return "__image" . $photoData->ID . '__';
    }

    public function OnGallery($galleryData): string
    {
        return "__gallery" . $galleryData->ID . '__';
    }
}
