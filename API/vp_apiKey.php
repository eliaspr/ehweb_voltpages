<?php

require_once __DIR__ . '/../vp_database.php';

class VPApiKey
{

    private $exists = false;
    public $ID;
    public $Key;
    public $OwnerID;
    public $CreatedDate;
    public $CreatedDateFormatted;

    public $HasExpireDate;
    public $ExpireDate;
    public $ExpireDateFormatted;

    public function __construct($key)
    {
        $key = VPDatabaseConn::EscapeSQLString($key);
        $sql = "SELECT * FROM vp_apiKeys WHERE `Key` = '$key'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);
        if ($result == null) {
            $this->exists = false;
        } else {
            $this->exists = true;

            $this->ID = $result['ID'];
            $this->Key = $result['Key'];
            $this->OwnerID = $result['Owner'];
            $this->CreatedDate = $result['CreatedDate'];
            $this->ExpireDate = $result['ExpireDate'];

            if ($this->ExpireDate == null) {
                $this->HasExpireDate = false;
            } else {
                $this->HasExpireDate = true;
                $time = strtotime($this->ExpireDate);
                $this->ExpireDateFormatted = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $time));
            }

            $time = strtotime($this->CreatedDate);
            $this->CreatedDateFormatted = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $time));
        }
    }

    public function exists()
    {
        return $this->exists;
    }

    public function expired()
    {
        if (!$this->HasExpireDate) return false;

        $now = time();
        $expireTime = strtotime($this->ExpireDate);
        return $now > $expireTime;
    }

}