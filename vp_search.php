<?php

class VPSearchResult
{
    const TYPE_IMAGE = 'image';
    const TYPE_GALLERY = 'gallery';
    const TYPE_ARTICLE = 'article';
    const TYPE_COMMENT = 'comment';
    const TYPE_CALENDAR = 'calendar';
    const TYPE_DATE = 'date';
    const TYPE_USER = 'user';

    public $Type;
    public $ObjectID;
    public $Preview;

    public function __construct($Type, $ObjectID, $Preview)
    {
        $this->Type = $Type;
        $this->ObjectID = $ObjectID;
        $this->Preview = $Preview;
    }

}

class VPSearchParameters
{
    public $SearchImages = true;
    public $SearchGalleries = true;
    public $SearchArticles = true;
    public $SearchComments = true;
    public $SearchCalendars = true;
    public $SearchDates = true;
    public $SearchUsers = true;
}

class VPSearchEngine
{

    public static function ExecuteSearch(string $query, VPSearchParameters $params = null): array
    {
        if ($params == null) $params = new VPSearchParameters();
        $results = array();
        $conn = VPDatabaseConn::GetDatabaseConnection();
        $query = VPDatabaseConn::EscapeSQLString(utf8_decode(trim($query)));

        if (strlen($query) == 0) return array();

        $userIDs = array();
        $sql = "SELECT `ID`, `Name` FROM vp_users WHERE `Name` LIKE '%$query%'";
        $result = $conn->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $userIDs[] = $row['ID'];
            if ($params->SearchUsers)
                $results[] = new VPSearchResult(VPSearchResult::TYPE_USER, $row['ID'], utf8_encode($row['Name']));
        }

        if ($params->SearchImages) {
            $sql = "SELECT `ID`, `Title` FROM vp_images WHERE `Title` LIKE '%$query%' OR `Info` LIKE '%$query%'";
            $result = $conn->PerformQuery($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $results[] = new VPSearchResult(VPSearchResult::TYPE_IMAGE, $row['ID'], utf8_encode($row['Title']));
            }
        }

        if ($params->SearchGalleries) {
            $sql = "SELECT `ID`, `Title` FROM vp_galleries WHERE `Title` LIKE '%$query%' OR `Info` LIKE '%$query%'";
            $result = $conn->PerformQuery($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $results[] = new VPSearchResult(VPSearchResult::TYPE_GALLERY, $row['ID'], utf8_encode($row['Title']));
            }
        }

        if ($params->SearchArticles) {
            $sql = "SELECT `ID`, `Title` FROM vp_articles WHERE `Title` LIKE '%$query%' OR `Text` LIKE '%$query%'";
            $result = $conn->PerformQuery($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $results[] = new VPSearchResult(VPSearchResult::TYPE_ARTICLE, $row['ID'], utf8_encode($row['Title']));
            }
        }

        if ($params->SearchComments) {
            $sql = "SELECT `ID`, `Text` FROM vp_comments WHERE `Text` LIKE '%$query%'";
            $result = $conn->PerformQuery($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $results[] = new VPSearchResult(VPSearchResult::TYPE_COMMENT, $row['ID'], utf8_encode($row['Text']));
            }
        }

        if ($params->SearchCalendars) {
            $sql = "SELECT `ID`, `Name` FROM vp_calendars WHERE `Name` LIKE '%$query%'";
            $result = $conn->PerformQuery($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $results[] = new VPSearchResult(VPSearchResult::TYPE_CALENDAR, $row['ID'], utf8_encode($row['Name']));
            }
        }

        if ($params->SearchDates) {
            $sql = "SELECT `ID`, `Name` FROM vp_dates WHERE `Name` LIKE '%$query%' OR `Info` LIKE '%$query%'";
            $result = $conn->PerformQuery($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $results[] = new VPSearchResult(VPSearchResult::TYPE_DATE, $row['ID'], utf8_encode($row['Name']));
            }
        }

        return $results;
    }

    public static function CreateSearchBar()
    {
        $targetURL = VPConfig::$VP_REDIRECT_URL . '/search';
        echo '<form method="get" action="' . $targetURL . '">';
        echo '<input name="query" placeholder="' . VPLocale::Get("search-page.search-bar") . '" style="width: 400px; font-size: 1.0em;"/>';
        echo '&nbsp;<button>' . VPLocale::Get("search-page.search") . '</button>';
        echo '</form>';
    }

}