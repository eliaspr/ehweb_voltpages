<?php

require_once 'vp_database.php';

class VPImage
{

    private $ImageFound;

    public $ID;
    public $File;
    public $PreviewFile;
    public $Title;
    public $Info;
    public $Photographer;
    public $Owner;
    public $Timestamp;
    public $TimestampFormatted;
    public $GalleryID;
    public $GalleryIndex;

    private function LoadFromDatabase($imageID)
    {
        $imageID = VPDatabaseConn::EscapeSQLString($imageID);
        $sql = "SELECT * FROM `vp_images` WHERE `ID` = '$imageID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->ImageFound = false;
        } else {
            $this->ImageFound = true;
            $imageRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($imageRow);
        }
    }

    private function LoadFromRow($imageRow)
    {
        $this->ID = $imageRow['ID'];
        $this->File = utf8_encode($imageRow['File']);
        $this->Title = utf8_encode($imageRow['Title']);
        $this->Info = utf8_encode($imageRow['Info']);
        $this->Photographer = $imageRow['Photographer'];
        $this->Owner = $imageRow['Owner'];

        $this->GalleryID = $imageRow['GalleryID'];
        $this->GalleryIndex = $imageRow['GalleryIndex'];

        $this->Timestamp = $imageRow['Timestamp'];
        $time = strtotime($this->Timestamp);
        $this->TimestampFormatted = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $time));

        $previewFile = utf8_encode($imageRow['PreviewFile']);
        if ($previewFile != null && strlen($previewFile) > 0) {
            $this->PreviewFile = $previewFile;
        } else {
            $this->PreviewFile = utf8_encode($imageRow['File']);
        }
    }

    public function WasImageFound()
    {
        return $this->ImageFound;
    }

    public static function GetImageData($imageID)
    {
        $image = new VPImage();
        $image->LoadFromDatabase($imageID);
        return $image;
    }

    public static function GetImageDataFromRow($imageRow)
    {
        $image = new VPImage();
        $image->LoadFromRow($imageRow);
        return $image;
    }

}