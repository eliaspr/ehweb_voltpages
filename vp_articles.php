<?php

require_once 'vp_database.php';

class VPArticle
{

    private $ArticleFound;

    public $ID;
    public $Timestamp;
    public $TimestampFormatted;
    public $Title;
    public $Text;
    public $TagIDs;
    public $AuthorID;
    public $DisplayAuthorID;
    public $Published;
    public $ThumbnailID;
    public $HumanLink;

    private function LoadFromDatabase($articleID)
    {
        $articleID = VPDatabaseConn::EscapeSQLString($articleID);
        $sql = "SELECT * FROM `vp_articles` WHERE `ID` = '$articleID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->ArticleFound = false;
        } else {
            $this->ArticleFound = true;
            $articleRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($articleRow);
        }
    }

    private function LoadFromRow($articleRow)
    {
        $this->ID = $articleRow['ID'];
        $this->Title = utf8_encode($articleRow['Title']);
        $this->Text = utf8_encode($articleRow['Text']);
        $this->AuthorID = $articleRow['Author'];
        $this->DisplayAuthorID = $articleRow['DisplayAuthor'];
        $this->Published = $articleRow['Published'];
        $this->ThumbnailID = $articleRow['Thumbnail'];

        $tagArrayRaw = explode(";", $articleRow['Tags']);
        $this->TagIDs = array();
        foreach ($tagArrayRaw as $tag) {
            $tag = trim($tag);
            if (strlen($tag) > 0) {
                $this->TagIDs[] = $tag;
            }
        }

        $this->Timestamp = $articleRow['Timestamp'];
        $time = strtotime($this->Timestamp);
        $this->TimestampFormatted = trim(strftime(VPConfig::$VP_DATE_FORMAT, $time));

        if(VPConfig::$VP_WEBSITE_PAGE_ARTICLE != null) {
            if (VPConfig::$VP_ENABLE_BETTER_ARTICLE_LINKS) {
                $search = array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß",);
                $replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss");
                $temp = str_replace($search, $replace, $this->Title);

                $title = "";
                $lastDash = false;
                $allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVW0123456789";
                for ($i = 0; $i < strlen($temp); $i++) {
                    $ch = $temp[$i];
                    if (strpos($allowed, $ch) === FALSE)
                        $ch = '-';
                    if ($ch == '-') {
                        if (!$lastDash) $title .= $ch;
                        $lastDash = true;
                    } else {
                        $title .= $ch;
                        $lastDash = false;
                    }
                }

                $this->HumanLink = $this->ID . '_' . $title;
            } else {
                $this->HumanLink = $this->ID;
            }
            $this->HumanLink = str_replace('{id}', $this->HumanLink, VPConfig::$VP_WEBSITE_PAGE_ARTICLE);
        } else {
            $this->HumanLink = null;
        }
    }

    public
    function WasArticleFound()
    {
        return $this->ArticleFound;
    }

    public
    static function GetLatestArticles($count, $ascending, $onlyPublished = true)
    {
        return VPArticle::GetArticleArray($count, 0, $ascending);
    }

    public
    static function GetArticleArray($count, $offset, $ascendingDate, $onlyPublished = true)
    {
        $count = VPDatabaseConn::EscapeSQLString($count);
        if ($onlyPublished) {
            $sql = "SELECT * FROM `vp_articles` WHERE `Published` = '1' ORDER BY `Timestamp` DESC LIMIT $count";
        } else {
            $sql = "SELECT * FROM `vp_articles` ORDER BY `Timestamp` DESC LIMIT $count";
        }
        if ($offset > 0) $sql .= " OFFSET " . VPDatabaseConn::EscapeSQLString($offset);
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        $articleArray = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $articleData = new VPArticle();
            $articleData->LoadFromRow($row);
            $articleArray[] = $articleData;
        }
        if ($ascendingDate) {
            $articleArray = array_reverse($articleArray);
        }
        return $articleArray;
    }

    public
    static function GetArticle($articleID)
    {
        $article = new VPArticle();
        $article->LoadFromDatabase($articleID);
        return $article;
    }

    public
    static function GetArticleFromRow($articleRow)
    {
        $article = new VPArticle();
        $article->LoadFromRow($articleRow);
        return $article;
    }

    public
    static function GetTotalArticleCount()
    {
        $sql = "SELECT `ID` FROM `vp_articles`";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        return mysqli_num_rows($result);
    }

    public
    static function GetArticleTitle($articleID)
    {
        $articleID = VPDatabaseConn::GetDatabaseConnection()->EscapeSQLString($articleID);
        $sql = "SELECT `Title` FROM `vp_articles` WHERE `ID` = '$articleID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) return null;
        else return utf8_encode(mysqli_fetch_assoc($result)['Title']);
    }

    public
    static function GetTagName($tagID)
    {
        $tagID = VPDatabaseConn::EscapeSQLString($tagID);
        $sql = "SELECT `Name` FROM `vp_tags` WHERE `ID` = '$tagID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);
        return $result == null ? null : utf8_encode($result['Name']);
    }

    public
    function ProcessText($processImages = true, VPArticleRenderer $renderer = null)
    {
        if ($renderer == null)
            $renderer = new VPArticleRenderer;

        require_once 'vp_images.php';
        require_once 'vp_galleries.php';
        require_once 'vp_login.php';

        require_once "Dependencies/Parsedown.php";
        $parser = new Parsedown();
        $markdownProcessed = $parser->text($this->Text);

        $result = "";
        $nextIndex = 0;
        while (true) {
            $indexA = strpos($markdownProcessed, "<p>@" . VPConfig::$VP_ARTICLE_INCLUDE_PHOTO_KEYWORD, $nextIndex);
            $indexB = strpos($markdownProcessed, "<p>@" . VPConfig::$VP_ARTICLE_INCLUDE_GALLERY_KEYWORD, $nextIndex);

            if ($indexA === false && $indexB === false) {
                $result .= substr($markdownProcessed, $nextIndex);
                break;
            }

            if ($indexA !== false && ($indexB === false || $indexA < $indexB)) {
                $result .= substr($markdownProcessed, $nextIndex, $indexA - $nextIndex);
                $nextIndex = strpos($markdownProcessed, "</p>", $indexA) + 4;

                if ($processImages) {
                    $instruction = substr($markdownProcessed, $indexA + 2, $nextIndex - $indexA - 6);
                    $instruction = substr($instruction, 1);
                    $uspos = strpos($instruction, '_');
                    $offset = strlen(VPConfig::$VP_ARTICLE_INCLUDE_PHOTO_KEYWORD) + 2;
                    $photoID = substr($instruction, $offset, $uspos - $offset);
                    $photoData = VPImage::GetImageData($photoID);
                    if ($photoData->WasImageFound())
                        $result .= $renderer->OnPhoto($photoData);
                }
            } else if ($indexB !== false && ($indexA === false || $indexB < $indexA)) {
                $result .= substr($markdownProcessed, $nextIndex, $indexB - $nextIndex);
                $nextIndex = strpos($markdownProcessed, "</p>", $indexB) + 4;

                if ($processImages) {
                    $instruction = substr($markdownProcessed, $indexB + 2, $nextIndex - $indexB - 6);
                    $instruction = substr($instruction, 1);
                    $uspos = strpos($instruction, '_');
                    $offset = strlen(VPConfig::$VP_ARTICLE_INCLUDE_GALLERY_KEYWORD) + 2;
                    $galleryID = substr($instruction, $offset, $uspos - $offset);

                    $galleryData = VPGallery::GetGalleryData($galleryID);
                    if ($galleryData->WasGalleryFound())
                        $result .= $renderer->OnGallery($galleryData);
                }
            }
        }

        return $result;
    }

}

class VPArticleRenderer
{
    private static $globalGalleryIndex = 0;
    private static $listedPhotographers = [];
    private static $galleryScriptIncluded = false;

    public static $usePreviewImages = false;

    public function OnPhoto($photoData): string
    {
        $file = self::$usePreviewImages ? $photoData->PreviewFile : $photoData->File;
        $result = '<table style="width: 100%;"><tr><td style="width: 15%;"></td><td style="width: 70%;">';
        $result .= '<p style="text-align: center;"><a href="/' . htmlspecialchars($photoData->File) . '"><img style="width: 100%;" src="/' . htmlspecialchars($file) . '"/></a><br>';
        $result .= '<span style="font-size: 12px;"><strong>' . $photoData->Title . '</strong>, Bild von ' . VPUserData::GetUserName($photoData->Photographer) . '</span>';
        $result .= '</p></td><td style="width: 15%;"></td></tr></table>';
        return $result;
    }

    public function OnGallery($galleryData): string
    {
        $displayImage = "";
        $displayImgPhotograph = "";

        $result = '<div style="display: none;">';
        $result .= '<span id="gallery_' . self::$globalGalleryIndex . '_current">0</span>';
        $result .= '<span id="gallery_' . self::$globalGalleryIndex . '_count">' . sizeof($galleryData->ImageIDs) . '</span>';
        for ($i = 0; $i < sizeof($galleryData->ImageIDs); $i++) {
            $imageData = VPImage::GetImageData($galleryData->ImageIDs[$i]);
            $file = self::$usePreviewImages ? $imageData->PreviewFile : $imageData->File;
            $result .= '<span id="gallery_' . self::$globalGalleryIndex . '_img_' . $i . '">' . $imageData->Photographer . ';/' . htmlspecialchars($file) . '</span>';

            if (array_search($imageData->Photographer, self::$listedPhotographers) === false) {
                self::$listedPhotographers[] = $imageData->Photographer;
                $result .= '<span id="photographer_' . $imageData->Photographer . '">' . VPUserData::GetUserName($imageData->Photographer) . '</span>';
            }

            if ($i == 0) {
                $displayImage = $file;
                $displayImgPhotograph = VPUserData::GetUserName($imageData->Photographer);
            }
        }

        $result .= '</div>';
        $result .= '<table style="width: 100%;"><tr>';

        $result .= '<td style="width: 15%; text-align: center; vertical-align: middle; cursor: pointer;" onclick="vp_gallery_previous(' . self::$globalGalleryIndex . ')">';
        $result .= '<img style="width: 50px; height: 50px;" src="' . VPConfig::$VP_INSTALL_DIR . 'Image/left.png"/></td>';

        $result .= '<td  style="width: 70%;"><img style="width: 100%;" src="/' . htmlspecialchars($displayImage) . '" id="gallery_' . self::$globalGalleryIndex . '_display"/><br>';
        $result .= '<span style="font-size: 12px;"><strong>' . $galleryData->Title . '</strong> [<span id="gallery_' . self::$globalGalleryIndex . '_currIndex">1</span>';
        $result .= '/' . sizeof($galleryData->ImageIDs) . '], Bild von <span id="gallery_' . self::$globalGalleryIndex . '_photographer">' . $displayImgPhotograph . '</span></span></td>';

        $result .= '<td style="width: 15%; text-align: center; vertical-align: middle; cursor: pointer;" onclick="vp_gallery_next(' . self::$globalGalleryIndex . ')">';
        $result .= '<img style="width: 50px; height: 50px;" src="' . VPConfig::$VP_INSTALL_DIR . 'Image/right.png"/></td>';

        $result .= '</tr></table>';


        if (!self::$galleryScriptIncluded) {
            self::$galleryScriptIncluded = true;
            $result .= '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_galleryDisplay.js" type="text/javascript"></script>';
        }

        self::$globalGalleryIndex++;
        return $result;
    }
}