<?php

require_once 'vp_config.php';

class VPDatabaseConn
{

    private static $VP_CONNECTION = null;

    public static function GetDatabaseConnection(): VPDatabaseConn
    {
        if (VPDatabaseConn::$VP_CONNECTION == null) {
            VPDatabaseConn::$VP_CONNECTION = new VPDatabaseConn();
        }
        return VPDatabaseConn::$VP_CONNECTION;
    }

    public static function SetUpTables()
    {
        $conn = VPDatabaseConn::GetDatabaseConnection();

        $conn->PerformQuery("CREATE TABLE `vp_users` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `Name` TEXT NOT NULL,
                `E-Mail` TEXT NOT NULL,
                `Telephone` TEXT NOT NULL,
                `Login` TEXT NOT NULL,
                `Password` TEXT NOT NULL,
                `Permissions`TEXT NOT NULL,
                `ResetPW` TINYINT NOT NULL DEFAULT '0',
                `PreferredLanguage` TEXT NOT NULL,
                `TimesLoggedIn` INT NOT NULL DEFAULT 0,
                `TimeOnline` INT NOT NULL DEFAULT 0,
                `DarkMode` TINYINT NOT NULL DEFAULT 0,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_articles` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `Timestamp` DATETIME NOT NULL,
                `Title` TEXT NOT NULL,
                `Text` LONGTEXT NOT NULL,
                `Tags` TEXT NOT NULL,
                `Author` VARCHAR(10) NOT NULL,
                `DisplayAuthor` VARCHAR(10) NOT NULL,
                `Published` TINYINT NOT NULL DEFAULT '0',
                `Thumbnail` VARCHAR(10) NOT NULL,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_images` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `File` TEXT NOT NULL,
                `PreviewFile` TEXT NOT NULL,
                `Title` TEXT NOT NULL,
                `Info` TEXT NOT NULL,
                `Photographer` VARCHAR(10) NOT NULL,
                `Owner` VARCHAR(10) NOT NULL,
                `Timestamp` DATETIME NOT NULL,
                `GalleryID` VARCHAR(10) NOT NULL DEFAULT '',
                `GalleryIndex` INT NOT NULL,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_tags` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `Name` TEXT NOT NULL,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_comments` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `UserID` VARCHAR(10) NOT NULL, 
                `ArticleID` VARCHAR(10) NOT NULL, 
                `Text` TEXT NOT NULL, 
                `ReplyTo` VARCHAR(10) NULL, 
                `Created` DATETIME NOT NULL, 
                `LastEdit` DATETIME NOT NULL, 
                `Revisions` INT NOT NULL, 
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_messages` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `SenderName` TEXT NOT NULL,
                `SenderEMail` TEXT NOT NULL,
                `Timestamp` DATETIME NOT NULL,
                `Subject` TEXT NOT NULL,
                `Message` TEXT NOT NULL,
                `Read` TINYINT NOT NULL,
                `Deleted` TINYINT NOT NULL,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_calendars` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `Name` TEXT NOT NULL,
                `OwnerID` VARCHAR(10) NOT NULL,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_dates` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `CalendarID` VARCHAR(10) NOT NULL,
                `Name` TEXT NOT NULL,
                `Date` DATETIME NOT NULL,
                `Info` TEXT NOT NULL,
                `CreatedBy` VARCHAR(10) NOT NULL,
                PRIMARY KEY(`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_galleries` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `Title` TEXT NOT NULL,
                `Info` TEXT NOT NULL,
                `Owner` VARCHAR(10) NOT NULL,
                `Timestamp` DATETIME NOT NULL,
                PRIMARY KEY (`ID`))");

        $conn->PerformQuery("CREATE TABLE `vp_apiKeys` (
                `ID` INT NOT NULL AUTO_INCREMENT,
                `Key` VARCHAR(20) NOT NULL,
                `Owner` VARCHAR(10) NOT NULL,
                `CreatedDate` DATETIME NOT NULL,
                `ExpireDate` DATETIME NULL,
                PRIMARY KEY (`ID`))");

        echo '<h1 style="color: green;">Successfully set up all VoltPages tables</h1>';
    }

    public static function EscapeSQLString($inp)
    {
        if (is_array($inp))
            return array_map(__METHOD__, $inp);
        if (!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }
        return $inp;
    }

    private $connection;

    private function __construct()
    {
        $this->connection = mysqli_connect(VPConfig::$VP_DATABASE_SERVER, VPConfig::$VP_DATABASE_USER, VPConfig::$VP_DATABASE_PASS, VPConfig::$VP_DATABASE_NAME);
        if ($this->connection == null) {
            echo '<h1>Failed to create database connection</h1><p>Please contact administrator</p>';
            echo VPConfig::$VP_DATABASE_USER . '@' . VPConfig::$VP_DATABASE_SERVER . '<br>Database Name:' . VPConfig::$VP_DATABASE_NAME . '<br>Password: ' . (strlen(VPConfig::$VP_DATABASE_PASS) > 0 ? 'yes' : 'no');
            exit;
        }
    }

    public function PerformQuery($sql)
    {
        return mysqli_query($this->connection, $sql);
    }

    public function PerformAndFetch($sql)
    {
        $result = mysqli_query($this->connection, $sql);
        if (mysqli_num_rows($result) == 0) return false;
        return mysqli_fetch_assoc($result);
    }

    public function GetMySQLiConnection()
    {
        return $this->connection;
    }

    public function CreateBackupSQL(): string
    {
        // https://cruso.de/source/php/mysql-backup/
        $tables = "*";
        $data = "\n/*---------------------------------------------------------------" .
            "\n  SQL DB BACKUP " . date("d.m.Y H:i") . " " .
            "\n  HOST: " . VPConfig::$VP_DATABASE_SERVER .
            "\n  DATABASE: " . VPConfig::$VP_DATABASE_NAME .
            "\n  TABLES: {$tables}" .
            "\n  ---------------------------------------------------------------*/\n";

        if ($tables == '*') { //get all of the tables
            $tables = array();
            $result = $this->PerformQuery("SHOW TABLES");
            while ($row = mysqli_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        foreach ($tables as $table) {
            $data .= "\n/*---------------------------------------------------------------" .
                "\n  TABLE: `{$table}`" .
                "\n  ---------------------------------------------------------------*/\n";
            $data .= "DROP TABLE IF EXISTS `{$table}`;\n";
            $res = $this->PerformQuery("SHOW CREATE TABLE `{$table}`");
            $row = mysqli_fetch_row($res);
            $data .= $row[1] . ";\n";

            $result = $this->PerformQuery("SELECT * FROM `{$table}`");
            $num_rows = mysqli_num_rows($result);

            if ($num_rows > 0) {
                $vals = Array();
                $z = 0;
                for ($i = 0; $i < $num_rows; $i++) {
                    $items = mysqli_fetch_row($result);
                    $vals[$z] = "(";
                    for ($j = 0; $j < count($items); $j++) {
                        if (isset($items[$j])) {
                            $vals[$z] .= "'" . self::EscapeSQLString($items[$j]) . "'";
                        } else {
                            $vals[$z] .= "NULL";
                        }
                        if ($j < (count($items) - 1)) {
                            $vals[$z] .= ",";
                        }
                    }
                    $vals[$z] .= ")";
                    $z++;
                }
                $data .= "INSERT INTO `{$table}` VALUES ";
                $data .= "  " . implode(";\nINSERT INTO `{$table}` VALUES ", $vals) . ";\n";
            }
        }
        return $data;
    }

}