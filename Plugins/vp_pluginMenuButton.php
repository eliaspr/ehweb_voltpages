<?php

class VPPluginMenuButton
{

    private $name, $icon, $link;
    private $requiredPermission;

    public function __construct($json)
    {
        $this->name = $json->name;
        $this->icon = $json->icon;
        $this->link = $json->link;

        if (property_exists($json, 'permission')) {
            $this->requiredPermission = $json->permission;
        } else {
            // 'u_' permissions are granted to everyone
            $this->requiredPermission = 'u_none';
        }
    }

    public function GetName()
    {
        return $this->name;
    }

    public function GetIcon()
    {
        return $this->icon;
    }

    public function GetLink()
    {
        return $this->link;
    }

    public function GetRequiredPermission()
    {
        return $this->requiredPermission;
    }


}