<?php

require_once 'vp_plugin.php';

class VPPluginManager
{

    private $plugins = array();

    public function LoadPlugin($pluginDir)
    {
        $plugin = new VPPlugin($pluginDir);
        $this->plugins[$plugin->GetID()] = $plugin;
    }

    public function GetPluginList(): array
    {
        return $this->plugins;
    }

    public function GetPluginCount(): int
    {
        return sizeof($this->plugins);
    }

    public function GetPlugin(string $id)
    {
        return array_key_exists($id, $this->plugins) ? $this->plugins[$id] : null;
    }

    public function ProcessPageRequest($path)
    {
        if (sizeof($path) == 0 || strlen($path[0]) == 0) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL);
            exit;
        }

        $pluginID = $path[0];
        $plugin = $this->GetPlugin($pluginID);

        if ($plugin == null) {
            echo '<p><b style="color: red;">' . VPLocale::Get("plugins.plugin-not-found") . '</b></p>';
        } else {
            $plugin->ProcessPageRequest(array_splice($path, 1));
        }
    }

}