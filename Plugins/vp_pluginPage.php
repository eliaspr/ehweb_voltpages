<?php

class VPPluginPage
{

    private $plugin;
    private $id, $phpFile, $permission;

    public function __construct(VPPlugin $plugin, $id, $phpFile, $permission)
    {
        $this->plugin = $plugin;
        $this->id = $id;
        $this->phpFile = $phpFile;
        $this->permission = $permission;
    }

    public function GetID(): string
    {
        return $this->id;
    }

    public function Show(array $path)
    {
        $permissions = VPPermissions::FromUserID(VPLogin::LoggedInUserID());
        if ($permissions->CheckPermission($this->permission)) {
            VPPluginUtil::$currentExecPlugin = $this->plugin;
            VPPluginUtil::$currentExecPath = $path;
            VPPluginUtil::$currentUserPermissions = $permissions;

            /** @noinspection PhpIncludeInspection */
            include($_SERVER['DOCUMENT_ROOT'] . $this->plugin->GetResource($this->phpFile));
        } else {
            VPPermissions::NoPermissionMessage();
        }
    }

}