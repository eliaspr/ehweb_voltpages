<?php

require_once 'vp_pluginMenuButton.php';
require_once 'vp_pluginPage.php';

class VPPlugin
{

    private $pluginDirectory;
    private $id, $name, $author, $version;
    private $menuButtons = array();
    private $pages = array();

    public function __construct($directory)
    {
        if ($directory[0] == '/')
            $directory = substr($directory, 1);
        if ($directory[strlen($directory) - 1] != '/')
            $directory .= '/';
        $this->pluginDirectory = $directory;

        $infoJsonFile = $directory . "plugin.json";
        if (!file_exists($infoJsonFile)) {
            echo '<p style="color: red; font-weight: bold;">Could not find plugin info json: ' . $infoJsonFile . '</p>';
            exit;
        }
        $infoJSON = json_decode(file_get_contents($infoJsonFile));

        $this->id = $infoJSON->id;
        $this->name = $infoJSON->name;
        $this->author = $infoJSON->author;
        $this->version = $infoJSON->version;

        if (property_exists($infoJSON, 'tables')) {
            $tablesJSON = $infoJSON->tables;
            foreach ($tablesJSON as $tableJSON) {
                $tableName = $tableJSON->name;
                $sql = "CREATE TABLE IF NOT EXISTS `$tableName` (`ID` INT NOT NULL AUTO_INCREMENT,";
                if (property_exists($tableJSON, 'columns')) {
                    foreach ($tableJSON->columns as $col)
                        $sql .= $col . ', ';
                }
                $sql .= "PRIMARY KEY (`ID`))";

                VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            }
        }

        if (property_exists($infoJSON, 'menu')) {
            $menuJSON = $infoJSON->menu;
            foreach ($menuJSON as $menu) {
                $this->menuButtons[] = new VPPluginMenuButton($menu);
            }
        }

        if (property_exists($infoJSON, 'pages')) {
            $pagesJSON = $infoJSON->pages;
            $this->ParsePageArray($pagesJSON);
        }
    }

    private function ParsePageArray($pagesJSON)
    {
        foreach ($pagesJSON as $page) {
            if (gettype($page) == 'array')
                $this->ParsePageArray($page);
            else {
                $pageID = $page->id;
                $permission = property_exists($page, 'permission') ? $page->permission : 'u_none';
                $this->pages[$pageID] = new VPPluginPage($this, $pageID, $page->php, $permission);
            }
        }
    }

    public function GetID(): string
    {
        return $this->id;
    }

    public function GetName(): string
    {
        return $this->name;
    }

    public function GetAuthor(): string
    {
        return $this->author;
    }

    public function GetVersion(): string
    {
        return $this->version;
    }

    public function GetResource($path): string
    {
        return '/' . $this->pluginDirectory . $path;
    }

    public function GetMenuButtons(): array
    {
        return $this->menuButtons;
    }

    public function GetMenuButtonCount(): int
    {
        return sizeof($this->menuButtons);
    }

    public function ProcessPageRequest($path)
    {
        if (sizeof($path) == 0 || strlen($path[0]) == 0) {
            $pageID = "home";
        } else {
            $pageID = $path[0];
        }

        if (array_key_exists($pageID, $this->pages)) {
            $page = $this->pages[$pageID];
            $page->Show($path);
        } else {
            echo '<p><b style="color: red;">' . VPLocale::Get("plugins.page-not-found") . '</b></p>';
        }
    }

}

class VPPluginUtil
{

    public static $currentExecPlugin = null;
    public static $currentExecPath = null;
    public static $currentUserPermissions = null;

    public static function Resource($path): string
    {
        if ($path[0] == '/')
            $path = substr($path, 1);

        return self::$currentExecPlugin == null ? ('/' . $path) : self::$currentExecPlugin->GetResource($path);
    }

    public static function ResourceVP($path): string
    {
        if ($path[0] == '/')
            $path = substr($path, 1);

        return VPConfig::$VP_INSTALL_DIR . $path;
    }

    public static function IconVP($icon): string
    {
        return self::ResourceVP('Image/Icon/' . $icon);
    }

    public static function Path(): array
    {
        return self::$currentExecPath == null ? array() : self::$currentExecPath;
    }

    public static function Link(string $link): string
    {
        return VPConfig::$VP_REDIRECT_URL . '/plugin/' . self::$currentExecPlugin->GetID() . '/' . $link;
    }

    public static function CreateButton($icon, $text, $link, $permission = "u_none")
    {
        if (!self::$currentUserPermissions->CheckPermission($permission)) return;
        $fullIconPath = self::Resource($icon);
        echo '<a href="' . self::Link($link) . '"><button class="vp_dashboard_button""><img src="' . $fullIconPath . '"/>' . $text . '</button></a>';
    }

    public static function Redirect(string $link)
    {
        header("Location: " . self::Link($link));
        exit;
    }

    public static function Permissions(): VPPermissions
    {
        return self::$currentUserPermissions;
    }

    public static function Plugin(): VPPlugin
    {
        return self::$currentExecPlugin;
    }

}