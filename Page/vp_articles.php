<?php

require_once 'vp_logger.php';
require_once 'vp_pageUtil.php';

class VPArticlesPage
{

    public static function ShowArticlesPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if (sizeof($path) == 0) {
            if ($permissions->CanViewArticleList()) {
                VPArticlesPage::ShowOverviewPage($path, $permissions);
            } else {
                VPPermissions::NoPermissionMessage();
            }
        } else {
            $page = $path[0];

            if ($page == "edit") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditArticle($path[1])) {
                        VPArticlesPage::ShowEditPage(array_splice($path, 1), $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/photos");
                    exit;
                }
            } else if ($page == "delete") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditArticle($path[1])) {
                        VPArticlesPage::ShowOverviewPage($path, $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/articles");
                    exit;
                }
            } else if ($page == "create") {
                if ($permissions->CanCreateArticles()) {
                    VPArticlesPage::CreateArticle();
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else if ($page == "create_tag") {
                if ($permissions->CanCreateArticleTags()) {
                    VPArticlesPage::CreateTag();
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/articles");
                exit;
            }
        }
    }

    private static function CreateTag()
    {
        if (isset($_GET['tagName'])) {
            $name = VPDatabaseConn::EscapeSQLString(trim($_GET['tagName']));
            $sql = "SELECT `ID` FROM `vp_tags` WHERE `Name` = '$name'";
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch(utf8_decode($sql));
            if ($result == null) {
                $sql = "INSERT INTO `vp_tags` (`ID`, `Name`) VALUES (NULL, '$name')";
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            }
            VPLogger::GetLogger()->LogUserActivity("created new tag {TagName=$name}");
        }
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/articles");
        exit;
    }

    private static function CreateArticle()
    {
        $userID = VPLogin::LoggedInUserID();
        $sqlValues = "NULL, NOW(), '', '', '', '$userID', '$userID', '0', ''";
        $sql = "INSERT INTO `vp_articles` (`ID`, `Timestamp`, `Title`, `Text`, `Tags`, `Author`, `DisplayAuthor`, `Published`, `Thumbnail`) VALUES ($sqlValues)";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        $sql = "SELECT `ID` FROM `vp_articles` ORDER BY `ID` DESC LIMIT 1";
        $newestID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['ID'];
        VPLogger::GetLogger()->LogUserActivity("created new article {ID=$newestID}");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/articles/edit/$newestID");
        exit;
    }

    private static function ShowOverviewPage($path, $permissions)
    {
        // 0 - no selector
        // 1 - write comment
        // 2 - view comments
        $articleSelector = 0;
        if (isset($_GET['for'])) {
            if ($_GET['for'] == "writeComment") {
                $articleSelector = 1;
            } else if ($_GET['for'] == "viewComments") {
                $articleSelector = 2;
            }
        }

        $dateFrom = isset($_GET['since']) ? $_GET['since'] : "";
        $articleCount = isset($_GET['count']) ? $_GET['count'] : "";
        $textSearch = isset($_GET['search']) ? $_GET['search'] : "";

        if ($dateFrom . $articleCount . $textSearch == "") {
            if (isset($_SESSION['vp_news_search'])) {
                $searchInfo = $_SESSION['vp_news_search'];
                $spl = explode(";", $searchInfo);
                switch ($spl[0]) {
                    case "since":
                        $dateFrom = $spl[1];
                        break;
                    case "count":
                        $articleCount = $spl[1];
                        break;
                    case "search":
                        $textSearch = $spl[1];
                        break;
                }
            } else {
                // No user specified search
                $articleCount = "10";
            }
        }

        $hiddenInputs = "";
        switch ($articleSelector) {
            case 0:
                {
                    if ($permissions->CanCreateArticles()) {
                        require_once 'vp_pageUtil.php';
                        VPPageUtil::CreateNewArticleButton();
                    }
                    echo '<h1>' . VPLocale::Get("articles.header.default") . '</h1>';
                    break;
                }
            case 1:
                {
                    echo '<h1>' . VPLocale::Get("articles.header.write-comment") . '</h1><p><a href="' . VPConfig::$VP_REDIRECT_URL . '/comments">Zurück</a></p>';
                    $hiddenInputs = '<input hidden style="display: none;" name="for" value="writeComment"/>';
                    break;
                }
            case 2:
                {
                    echo '<h1>' . VPLocale::Get("articles.header.view-comments") . '</h1><p><a href="' . VPConfig::$VP_REDIRECT_URL . '/comments">Zurück</a></p>';
                    $hiddenInputs = '<input hidden style="display: none;" name="for" value="viewComments"/>';
                    break;
                }
        }

        if (sizeof($path) >= 2 && $path[0] == "delete") {
            $articleID = $path[1];
            $articleInfo = VPArticle::GetArticle($articleID);
            $deleteSQL = "DELETE FROM `vp_articles` WHERE `ID` = '$articleID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($deleteSQL);
            $deleteSQL = "DELETE FROM `vp_comments` WHERE `ArticleID` = '$articleID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($deleteSQL);
            echo '<p class="vp_info_box">' . VPLocale::Get("articles.message.deleted", array($articleInfo->Title)) . '</p>';
            VPLogger::GetLogger()->LogUserActivity("deleted article {ID=$articleID}");
            if (!$permissions->CanViewArticleList()) return;
        }

        $formAction = VPConfig::$VP_REDIRECT_URL . '/articles';
        echo '<div class="vp_search_panel"><table><tr>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($dateFrom) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("articles.search.since", array('<input type="date" name="since" value="' . $dateFrom . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>' . $hiddenInputs . '
                    </form></td>';
        echo '  <td style="width: 34%;"><form method="get"' . (strlen($articleCount) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("articles.search.newest", array('<input type="number" name="count" style="width: 50px;" value="' . ($articleCount == 0 ? "10" : $articleCount) . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>' . $hiddenInputs . '
                    </form></td>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($textSearch) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("articles.search.text", array('<input type="text" name="search" value="' . $textSearch . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.search") . '</button>' . $hiddenInputs . '
                    </form></td>';
        echo '</tr></table></div>';

        if ($dateFrom != "") {
            $dateFrom = VPDatabaseConn::EscapeSQLString($dateFrom);
            $sql = "SELECT * FROM `vp_articles` WHERE `Timestamp` IS NOT NULL AND `Timestamp` > '$dateFrom' ORDER BY `Timestamp` DESC LIMIT 100";
            $searchInfo = "since;$dateFrom";
        } else if ($articleCount != "") {
            if ($articleCount < 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.negative") . '</h2>';
                return;
            }
            $articleCount = VPDatabaseConn::EscapeSQLString($articleCount);
            $sql = "SELECT * FROM `vp_articles` ORDER BY `Timestamp` DESC LIMIT $articleCount";
            $searchInfo = "count;$articleCount";
        } else if ($textSearch != "") {
            $textSearch = trim($textSearch);
            if (strlen($textSearch) == 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.empty") . '</h2>';
                return;
            }
            $textSearch = VPDatabaseConn::EscapeSQLString($textSearch);
            $sql = "SELECT * FROM `vp_articles` WHERE `Title` LIKE '%$textSearch%' OR `Text` LIKE '%$textSearch%' ORDER BY `Timestamp` DESC LIMIT 100";
            $searchInfo = "search;$textSearch";
        } else {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.null") . '</h2>';
            return;
        }

        $_SESSION['vp_news_search'] = $searchInfo;
        $timer = new VPTimer();
        $timer->Start();
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
        $searchTime = $timer->Stop();

        $resultCount = mysqli_num_rows($result);
        if ($resultCount == 0) {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.no-results") . '</h2>';
        } else {
            require_once __DIR__ . "/../Dependencies/Parsedown.php";
            require_once __DIR__ . "/../Dependencies/html2text.php";

            echo '<p class="vp_search_result_count">' . VPLocale::Get($resultCount == 1 ? "search.result-count-one" : "search.result-count", [mysqli_num_rows($result), $searchTime]) . '</p>';

            while ($articleRow = mysqli_fetch_assoc($result)) {
                $article = VPArticle::GetArticleFromRow($articleRow);
                $canEdit = $permissions->CanEditArticle($article->ID);

                $thumbnailImage = VPImage::GetImageData($article->ThumbnailID);

                echo '<div class="vp_article_list_box_wrapper">';

                switch ($articleSelector) {
                    case 0:
                        {
                            if ($canEdit)
                                echo '<a class="vp_insible_link" href="' . VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $article->ID . '">';
                            break;
                        }
                    case 1:
                        {
                            echo '<a class="vp_insible_link" href="' . VPConfig::$VP_REDIRECT_URL . '/comments/write?article=' . $article->ID . '">';
                            break;
                        }
                    case 2:
                        {
                            echo '<a class="vp_insible_link" href="' . VPConfig::$VP_REDIRECT_URL . '/comments/?article=' . $article->ID . '">';
                            break;
                        }
                }

                echo '<div class="vp_article_list_box">';

                if ($thumbnailImage->WasImageFound()) {
                    $thumbnailStyle = 'background-image: url(/' . htmlspecialchars($thumbnailImage->PreviewFile) . ');';
                } else {
                    $thumbnailStyle = 'background-image : url(' . VPConfig::$VP_INSTALL_DIR . 'Image/img-placeholder' . (VPConfig::IsDarkModeEnabled() ? '-dark' : '') . '.png);';
                }
                echo '<div class="vp_article_list_thumbnail" style="' . $thumbnailStyle . '"></div>';

                echo '<div class="vp_article_list_text">';
                echo '<h2 style="margin-top: 0px; margin-bottom: 0px;">' . $article->Title . '</h2>';

                echo '<div style="font-size: 13px;">' . $article->TimestampFormatted . ' | ' . VPUserData::GetUserName($article->DisplayAuthorID);
                {
                    $tagList = $article->TagIDs;
                    if (sizeof($tagList) > 0) {
                        echo '<br>';
                        for ($i = 0; $i < sizeof($tagList); $i++) {
                            echo VPArticle::GetTagName($tagList[$i]);
                            if ($i < sizeof($tagList) - 1) {
                                echo ', ';
                            }
                        }
                    }
                }
                if (!$article->Published) {
                    echo '<br><b style="color: red;">' . VPLocale::Get("articles.not-public") . '</b>';
                }
                if ($articleSelector == 0 && !$canEdit) {
                    echo '<br><b>' . VPLocale::Get("articles.cant-edit") . '</b>';
                }
                echo '</div>';

                {
                    $markdownParser = new Parsedown;
                    $parsedText = $markdownParser->text($article->Text);
                    $plainText = convert_html_to_text($parsedText);
                    if (strlen($plainText) > 200) {
                        $plainText = substr($plainText, 0, 197) . "...";
                    }
                    echo '<div class="vp_article_list_text_teaser"><p>' . $plainText . '</p></div>';
                }
                echo '</div>';

                echo '</div>';
                if ($canEdit) echo '</a>';
                echo '</div>';
            }
        }

        if ($articleSelector == 0) {
            echo '<div class="vp_search_panel" style="margin-top: 80px; padding-bottom: 15px;"><h2 style="margin-top: 0px;">' . VPLocale::Get("tags.header") . '</h2><p>';
            $sql = "SELECT `Name`, `ID` FROM `vp_tags` ORDER BY `Name` ASC";
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            $i = 0;
            $m = mysqli_num_rows($result);
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<span title="ID: ' . $row['ID'] . '">' . utf8_encode($row['Name']) . '</span>';
                if (++$i < $m) echo ', ';
            }
            echo '</p>';
            {
                if ($permissions->CanCreateArticleTags()) {
                    echo '<span id="vp_newtag_redirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/articles/create_tag?tagName=</span>';
                    echo '<button type="button" onClick="vp_articleNewTag()">' . VPLocale::Get("tags.new") . '</button>';
                }
            }
            echo '</div><div style="height:100px;"></div>';
        }

        echo '<span style="display: none;" id="vp_text_newTagName">' . VPLocale::Get("tags.new-tag-name") . '</span>';
        echo '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_articleList.js"></script>';
    }

    private static function ShowEditPage($path, VPPermissions $permissions)
    {
        $articleID = $path[0];

        echo '<span style="display:none;" id="vp_text_tooManyChars">' . VPLocale::Get("articles.edit-notice.too-many-chars") . '</span>';
        echo '<span style="display:none;" id="vp_text_saveChanges">' . VPLocale::Get("general.save-changes") . '</span>';
        echo '<span style="display:none;" id="vp_text_deleteWarning">' . VPLocale::Get("articles.delete-warning") . '</span>';
        echo '<span style="display:none;" id="vp_text_noTags">' . VPLocale::Get("articles.no-tags") . '</span>';
        echo '<span style="display:none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved-changes") . '</span>';

        echo '<h1>' . VPLocale::Get("articles.edit") . '</h1>';
        echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/articles">' . VPLocale::Get("general.back-overview") . '</a>';
        if (VPConfig::$VP_WEBSITE_PAGE_ARTICLE != null) {
            echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
            $articleData = VPArticle::GetArticle($articleID);
            echo '<a target="_blank" href="' . $articleData->HumanLink . '">' . VPLocale::Get("articles.view-website") . '</a>';
        }
        echo '</p>';

        if (isset($_POST['article_update']) && $_POST['article_update'] == "true") {
            $a_text = str_replace("\r\n", "\n", VPDatabaseConn::EscapeSQLString($_POST['article_text']));
            $a_title = VPDatabaseConn::EscapeSQLString($_POST['article_title']);
            $a_author = VPDatabaseConn::EscapeSQLString($_POST['article_author']);
            $a_public = (isset($_POST['article_public']) && $_POST['article_public'] == "on") ? 1 : 0;
            $a_tags = VPDatabaseConn::EscapeSQLString($_POST['article_tags']);

            $sql = "UPDATE `vp_articles` SET `Title` = '$a_title', `Text` = '$a_text', `DisplayAuthor` = '$a_author', `Published` = '$a_public', `Tags` = '$a_tags' WHERE `ID` = '$articleID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            echo '<p class="vp_info_box">' . VPLocale::Get("articles.message.updated") . '</p>';
            VPLogger::GetLogger()->LogUserActivity("modified article {ID=$articleID}");

            if ($permissions->CanChangeArticleTimestamps()) {
                $newTime = VPDatabaseConn::EscapeSQLString($_POST['article_timestamp']);
                $sql = "UPDATE `vp_articles` SET `Timestamp` = '$newTime' WHERE `ID` = '$articleID'";
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            }
        }

        if (isset($_GET['thumbnailID'])) {
            $thumbnailID = $_GET['thumbnailID'];
            if ($thumbnailID == 'remove') {
                $sql = "UPDATE `vp_articles` SET `Thumbnail` = '' WHERE `ID` = '$articleID'";
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
                echo '<p class="vp_info_box">' . VPLocale::Get("articles.message.thumbnail-removed") . '</p>';
            } else {
                $sql = "UPDATE `vp_articles` SET `Thumbnail` = '$thumbnailID' WHERE `ID` = '$articleID'";
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
                echo '<p class="vp_info_box">' . VPLocale::Get("articles.message.thumbnail-updated") . '</p>';
            }
        }

        $articleData = VPArticle::GetArticle($articleID);

        echo '<span id="vp_delete_redirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/articles/delete/' . $articleID . '</span>';
        echo '<form method="post" id="vp_articleForm" action="' . VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $articleID . '">';
        echo '<input name="article_update" value="true" type="text" hidden/><div class="vp_edit_box"><div class="vp_edit_box_header">';
        echo VPLocale::Get("articles.article-data") . '<span style="float: right">';

        echo '<span class="vp_delete_button" onClick="vp_articleDelete()">' . VPLocale::Get("general.delete") . '</span>';
        echo '<span class="vp_save_button" onClick="vp_articleSave()">' . VPLocale::Get("general.save") . '</span></span>';
        echo '</div><div class="vp_edit_box_body"><table>';

        echo '<tr><td>' . VPLocale::Get("articles.attribs.created-date");
        echo '</td><td><input id="articleTimestampDisplay" type="text" value="' . $articleData->TimestampFormatted . '" disabled/>';
        if ($permissions->CanChangeArticleTimestamps()) {
            $time = strtotime($articleData->Timestamp);
            $currentDatetime = date("Y-m-d", $time) . 'T' . date("H:i", $time);
            echo '<input id="articleTimestampEdit" name="article_timestamp" type="datetime-local" value="' . $currentDatetime . '" style="display: none;"/>';
            echo '&nbsp;<button id="articleTimestampBtn" onclick="vp_articleEditDate()" type="button">' . VPLocale::Get("articles.edit-timestamp") . '</button>';
        }
        echo '</td></tr>';

        echo '<tr><td>' . VPLocale::Get("articles.attribs.created-by") . '</td><td><input type="text" value="' . VPUserData::GetUserName($articleData->AuthorID) . '" disabled/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("articles.attribs.link") . '</td><td><input type="text" value="' . VPConfig::$VP_PUBLIC_WEBSITE_URL . $articleData->HumanLink . '" style="width: 50%;" disabled id="vp_articleLink"/>&nbsp;<button type="button" onclick="vp_copyClipboard(\'vp_articleLink\')" title="' . VPLocale::Get('general.copy-clipboard') . '">' . VPLocale::Get('general.copy') . '</button></td></tr>';
        echo '<tr><td>' . VPLocale::Get("articles.attribs.title") . '</td><td><input type="text" onInput="vp_articleInput()" name="article_title" value="' . htmlspecialchars($articleData->Title) . '" style="width: 50%;"/><span style="margin-left: 10px; font-size: 10px;">' . VPLocale::Get("articles.edit-notice.max-chars", array("200")) . '</span></td></tr>';

        echo '<tr><td>' . VPLocale::Get("articles.attribs.published") . '</td><td><input type="checkbox" name="article_public" id="chbx_public" class="vp_fancy_checkbox" onChange="vp_articleInput()"' . ($articleData->Published ? " checked" : "") . '/><label for="chbx_public" class="vp_fancy_checkbox_label"></label>';
        if (!$articleData->Published) {
            echo '<span style="margin-left: 5px; font-size: 12px; color: red;">' . VPLocale::Get("articles.edit-notice.publish") . '</span>';
        }
        echo '</td></tr>';

        require_once 'vp_pageUtil.php';
        echo '<tr><td>' . VPLocale::Get("articles.attribs.author") . '</td><td>';
        VPPageUtil::CreateUserDropdown("article_author", true, $articleData->DisplayAuthorID, "vp_input_author");
        echo '<span style="margin-left: 10px; font-size: 12px;">' . VPLocale::Get("articles.edit-notice.author") . '</span></td></tr>';

        echo '<tr><td colspan="2" style="height: 8px;"></td></tr>';
        echo '<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">';
        echo '<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>';
        echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("articles.attribs.text") . '</td><td style="width: 100%;" id="vp_article_text_wrap">' .
            '<textarea id="vp_article_text" type="text" name="article_text">' .
            htmlspecialchars($articleData->Text) . '</textarea></td></tr>';

        echo '<tr><td colspan="2" style="height: 8px;"></td></tr>';
        echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("articles.attribs.tags") . '</td><td>';
        $tagList = "";
        echo '<div style="display: none;" id="vp_tagHiddenData">';
        foreach ($articleData->TagIDs as $tag) {
            $tagList .= $tag . ";";
            echo '<span style="display: none;" id="vp_tagName_' . $tag . '">' . VPArticle::GetTagName($tag) . '</span>';
        }
        echo '<span style="display: none;" id="delete_image_url">' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Delete.png</span></div>';
        if (strlen($tagList) > 0 && $tagList[strlen($tagList) - 1] == ";") $tagList = substr($tagList, 0, -1);
        echo '<input id="vp_article_tags" name="article_tags" value=";' . $tagList . ';" hidden style="display: none;"/>';
        require_once 'vp_pageUtil.php';
        VPPageUtil::CreateTagDropdown("DUMMY_vp_tag_selector", $permissions, true, -1, "vp_tag_selector");
        echo '<button type="button" style="margin-left: 5px;" onClick="vp_articles_addTagFromDropdown()">' . VPLocale::Get("articles.add-tag") . '</button><br>';
        echo '<div style="width: 70%; padding: 4px; border: 1px solid black; margin-top: 3px;"><div id="vp_article_tagDisplay"></div></div>';
        echo '</td></tr>';

        echo '<tr><td colspan="2" style="height: 13px;"></td></tr>';
        echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("articles.attribs.thumbnail") . '</td><td style="vertical-align: top;">';
        {
            $thumbnailData = VPImage::GetImageData($articleData->ThumbnailID);
            if ($thumbnailData->WasImageFound()) {
                $imagePath = "/" . $thumbnailData->PreviewFile;
            } else {
                $imagePath = VPConfig::$VP_INSTALL_DIR . "Image/img-placeholder" . (VPConfig::IsDarkModeEnabled() ? '-dark' : '') . ".png";
            }
            $imagePath = htmlspecialchars($imagePath);
            echo '<span style="display: none;" id="vp_redirectURL">' . VPConfig::$VP_REDIRECT_URL . '</span>';
            echo '<table><tr><td>';
            if ($thumbnailData->WasImageFound()) echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/photos/edit/' . $articleData->ThumbnailID . '?cameFrom=article&articleID=' . $articleID . '">';
            echo '<img src="' . $imagePath . '" style="width: 200px; height: auto; border: 1px solid black;"/>';
            if ($thumbnailData->WasImageFound()) echo '</a>';
            echo '</td>';
            echo '<td><button type="button" onclick="vp_articleChooseThumbnail(' . $articleID . ')">' . VPLocale::Get("articles.select-thumbnail") . '</button><br>';
            echo '<button type="button" onclick="vp_articleRemoveThumbnail(' . $articleID . ')"' . ($thumbnailData->WasImageFound() ? "" : "disabled") . '>' . VPLocale::Get("articles.remove-thumbnail") . '</button>';
            echo '</td></tr></table>';
        }
        echo '</td></tr>';

        echo '</table></div></div></form>';

        $comments = VPComment::GetCommentsOnArticle($articleID);
        if (sizeof($comments) > 0) {
            echo '<h1 style="margin-top: 50px;" id="comments">' . VPLocale::Get("comments.header") . '</h1><div style="width: 700px;">';
            $index = 0;
            require_once __DIR__ . "/../Dependencies/Parsedown.php";
            $markdownParser = new Parsedown;
            foreach ($comments as $commentInfo) {
                if ($index++ > 0) echo '<hr>';

                $articleTitle = VPArticle::GetArticleTitle($commentInfo->ArticleID);
                echo '<p>' . VPLocale::Get("comments.comment-header", array($commentInfo->GetUserName(), $commentInfo->CreatedFormattedLong, $articleTitle));
                echo '<div style="margin-left: 20px;">' . $markdownParser->text($commentInfo->Text) . '</div>';
                echo '</p>';
            }
            echo '</div><div style="height: 40px;"></div>';
        }

        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_articleEdit.js"></script>';
    }

}