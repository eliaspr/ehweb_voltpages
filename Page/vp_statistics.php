<?php

class VPStatsPage
{

    private static function Count($sql): int
    {
        return mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql));
    }

    private static function FormatNumber($number)
    {
        if ($number < 1000)
            return $number;

        $number /= 1000;
        if ($number < 1000) {
            $thousands = intval($number);
            $decimals = intval($number * 10 % 10);
            return $thousands . "," . $decimals . 'k';
        }

        $number /= 1000;
        if ($number < 1000) {
            $thousands = intval($number);
            $decimals = intval($number * 10 % 10);
            return $thousands . "," . $decimals . 'M';
        }

        $number /= 1000;
        $thousands = intval($number);
        $decimals = intval($number * 10 % 10);
        return $thousands . "," . $decimals . 'B';
    }

    private static function FormatTimeInterval($minutes, $mouseover = false)
    {
        if ($minutes < 60 && !$mouseover)
            return $minutes . ' min';

        if ($minutes < (24 * 60) || $mouseover) {
            $hours = intval($minutes / 60);
            $minutes %= 60;
            $minutes = $minutes < 10 ? '0' . $minutes : $minutes;
            return "$hours:$minutes h";
        }

        $days = $minutes / (24 * 60);
        return intval($days) . ',' . intval($days * 10 % 10) . ' Tage';
    }

    private static function ShowStat($langID, $value, $total = null, $format = 'number')
    {
        switch ($format) {
            case 'number':
                $valueFormatted = self::FormatNumber($value);
                $valueMouseover = $value;
                break;
            case 'time':
                $valueFormatted = self::FormatTimeInterval($value);
                $valueMouseover = self::FormatTimeInterval($value, true);
                break;
            default:
                $valueFormatted = htmlspecialchars('<unknown format: ' . $format . '>');
        }

        echo '<tr><td>' . VPLocale::Get('stats.names.' . $langID) . ':</td> <td></td>';
        echo '<td style="text-align: right;" title="' . $valueMouseover . '"><strong>' . $valueFormatted . '</strong></td><td style="vertical-align: bottom;">';
        if ($total !== null) {
            $percentOwn = $total == 0 ? 0 : intval($value / $total * 100);
            echo '<span style="margin-left: 10px; font-size: 12px;">' . $percentOwn . ' %</span>';
        }
        echo '</td>';
        if ($total !== null) {
            $percentOwn = $total == 0 ? 0 : intval($value / $total * 100);
            $percentOther = 100 - $percentOwn;
            echo '<td></td><td><table style="width: 100%;"><tr>';
            echo '<td style="background: red; width: ' . $percentOwn . '%; height: 1em;" title="' . VPLocale::Get("stats.your-value-mouseover", [$value, $percentOwn]) . '"></td>';
            echo '<td style="background: #444444; width: ' . $percentOther . '%; height: 1em;" title="' . VPLocale::Get("stats.total-website-mouseover", [$total]) . '"></td>';
            echo '</tr></table></td>';
        }
        echo '</tr>';
    }

    public static function ShowStats($userID)
    {
        echo '<div class="vp_edit_box"><div class="vp_edit_box_header">' . VPLocale::Get("stats.header") . '</div><div class="vp_edit_box_body"><table>';
        echo '<tr><th>' . VPLocale::Get("stats.statistic") . '</th> <td style="width: 18px;"></td> <th colspan="2">' . VPLocale::Get("stats.your-value") . '</th> <td style="width: 25px;"></td>';
        echo '<td style="font-size: 11px; padding: 3px; font-weight: bold; margin-top: 0;"><span style="color: red;">' . VPLocale::Get("stats.your-value") . '</span> | ';
        echo '<span style="color: #444444;">' . VPLocale::Get("stats.total-website") . '</span></td></tr>';

        $sql = "SELECT TimesLoggedIn, TimeOnline FROM vp_users WHERE ID = $userID";
        $userData = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);
        self::ShowStat("times-logged-in", $userData['TimesLoggedIn']);
        self::ShowStat("online-time", intval($userData['TimeOnline'] / 60), null, 'time');

        $allArticles = VPDatabaseConn::GetDatabaseConnection()->PerformQuery("SELECT Text, DisplayAuthor FROM vp_articles");
        self::ShowStat('articles-created', self::Count("SELECT ID FROM vp_articles WHERE Author = $userID"), mysqli_num_rows($allArticles));
        self::ShowStat('articles-author', self::Count("SELECT ID FROM vp_articles WHERE DisplayAuthor = $userID"), mysqli_num_rows($allArticles));

        $totalChars = 0;
        $charsWritten = 0;
        while ($row = mysqli_fetch_assoc($allArticles)) {
            $textlen = strlen($row['Text']);
            $totalChars += $textlen;
            if ($row['DisplayAuthor'] == $userID)
                $charsWritten += $textlen;
        }
        self::ShowStat('articles-characters', $charsWritten, $totalChars);

        $totalComments = self::Count("SELECT ID FROM vp_comments");
        self::ShowStat('comments-written', self::Count("SELECT ID FROM vp_comments WHERE UserID = $userID"), $totalComments);

        $totalPhotos = self::Count("SELECT ID FROM vp_images WHERE GalleryID = ''");
        self::ShowStat('photos-uploaded', self::Count("SELECT ID FROM vp_images WHERE Owner = $userID AND GalleryID = ''"), $totalPhotos);
        self::ShowStat('photos-shot', self::Count("SELECT ID FROM vp_images WHERE Photographer = $userID AND GalleryID = ''"), $totalPhotos);

        $totalCalendars = self::Count("SELECT ID FROM vp_calendars");
        self::ShowStat('calendars-created', self::Count("SELECT ID FROM vp_calendars WHERE OwnerID = $userID"), $totalCalendars);

        $totalDates = self::Count("SELECT ID FROM vp_dates");
        self::ShowStat('dates-created', self::Count("SELECT ID FROM vp_dates WHERE CreatedBy = $userID"), $totalDates);

        $totalGalleries = self::Count("SELECT ID FROM vp_galleries");
        self::ShowStat('galleries-created', self::Count("SELECT ID FROM vp_galleries WHERE `Owner` = $userID"), $totalGalleries);

        $totalPhotos = self::Count("SELECT ID FROM vp_images WHERE GalleryID <> ''");
        self::ShowStat('gallery-photos-uploaded', self::Count("SELECT ID FROM vp_images WHERE Owner = $userID AND GalleryID <> ''"), $totalPhotos);
        self::ShowStat('gallery-photos-shot', self::Count("SELECT ID FROM vp_images WHERE Photographer = $userID AND GalleryID <> ''"), $totalPhotos);

        echo '</table></div></div>';
    }

}