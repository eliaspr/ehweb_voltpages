<?php
$galleryMode = isset($_GET['gallery']);
if ($galleryMode) {
    require_once __DIR__ . '/../vp_galleries.php';
    $galleryID = VPDatabaseConn::EscapeSQLString($_GET['gallery']);
    $galleryData = VPGallery::GetGalleryData($galleryID);
}
?>

<span id="vp_text_imageUploaded" style="display: none"><?php echo VPLocale::Get("photos.image-uploaded") ?></span>
<link rel="stylesheet" type="text/css" href="<?php echo VPConfig::$VP_INSTALL_DIR; ?>Dependencies/dropzone.css"/>
<script type="text/javascript" src="<?php echo VPConfig::$VP_INSTALL_DIR; ?>Dependencies/dropzone.js"></script>
<script>
    <?php
    $entries = array("dictDefaultMessage", "dictFallbackMessage", "dictFallbackText", "dictFileTooBig", "dictInvalidFileType",
        "dictResponseError", "dictCancelUpload", "dictCancelUploadConfirmation", "dictRemoveFile", "dictMaxFilesExceeded");
    foreach ($entries as $i) {
        $replacements = array();
        if ($i == "dictDefaultMessage") $replacements[] = "20 MB";
        echo 'Dropzone.prototype.defaultOptions.' . $i . ' = "' . VPLocale::Get("photos.dropzone.$i", $replacements) . '";' . PHP_EOL;
    }
    ?>
</script>
<script type="text/javascript" src="<?php echo VPConfig::$VP_INSTALL_DIR; ?>Scripts/vp_photoUpload.js"></script>

<h1><?php
    if ($galleryMode)
        echo VPLocale::Get("galleries.upload", [$galleryData->Title]);
    else
        echo VPLocale::Get("photos.upload")
    ?></h1>

<p>
    <a href="<?php
    if ($galleryMode)
        echo VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID;
    else
        echo VPConfig::$VP_REDIRECT_URL . '/photos'; ?>"><?php echo VPLocale::Get("general.done") ?></a><br><br>
</p>

<form action="<?php
if ($galleryMode)
    echo VPConfig::$VP_REDIRECT_URL . '/photos/upload?gallery=' . $galleryID;
else
    echo VPConfig::$VP_REDIRECT_URL . '/photos/upload'; ?>" class="dropzone"
      id="photo_upload_dropzone">
    <div class="fallback">
        <input name="file" type="file" multiple/>
    </div>
</form>

<div id="vp_uploaded_files" style="margin-top: 50px;"></div>