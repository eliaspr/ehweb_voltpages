<?php /** @noinspection PhpIncludeInspection */

class VPLogbook
{

    public static function ShowLogbookPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if ($permissions->CanViewLogbook()) {
            if (sizeof($path) == 0) {
                echo '<h1>'.VPLocale::Get("dashboard.sidebar.logbook").'</h1>';
                $files = scandir(VPConfig::GetLogDirectory());
                $assoc = array();
                foreach ($files as $file) {
                    if ($file[0] == '.') continue;
                    $assoc[strtotime($file)] = $file;
                }
                krsort($assoc);

                foreach ($assoc as $file) {
                    $filesize = filesize(VPConfig::GetLogDirectory() . $file);
                    echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/log/' . $file . '">' . $file . ' ('.number_format($filesize / 1024, 2).' KB)</a><br>';
                }
            } else {
                $logfile = $path[0];
                echo '<h1>'.VPLocale::Get("dashboard.sidebar.logbook").': '.$logfile.'</h1>';
                echo '<p><a href="'.VPConfig::$VP_REDIRECT_URL.'/log">'.VPLocale::Get("general.back").'</a></p>';
                echo '<div style="font-family: monospace; width: 100%; height: 700px; overflow-y: scroll;">';
                $logdate = file_get_contents(VPConfig::GetLogDirectory() . $logfile);
                $logdate = htmlspecialchars($logdate);
                echo str_replace(PHP_EOL, '<br>', $logdate);
                echo '</div>';
            }
        } else {
            VPPermissions::NoPermissionMessage();
        }
    }

}