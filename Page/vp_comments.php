<?php

require_once 'vp_logger.php';
require_once 'vp_pageUtil.php';

class VPCommentsPage
{

    public static function ShowCommentsPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if (sizeof($path) == 0) {
            if ($permissions->CanViewCommentList()) {
                VPCommentsPage::ShowOverviewPage($path, $permissions);
            } else {
                VPPermissions::NoPermissionMessage();
            }
        } else {
            $page = $path[0];

            if ($page == "write") {
                if ($permissions->CanCreateComment()) {
                    VPCommentsPage::ShowNewCommentPage($path);
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else if ($page == "edit") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditComment($path[1])) {
                        VPCommentsPage::ShowEditPage($path[1]);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/comments");
                    exit;
                }
            } else if ($page == "delete") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditComment($path[1])) {
                        VPCommentsPage::ShowOverviewPage($path, $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/comments");
                    exit;
                }
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/comments");
                exit;
            }
        }
    }

    private static function ShowNewCommentPage($path)
    {
        if (isset($_GET['article'])) {
            $articleID = $_GET['article'];
            $userID = VPLogin::LoggedInUserID();
            $sqlValues = "NULL, '$userID', '$articleID', '', NULL, NOW(), NOW(), 0";
            $sql = "INSERT INTO `vp_comments` (`ID`, `UserID`, `ArticleID`, `Text`, `ReplyTo`, `Created`, `LastEdit`, `Revisions`) VALUES ($sqlValues)";
            echo $sql;
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            $sql = "SELECT `ID` FROM `vp_comments` ORDER BY `ID` DESC LIMIT 1";
            $newestID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['ID'];
            VPLogger::GetLogger()->LogUserActivity("created new comment {ArticleID=$articleID CommentID=$newestID}");
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/comments/edit/$newestID");
            exit;
        } else {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . '/comments');
            exit;
        }
    }

    private static function ShowOverviewPage($path, $permissions)
    {
        require_once __DIR__ . "/../Dependencies/Parsedown.php";

        if ($permissions->CanCreateComment()) {
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/articles?for=writeComment"><button class="vp_dashboard_button"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Write.png"/>' . VPLocale::Get("comments.new") . '</button></a>';
        }

        echo '<h1>' . VPLocale::Get("comments.header") . '</h1>';

        $articleID = isset($_GET['article']) ? $_GET['article'] : "";
        $commentCount = isset($_GET['count']) ? $_GET['count'] : "";
        $textSearch = isset($_GET['search']) ? $_GET['search'] : "";

        if ($articleID . $commentCount . $textSearch == "") {
            if (isset($_SESSION['vp_comments_search'])) {
                $searchInfo = $_SESSION['vp_comments_search'];
                $spl = explode(";", $searchInfo);
                switch ($spl[0]) {
                    case "article":
                        $articleID = $spl[1];
                        break;
                    case "count":
                        $commentCount = $spl[1];
                        break;
                    case "search":
                        $textSearch = $spl[1];
                        break;
                }
            } else {
                // No user specified search
                $commentCount = 20;
            }
        }

        if ($articleID != "")
            $articleTitle = VPArticle::GetArticleTitle($articleID);
        else
            $articleTitle = "";

        if (sizeof($path) >= 2 && $path[0] == "delete") {
            $targetID = $path[1];
            $targetInfo = VPComment::GetComment($targetID);
            $deleteSQL = "DELETE FROM `vp_comments` WHERE `ID` = '$targetID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($deleteSQL);
            echo '<p class="vp_info_box">' . VPLocale::Get("comments.deleted", array($targetInfo->GetUserName(), $targetInfo->CreatedFormattedLong)) . '</p>';
            VPLogger::GetLogger()->LogUserActivity("deleted comment {CommentID=$targetID}");
            if (!$permissions->CanViewCommentList()) return;
        }

        $formAction = VPConfig::$VP_REDIRECT_URL . '/comments';
        echo '<div class="vp_search_panel"><table><tr>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($articleID) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("comments.search.article", array('<input disabled value="' . $articleTitle . '">')) . '<br>
                        <a href="' . VPConfig::$VP_REDIRECT_URL . '/articles?for=viewComments"><button type="button">' . VPLocale::Get("comments.search.select-article") . '</button></a>
                    </form></td>';
        echo '  <td style="width: 34%;"><form method="get"' . (strlen($commentCount) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("comments.search.newest", array('<input type="number" name="count" style="width: 50px;" value="' . ($commentCount == 0 ? "20" : $commentCount) . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>
                    </form></td>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($textSearch) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("comments.search.text", array('<input type="text" name="search" value="' . $textSearch . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.search") . '</button>
                    </form></td>';
        echo '</tr></table></div>';

        if ($articleID != "") {
            $articleID = VPDatabaseConn::EscapeSQLString($articleID);
            $sql = "SELECT * FROM `vp_comments` WHERE `ArticleID` = '$articleID' ORDER BY `Created` DESC";
            $searchInfo = "article;$articleID";
        } else if ($commentCount != "") {
            if ($commentCount < 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.negative") . '</h2>';
                return;
            }
            $commentCount = VPDatabaseConn::EscapeSQLString($commentCount);
            $sql = "SELECT * FROM `vp_comments` ORDER BY `Created` DESC LIMIT $commentCount";
            $searchInfo = "count;$commentCount";
        } else if ($textSearch != "") {
            $textSearch = trim($textSearch);
            if (strlen($textSearch) == 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.empty") . '</h2>';
                return;
            }
            $textSearch = VPDatabaseConn::EscapeSQLString($textSearch);
            $sql = "SELECT * FROM `vp_comments` WHERE `Text` LIKE '%$textSearch%' ORDER BY `Created` DESC LIMIT 100";
            $searchInfo = "search;$textSearch";
        } else {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.null") . '</h2>';
            return;
        }

        $_SESSION['vp_comments_search'] = $searchInfo;
        $timer = new VPTimer();
        $timer->Start();
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
        $searchTime = $timer->Stop();

        $resultCount = mysqli_num_rows($result);
        if ($resultCount == 0) {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.no-results") . '</h2>';
        } else {
            $index = 0;
            echo '<p class="vp_search_result_count">'.VPLocale::Get($resultCount == 1 ? "search.result-count-one" : "search.result-count", [ mysqli_num_rows($result), $searchTime ]).'</p>';

            echo '<div style="margin-top: 30px;">';
            while ($row = mysqli_fetch_assoc($result)) {
                if ($index++ > 0) echo '<hr>';

                $commentInfo = VPComment::GetCommentFromRow($row);
                $articleTitle = VPArticle::GetArticleTitle($commentInfo->ArticleID);
                echo '<p>' . VPLocale::Get("comments.comment-header", array($commentInfo->GetUserName(), $commentInfo->CreatedFormattedLong, $articleTitle));
                if ($permissions->CanEditComment($commentInfo->ID))
                    echo '<span style="margin-left: 10px; font-size: 13px;"><a href="' . VPConfig::$VP_REDIRECT_URL . '/comments/edit/' . $commentInfo->ID . '">' . VPLocale::Get("general.edit") . '</a></span>';

                $markdownParser = new Parsedown;
                echo '<div style="margin-left: 20px;">' . $markdownParser->text($commentInfo->Text) . '</div>';

                echo '</p>';
            }
            echo '</div>';
        }
    }

    private static function ShowEditPage($commentID)
    {
        echo '<h1>' . VPLocale::Get("comments.edit") . '</h1>';
        echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/comments">' . VPLocale::Get("general.back-overview") . '</a></p>';

        $commentData = VPComment::GetComment($commentID);
        if (isset($_POST['comment_update']) && $_POST['comment_update'] == "true") {
            $text_body = VPDatabaseConn::EscapeSQLString($_POST['comment_text']);
            $revisions = $commentData->Revisions + 1;
            $sql = "UPDATE `vp_comments` SET `Text` = '$text_body', `LastEdit` = NOW(), `Revisions` = '$revisions' WHERE `ID` = '$commentID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            VPLogger::GetLogger()->LogUserActivity("modified comment {CommentID=$commentID}");
            echo '<p class="vp_info_box">' . VPLocale::Get("comments.changes-saved") . '</p>';
        }

        $commentData = VPComment::GetComment($commentID);
        echo '<span id="vp_delete_redirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/comments/delete/' . $commentID . '</span>';
        echo '<form method="post" id="vp_commentForm" action="' . VPConfig::$VP_REDIRECT_URL . '/comments/edit/' . $commentID . '">';
        echo '<input name="comment_update" value="true" type="text" hidden/><div class="vp_edit_box"><div class="vp_edit_box_header">';
        echo VPLocale::Get("comments.comment-data") . '<span style="float: right">';
        echo '<span class="vp_delete_button" onClick="vp_commentDelete()">' . VPLocale::Get("general.delete") . '</span>';
        echo '<span class="vp_save_button" onClick="vp_commentSave()">' . VPLocale::Get("general.save") . '</span></span>';
        echo '</div><div class="vp_edit_box_body"><table>';

        echo '<tr><td>' . VPLocale::Get("comments.attribs.created-date") . '</td><td><input disabled value="' . VPUserData::GetUserName($commentData->UserID) . '"/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("comments.attribs.created-by") . '</td><td><input disabled value="' . $commentData->CreatedFormattedLong . '" style="width: 250px;"/></td></tr>';
        // echo '<tr><td>Zuletzt bearbeitet:</td><td><input disabled value="'.strftime(VPConfig::$VP_DATETIME_FORMAT, strtotime($commentData->LastEdit)).'" style="width: 250px;"/></td></tr>';
        // echo '<tr><td>Bearbeitet:</td><td><input disabled value="'.$commentData->Revisions.' mal"/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("comments.attribs.article") . '</td><td><input disabled value="' . VPArticle::GetArticleTitle($commentData->ArticleID) . '"/>&nbsp;<a target="_blank" href="' . VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $commentData->ArticleID . '">' . VPLocale::Get("comments.article-link") . '</a></td></tr>';

        echo '<tr><td colspan="2" style="height: 8px;"></td></tr>';
        echo '<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">';
        echo '<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>';
        echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("comments.attribs.comment") . '</td><td style="width: 100%;" id="vp_comment_text_wrap">' .
            '<textarea id="vp_comment_text" type="text" name="comment_text">' .
            htmlspecialchars($commentData->Text) . '</textarea><span style="font-size: 10px;" id="vp_comment_charLimit"></span></td></tr>';

        echo '</table></div></div></form>';

        echo '<span style="display:none;" id="vp_text_remaningChars">' . VPLocale::Get("comments.edit-notice.digit-state") . '</span>';
        echo '<span style="display:none;" id="vp_text_tooManyChars">' . VPLocale::Get("comments.edit-notice.too-many-chars") . '</span>';
        echo '<span style="display:none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved-changes") . '</span>';
        echo '<span style="display:none;" id="vp_text_deleteWarning">' . VPLocale::Get("comments.delete-warning") . '</span>';
        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_commentEdit.js"></script>';
    }

}