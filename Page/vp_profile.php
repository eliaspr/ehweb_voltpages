<?php

require_once 'vp_logger.php';

class VPProfilePage
{

    const ALLOWED_SIDEBAR_SPACINGS = [1, 5, 6, 8];
    const DEFAULT_SIDEBAR_SPACING = 5;

    public static function ShowProfilePage($path)
    {
        echo '<h1>' . VPLocale::Get("profile.header") . '</h1>';
        $userID = VPLogin::LoggedInUserID();

        if (isset($_POST['myprofile_update']) && $_POST['myprofile_update'] == "true") {
            $p_email = VPDatabaseConn::EscapeSQLString($_POST['profile_email']);
            $p_telnr = VPDatabaseConn::EscapeSQLString($_POST['profile_telephone']);
            $p_lang = VPDatabaseConn::EscapeSQLString($_POST['profile_language']);
            $p_darkMode = (isset($_POST['profile_darkMode']) && $_POST['profile_darkMode'] == "on") ? 1 : 0;

            $sql = "UPDATE `vp_users` SET `E-Mail` = '$p_email', `Telephone` = '$p_telnr', `PreferredLanguage` = '$p_lang', `DarkMode` = '$p_darkMode' WHERE `ID` = '$userID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            VPLogger::GetLogger()->LogUserActivity("updated own profile");
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/profile?updated=true");
        }

        if (isset($_GET['updated']) && $_GET['updated'] == "true") {
            echo '<p class="vp_info_box">' . VPLocale::Get("profile.updated") . '</p>';
        }

        $userData = VPUserData::GetUserData($userID);

        echo '<form method="post" id="vp_myprofileForm" action="' . VPConfig::$VP_REDIRECT_URL . '/profile">';
        echo '<input name="myprofile_update" value="true" type="text" hidden/><div class="vp_edit_box"><div class="vp_edit_box_header">';
        echo VPLocale::Get("profile.personal-info") . '<span style="float: right">';
        echo '<span id="vp_changePwURL" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/change_pw</span>';
        echo '<span class="vp_changePW_button" onClick="vp_myprofileChangePW()">' . VPLocale::Get("dashboard.sidebar.change_pw") . '</span>';
        echo '<span class="vp_save_button" onClick="vp_myprofileSave()">' . VPLocale::Get("general.save") . '</span></span>';
        echo '</div><div class="vp_edit_box_body"><table>';

        echo '<tr><td colspan="2"><span style="margin-left: 10px; font-size: 10px;">' . VPLocale::Get("profile.admin-attribs") . '</span></td></tr>';
        echo '<tr><td>' . VPLocale::Get("profile.attribs.name") . '</td><td><input type="text" value="' . htmlspecialchars($userData->Name) . '" disabled /></td></tr>';
        echo '<tr><td>' . VPLocale::Get("profile.attribs.login-id") . '</td><td><input type="text" value="' . htmlspecialchars($userData->LoginID) . '" disabled style="width: 80px;" /></td></tr>';

        echo '<tr><td colspan="2"><hr /></td></tr>';
        echo '<tr><td colspan="2"><span style="margin-left: 10px; font-size: 10px;">' . VPLocale::Get("profile.public-attribs") . '</span></td></tr>';
        echo '<tr><td>' . VPLocale::Get("profile.attribs.e-mail") . '</td><td><input type="text" onInput="vp_myprofileInput()" value="' . htmlspecialchars($userData->EMail) . '" name="profile_email" /></td></tr>';
        echo '<tr><td>' . VPLocale::Get("profile.attribs.telephone") . '</td><td><input type="text" onInput="vp_myprofileInput()" value="' . htmlspecialchars($userData->Telephone) . '" name="profile_telephone" /></td></tr>';

        echo '<tr><td colspan="2"><hr /></td></tr>';

        echo '<tr><td>' . VPLocale::Get("profile.attribs.language") . '</td><td>';
        $languages = VPLocale::GetAvailableLanguages();
        $currentLanguage = VPLocale::GetLocale()->GetLanguageName();
        echo '<select name="profile_language" onInput="vp_myprofileInput()">';
        foreach ($languages as $i) {
            echo '<option value="' . $i . '"' . ($i == $currentLanguage ? " selected" : "") . '>' . $i . '</option>';
        }
        echo '</select></td></tr>';

        echo '<tr><td>' . VPLocale::Get("profile.attribs.dark-mode") . '</td><td>';
        $currentMode = VPConfig::IsDarkModeEnabled();
        echo '<input type="checkbox" name="profile_darkMode" id="chkbx_darkmode" class="vp_fancy_checkbox" onChange="vp_myprofileInput()"' . ($currentMode ? ' checked' : '') . '/><label for="chkbx_darkmode" class="vp_fancy_checkbox_label"></label>';
        echo '</td></tr>';

//        echo '<tr><td>' . VPLocale::Get("profile.attribs.sidebar-spacing.name") . '</td><td>';
//        echo '<select name="sidebar_spacing" onInput="vp_myprofileInput()">';
//        $currentValue = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `SidebarSpacing` FROM vp_users WHERE `ID` = '$userID'")['SidebarSpacing'];
//        foreach(self::ALLOWED_SIDEBAR_SPACINGS as $a) {
//            echo '<option value="'.$a.'"' . ($currentValue == $a ? " selected" : "") . '>' . VPLocale::Get("profile.attribs.sidebar-spacing.$a") . '</option>';
//        }
//        echo '</select></td></tr>';

        echo '</table></div></div></form>';

        echo '<span style="display: none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved-changes") . '</span>';
        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_myProfile.js"></script>';

        require_once 'vp_statistics.php';
        VPStatsPage::ShowStats($userData->ID);

        echo '<div class="vp_edit_box"><div class="vp_edit_box_header">' . VPLocale::Get("profile.permissions") . '</div><div class="vp_edit_box_body"><p>';
        $permissions = VPPermissions::FromUserData($userData);
        $permissions->EchoPermissionTable();
        echo '</p></div></div>';
    }

}