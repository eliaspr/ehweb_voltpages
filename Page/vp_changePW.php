<?php

require_once 'vp_logger.php';

function IsValidPassword($pass) : bool {
    if(strlen($pass) < 8) return false;
    if(preg_match("/[0-9]/", $pass) !== 1) return false;
    if(preg_match("/[\@\_\!\#\$\%\^\&\*\(\)\<\>\?\/\\\[\]\|\}\{\~\:\"\§\=\+\-\*\{\}]/", $pass) !== 1) return false;
    return true;
}

if (isset($_POST['vp_oldPW']) && isset($_POST['vp_newPW_1']) && isset($_POST['vp_newPW_2'])) {
    $old = hash("sha512", $_POST['vp_oldPW']);
    $new1 = hash("sha512", $_POST['vp_newPW_1']);
    $new2 = hash("sha512", $_POST['vp_newPW_2']);

    $userData = VPUserData::GetUserData(VPLogin::LoggedInUserID());
    if ($userData->PasswordHash != $old) {
        VPLogger::GetLogger()->LogUserActivity("change password > wrong password");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/change_pw/?error=wrong_password");
        exit;
    }

    if ($old == $new1) {
        VPLogger::GetLogger()->LogUserActivity("change password > no change");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/change_pw/?error=no_change");
        exit;
    }

    if(!IsValidPassword($_POST['vp_newPW_1'])) {
        VPLogger::GetLogger()->LogUserActivity("change password > password not secure");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/change_pw/?error=pw_criteria");
        exit;
    }

    if ($new1 != $new2) {
        VPLogger::GetLogger()->LogUserActivity("change password > not identical");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/change_pw/?error=not_identical");
        exit;
    }

    VPLogin::AttemptPasswordChange(VPLogin::LoggedInUserID(), $_POST['vp_newPW_1']);
    VPLogger::GetLogger()->LogUserActivity("change password > password changed");
    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/dashboard/?info=pw_changed");
    exit;
}

?>

<div class="vp_login_form_container">
    <?php
    $credit = VPConfig::$VP_LOGIN_PAGE_BG_CREDIT;
    if($credit != null && strlen($credit) > 0) {
        echo '<p class="vp_login_form_img_credit">'.$credit.'</p>';
    }
    ?>

    <div class="vp_login_form">
        <div class="vp_login_form_img_container">
            <a href="/">
                <img src="<?php echo VPConfig::$VP_USER_ICON; ?>"/>
            </a>
        </div>

        <form class="vp_login_form_form" method="post" action="<?php echo VPConfig::$VP_REDIRECT_URL; ?>/change_pw">
            <p style="margin-top: 0px;"><b
                        style="font-size: 20px; font-weight: bold; color: white;"><?php echo VPLocale::Get("authentication.change-password") ?></b>
            </p>

            <div class="vp_login_form_info"><?php echo VPLocale::Get("authentication.old-password") ?></div>
            <input class="vp_login_form_input" name="vp_oldPW" type="password" required/>

            <div class="vp_login_form_info" style="margin-top: 15px;"><?php echo VPLocale::Get("authentication.new-password") ?></div>
            <span class="vp_pw_tooltip">
                <input class="vp_login_form_input" name="vp_newPW_1" id="vp_newPW_1" type="password" required oninput="vp_updatePassRequirements();"/>
                <span class="vp_pw_tooltiptext">
                    <span id="pwcheck_1" style="color: red;"><?php echo VPLocale::Get("authentication.pw-requirement.characters", [8]) ?></span><br>
                    <span id="pwcheck_2" style="color: red;"><?php echo VPLocale::Get("authentication.pw-requirement.digit") ?></span><br>
                    <span id="pwcheck_3" style="color: red;"><?php echo VPLocale::Get("authentication.pw-requirement.special") ?></span>
                </span>
            </span>

            <script>
                function vp_updatePassRequirements() {
                    var password = document.getElementById("vp_newPW_1").value;

                    var length = password.length >= 8;
                    document.getElementById("pwcheck_1").style.color = length ? "green": "red";

                    var digit = RegExp("[0-9]").test(password);
                    document.getElementById("pwcheck_2").style.color = digit ? "green": "red";

                    var special = RegExp("[@_!#$%^&*()<>?/\\\[\]|}{~:\"§=+-*{}]").test(password);
                    document.getElementById("pwcheck_3").style.color = special ? "green": "red";
                }

                vp_updatePassRequirements();
            </script>

            <div class="vp_login_form_info"
                 style="margin-top: 15px;"><?php echo VPLocale::Get("authentication.new-password-repeat") ?></div>
            <input class="vp_login_form_input" name="vp_newPW_2" type="password" required/>

            <span>
                <a href="<?php echo VPConfig::$VP_REDIRECT_URL; ?>/profile"
                   style="margin-top: 15px; text-decoration: none;"><button class="vp_login_form_button"
                                                                            type="button"><?php echo VPLocale::Get("general.cancel") ?></button></a>
                <button class="vp_login_form_button" type="submit"
                        style="margin-top: 15px;"><?php echo VPLocale::Get("authentication.change-password") ?></button>
            </span>

            <?php
            if (isset($_GET['error'])) {
                $errcode = $_GET['error'];
                if ($errcode == "no_change") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.no-change") . '</p>';
                } else if ($errcode == "wrong_password") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.wrong-password") . '</p>';
                } else if ($errcode == "not_identical") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.not-identical") . '</p>';
                } else if ($errcode == "pw_criteria") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.pw-criteria") . '</p>';
                }
            }
            ?>

            <table style="margin-top: 20px;">
                <tr>
                    <td><span style="font-size: 13px; color: white;">VoltPages v<?php echo VoltPages::$VP_VERSION ?></span></td>
                    <td></td>
                    <td><a href="https://www.github.com/eliaspr/VoltPages" target="_blank">
                            <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/github.png"
                                 style="width: 14px; height: 14px;"/>
                        </a></td>
                </tr>
            </table>
        </form>
    </div>
</div>