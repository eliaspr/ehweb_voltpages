<?php

class VPPluginList
{

    public static function ShowPluginList()
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if ($permissions->CheckPermission("_admin")) {
            echo '<h1>' . VPLocale::Get("plugins.plugin-list.header") . '</h1>';

            $count = VoltPages::GetPluginManager()->GetPluginCount();
            echo '<p class="vp_search_result_count">' . ($count == 1 ? VPLocale::Get("plugins.plugin-active") : VPLocale::Get("plugins.plugins-active", [$count])) . '</p>';

            echo '<table class="vp_fancy_table"><tr>';
            echo '<th>' . VPLocale::Get("plugins.plugin-list.table.id") . '</th>';
            echo '<th>' . VPLocale::Get("plugins.plugin-list.table.name") . '</th>';
            echo '<th>' . VPLocale::Get("plugins.plugin-list.table.version") . '</th>';
            echo '<th>' . VPLocale::Get("plugins.plugin-list.table.author") . '</th></tr>';

            if (VoltPages::GetPluginManager()->GetPluginCount() == 0) {
                echo '<tr><td colspan="4" style="text-align: center;"><em>' . VPLocale::Get("plugins.plugin-list.no-plugins") . '</em></td>';
            } else {

                foreach (VoltPages::GetPluginManager()->GetPluginList() as $plugin) {
                    echo '<tr><td>' . $plugin->GetID() . '</td>';
                    echo '<td>' . $plugin->GetName() . '</td>';
                    echo '<td>' . $plugin->GetVersion() . '</td>';
                    echo '<td>' . $plugin->GetAuthor() . '</td></tr>';
                }
            }

            echo '</table>';
        } else {
            VPPermissions::NoPermissionMessage();
        }
    }

}