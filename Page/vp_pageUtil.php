<?php

class VPPageUtil
{

    // whether vp_newArticleButton.js has already been included
    private static $articleButtonScript = false;

    // whether vp_newUserButton.js has already been included
    private static $userButtonScript = false;

    // whether vp_newGalleryButton.js has already been included
    private static $galleryButtonScript = false;

    public static function CreateUserDropdown($htmlName, $dropdown = true, $selectedID = -1, $htmlID = "", $exclude = [])
    {
        echo '<select name="' . $htmlName . '"' . ($dropdown ? "" : 'size="10"') . '' . (strlen($htmlID) > 0 ? 'id="' . $htmlID . '"' : '') . '>';
        $sql = "SELECT `Name`, `ID` FROM `vp_users` ORDER BY `Name` ASC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result)) {
            if (in_array($row['ID'], $exclude)) continue;
            $selected = $selectedID == $row['ID'] ? " selected" : "";
            echo '<option value="' . $row['ID'] . '"' . $selected . '>' . utf8_encode($row['Name']) . '</option>';
        }
        echo '</select>';
    }

    public static function CreateTagDropdown($htmlName, $permissions = null, $dropdown = true, $selectedID = -1, $htmlID = "")
    {
        echo '<select name="' . $htmlName . '"' . ($dropdown ? "" : 'size="10"') . '' . (strlen($htmlID) > 0 ? 'id="' . $htmlID . '"' : '') . '>';
        $sql = "SELECT `Name`, `ID` FROM `vp_tags` ORDER BY `Name` ASC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result)) {
            if ($permissions != null && !$permissions->CanUseArticleTag($row['ID'])) continue;

            $selected = $selectedID == $row['ID'] ? " selected" : "";
            echo '<option value="' . $row['ID'] . '"' . $selected . '>' . utf8_encode($row['Name']) . '</option>';
        }
        echo '</select>';
    }

    public static function CreateNewArticleButton()
    {
        self::CheckNewArticleScript();
        echo '<button class="vp_dashboard_button" onclick="vp_createArticle()"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Write.png"/>' . VPLocale::Get("articles.new") . '</button>';
    }

    public static function CheckNewArticleScript()
    {
        if (!VPPageUtil::$articleButtonScript) {
            echo '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_newArticleButton.js"></script>';
            echo '<span style="display: none;" id="vp_text_newArticle">' . VPLocale::Get("articles.confirm-new") . '</span>';
            echo '<span style="display: none;" id="vp_newArticleRedirect">' . VPConfig::$VP_REDIRECT_URL . '/articles/create</span>';
            VPPageUtil::$articleButtonScript = true;
        }
    }

    public static function CreateNewUserButton()
    {
        if (!VPPageUtil::$userButtonScript) {
            echo '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_newUserButton.js"></script>';
            echo '<span style="display: none;" id="vp_text_newUser">' . VPLocale::Get("users.confirm-new") . '</span>';
            VPPageUtil::$userButtonScript = true;
        }
        echo '<span style="display: none;" id="vp_newUserRedirect">' . VPConfig::$VP_REDIRECT_URL . '/users/create</span>';
        echo '<button class="vp_dashboard_button" onclick="vp_createUser()"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Add.png"/>' . VPLocale::Get("users.new") . '</button>';
    }

    public static function CreateNewGalleryButton()
    {
        self::CheckNewGalleryScript();
        echo '<button class="vp_dashboard_button" onclick="vp_createGallery()"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Add.png"/>' . VPLocale::Get("galleries.create") . '</button>';
    }

    public static function CheckNewGalleryScript()
    {
        if (!VPPageUtil::$galleryButtonScript) {
            echo '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_newGalleryButton.js"></script>';
            echo '<span style="display: none;" id="vp_newGalleryRedirect">' . VPConfig::$VP_REDIRECT_URL . '/galleries/create</span>';
            echo '<span style="display: none;" id="vp_text_newGallery">' . VPLocale::Get("galleries.confirm-new") . '</span>';
            VPPageUtil::$galleryButtonScript = true;
        }
    }

    public static function FormatByteSize($bytes)
    {
        $point = VPLocale::Get("general.decimal.point");
        $sep = VPLocale::Get("general.decimal.separator");
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2, $point, $sep) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2, $point, $sep) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2, $point, $sep) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' B';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' B';
        } else {
            $bytes = '0 B';
        }

        return $bytes;
    }
}

class VPTimer
{

    private $T_before, $T_after;
    private $Time;

    public function Start()
    {
        $this->T_before = microtime(true);
    }

    public function Stop(): string
    {
        $this->T_after = microtime(true);
        $this->Time = self::FormatTimeInterval($this->T_after - $this->T_before);
        return $this->Time;
    }

    public function GetTime(): string
    {
        return $this->Time;
    }

    private static function FormatTimeInterval($seconds)
    {
        $microseconds = $seconds * 1000000;
        if ($microseconds < 100) {
            return number_format($microseconds, 2, ',', '.') . ' μs';
        } else if ($microseconds < 100000) {
            $ms = $microseconds / 1000;
            return number_format($ms, 2, ',', '.') . ' ms';
        } else {
            $seconds = $microseconds / 1000000;
            return number_format($seconds, 2, ',', '.') . ' s';
        }
    }

}