<?php

require_once 'vp_logger.php';
require_once 'vp_pageUtil.php';

class VPUsersPage
{

    public static function ShowUsersPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if (sizeof($path) == 0) {
            if ($permissions->CanViewUserList()) {
                VPUsersPage::ShowOverviewPage($path, $permissions);
            } else {
                VPPermissions::NoPermissionMessage();
            }
        } else {
            $page = $path[0];

            if ($page == "create") {
                if ($permissions->CanAddUser()) {
                    VPUsersPage::CreateUser();
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else if ($page == "edit") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditUser($path[1])) {
                        VPUsersPage::ShowEditPage(array_splice($path, 1), $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/users");
                    exit;
                }
            } else if ($page == "delete") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditUser($path[1])) {
                        VPUsersPage::ShowOverviewPage($path, $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/users");
                    exit;
                }
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/users");
                exit;
            }
        }
    }

    private static function ShowOverviewPage($path, VPPermissions $permissions)
    {
        if ($permissions->CanAddUser()) {
            require_once 'vp_pageUtil.php';
            VPPageUtil::CreateNewUserButton();
        }

        $showAll = (isset($_GET['showAll']) && $_GET['showAll'] == "true");
        $nameSearch = isset($_GET['searchName']) ? $_GET['searchName'] : "";
        $textSearch = $showAll ? "" : isset($_GET['search']) ? $_GET['search'] : "";

        if (!$showAll && $nameSearch . $textSearch == "") {
            if (isset($_SESSION['vp_users_search'])) {
                $searchInfo = $_SESSION['vp_users_search'];
                $spl = explode(";", $searchInfo);
                switch ($spl[0]) {
                    case "searchName":
                        $nameSearch = $spl[1];
                        break;
                    case "search":
                        $textSearch = $spl[1];
                        break;
                }
            } else {
                // No user specified search
                $textSearch = "";
            }
        }

        echo '<h1>' . VPLocale::Get("users.header") . '</h1>';

        if (sizeof($path) >= 2 && $path[0] == "delete") {
            $targetID = $path[1];
            if ($targetID != VPLogin::LoggedInUserID()) {
                $userInfo = VPUserData::GetUserData($targetID);
                $deleteSQL = "DELETE FROM `vp_users` WHERE `ID` = '$targetID'";
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery($deleteSQL);
                echo '<p class="vp_info_box">' . VPLocale::Get("users.deleted", array($userInfo->Name)) . '</p>';
                VPLogger::GetLogger()->LogUserActivity("deleted user {ID=$targetID Name='" . $userInfo->Name . "'}");
            }
        }

        $formAction = VPConfig::$VP_REDIRECT_URL . '/users';
        echo '<div class="vp_search_panel"><table><tr>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($nameSearch) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("users.search.name", array('<input type="text" name="searchName" value="' . $nameSearch . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.search") . '</button>
                    </form></td>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($textSearch) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        ' . VPLocale::Get("users.search.complete", array('<input type="text" name="search" value="' . $textSearch . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.search") . '</button>
                    </form></td>';
        echo '  <td style="width: 33%; text-align: center;"><form method="get"' . ((strlen($nameSearch) == 0 && strlen($textSearch) == 0) ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                        <input style="display: none;" type="text" name="showAll" value="true" hidden>
                        <button type="submit">' . VPLocale::Get("users.show-all") . '</button>
                    </form></td>';
        echo '</tr></table></div>';

        if ($nameSearch != "") {
            $nameSearch = VPDatabaseConn::EscapeSQLString($nameSearch);
            if (strlen($nameSearch) == 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.empty") . '</h2>';
                return;
            }
            $sql = "SELECT * FROM `vp_users` WHERE `Name` LIKE '%$nameSearch%' ORDER BY `Name` ASC LIMIT 100";
            $searchInfo = "searchName;$nameSearch";
        } else {
            if ($textSearch != "") {
                $textSearch = VPDatabaseConn::EscapeSQLString($textSearch);
                $sql = "SELECT * FROM `vp_users` WHERE `Name` LIKE '%$textSearch%' OR `E-Mail` LIKE '%$textSearch%' ORDER BY `Name` ASC LIMIT 100";
            } else {
                $sql = "SELECT * FROM `vp_users` ORDER BY `Name` ASC LIMIT 100";
            }
            $searchInfo = "search;$textSearch";
        }

        $_SESSION['vp_users_search'] = $searchInfo;
        $timer = new VPTimer();
        $timer->Start();
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
        $searchTime = $timer->Stop();

        $resultCount = mysqli_num_rows($result);
        if ($resultCount == 0) {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.no-results") . '</h2>';
        } else {
            echo '<p class="vp_search_result_count">'.VPLocale::Get($resultCount == 1 ? "search.result-count-one" : "search.result-count", [ mysqli_num_rows($result), $searchTime ]).'</p>';
            echo '<table class="vp_fancy_table"><tr> 
                            <th>' . VPLocale::Get("users.table.id") . '</th> 
                            <th>' . VPLocale::Get("users.table.name") . '</th> 
                            <th>' . VPLocale::Get("users.table.e-mail") . '</th> 
                            <th>' . VPLocale::Get("users.table.login") . '</th> 
                            <th>' . VPLocale::Get("users.table.options") . '</th> 
                        </tr>';
            while ($userRow = mysqli_fetch_assoc($result)) {
                $userData = VPUserData::GetUserDataFromRow($userRow);
                echo '<tr>';
                echo '<td>' . $userData->ID . '</td>';
                echo '<td>' . $userData->Name . '</td>';
                echo '<td><a href="mailto:' . $userData->EMail . '" target="_blank">' . $userData->EMail . '</a></td>';
                echo '<td style="text-align: center;">' . (strlen($userData->LoginID) > 0 ? $userData->LoginID : "-") . '</td>';
                echo '<td>';
                if ($permissions->CanEditUser($userData->ID)) echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/users/edit/' . $userData->ID . '"><button>' . VPLocale::Get("general.edit") . '</button></a>';
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
    }

    private static function CreateUser()
    {
        $sqlValues = "NULL, '', '', '', '', '', '0'";
        $sql = "INSERT INTO `vp_users` (`ID`, `Name`, `E-Mail`, `Login`, `Password`, `Permissions`, `ResetPW`) VALUES ($sqlValues)";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        $sql = "SELECT `ID` FROM `vp_users` ORDER BY `ID` DESC LIMIT 1";
        $newestID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['ID'];
        VPLogger::GetLogger()->LogUserActivity("created user {ID=$newestID}");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/users/edit/$newestID");
        exit;
    }

    private static function ShowEditPage($path, $permissions)
    {
        $targetID = $path[0];

        echo '<h1>' . VPLocale::Get("users.edit") . '</h1>';
        echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/users">' . VPLocale::Get("general.back-overview") . '</a></p>';

        if (isset($_POST['user_update']) && $_POST['user_update'] == "true") {
            $u_name = VPDatabaseConn::EscapeSQLString($_POST['user_name']);
            $u_email = VPDatabaseConn::EscapeSQLString($_POST['user_email']);
            $u_loginid = VPDatabaseConn::EscapeSQLString($_POST['user_loginid']);
            $u_resetpw = (isset($_POST['user_resetpw']) && $_POST['user_resetpw'] == "on") ? 1 : 0;
            $u_permissions = VPDatabaseConn::EscapeSQLString($_POST['user_permissions']);

            if (isset($_POST['user_password']) && strlen($_POST['user_password']) > 0) {
                $pwhash = VPDatabaseConn::EscapeSQLString(hash("sha512", $_POST['user_password']));
                $pass = ", `Password` = '$pwhash'";
            } else {
                $pass = "";
            }

            $sql = "UPDATE `vp_users` SET `Name` = '$u_name', `E-Mail` = '$u_email', `Login` = '$u_loginid', `Permissions` = '$u_permissions', `ResetPW` = '$u_resetpw'" . $pass . " WHERE `ID` = '$targetID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            echo '<p class="vp_info_box">' . VPLocale::Get("users.updated") . '</p>';
            VPLogger::GetLogger()->LogUserActivity("updated user {ID=$targetID Name='" . $u_name . "'}");
        }

        $userData = VPUserData::GetUserData($targetID);

        {
            echo '<span id="vp_delete_redirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/users/delete/' . $targetID . '</span>';
            echo '<form method="post" id="vp_userdataForm" action="' . VPConfig::$VP_REDIRECT_URL . '/users/edit/' . htmlspecialchars($targetID) . '">';
            echo '<input name="user_update" value="true" type="text" hidden/><div class="vp_edit_box"><div class="vp_edit_box_header">';
            echo VPLocale::Get("users.user-data") . '<span style="float: right">';

            if ($targetID == VPLogin::LoggedInUserID()) {
                echo '<span class="vp_disabled_button" title="' . VPLocale::Get("users.cant-delete-self") . '">' . VPLocale::Get("general.delete") . '</span>';
            } else {
                echo '<span class="vp_delete_button" onClick="vp_userdataDelete()">' . VPLocale::Get("general.delete") . '</span>';
            }

            echo '<span class="vp_save_button" onClick="vp_userdataSave()">' . VPLocale::Get("general.save") . '</span></span>';
            echo '</div><div class="vp_edit_box_body"><table>';

            echo '<tr><td>' . VPLocale::Get("users.attribs.name") . '</td><td><input type="text" onInput="vp_userdataInput()" name="user_name" value="' . htmlspecialchars($userData->Name) . '"/></td></tr>';
            echo '<tr><td>' . VPLocale::Get("users.attribs.e-mail") . '</td><td><input type="text" onInput="vp_userdataInput()" name="user_email" value="' . htmlspecialchars($userData->EMail) . '"/></td></tr>';

            echo '<tr><td style="height: 8px;"></td></tr>';
            echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("users.attribs.login-id") . '</td><td><input style="width: 100px;" type="text" onInput="vp_userdataInput()" name="user_loginid" value="' . htmlspecialchars($userData->LoginID) . '"/><br>';
            echo '<span style="font-size: 12px;">' . VPLocale::Get("users.provide-no-login-id") . '</span></td></tr>';

            echo '<tr><td style="height: 8px;"></td></tr>';
            echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("users.attribs.password") . '</td><td><input type="password" onInput="vp_userdataInput()" name="user_password" value=""/><br>';
            echo '<input type="checkbox" name="user_resetpw" onChange="vp_userdataInput()"/>';
            echo '<span style="margin-left: 5px; font-size: 14px;">' . VPLocale::Get("users.force-password-reset") . '</span><br>';
            echo '<span style="font-size: 12px;">' . VPLocale::Get("users.provide-no-password") . '</span>';
            echo '</td></tr>';

            echo '</table>';
            echo '<hr><p><b>' . VPLocale::Get("users.permissions.header") . '</b></p>';

            echo '<span style="display: none;" id="vp_deleteIconPath">' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Delete.png</span>';
            echo '<input type="text" id="vp_permissionList" name="user_permissions" hidden value="' . htmlspecialchars($userData->Permissions) . '" style="display: none;"/>';
            echo '<input type="text" id="vp_newPermission"/> <button type="button" onClick="vp_userdataAddPermission()">' . VPLocale::Get("users.permissions.add") . '</button>';
            echo '<a style="margin-left: 5px; font-size: 12px;" href="' . VPConfig::$VP_INSTALL_DIR . 'Image/permissions.png" target="_blank">' . VPLocale::Get("users.permissions.available") . '</a>';
            echo '<div style="border: 1px solid black; padding: 3px; margin-top: 10px; width: 40%; height: auto; font-size: 14px;" id="vp_permissionDisplay"><em>' . VPLocale::Get("users.permissions.empty") . '</em></div>';

            echo '</div></div></form>';

            echo '<span style="display: none;" id="vp_text_deleteUser1">' . VPLocale::Get("users.delete-warning-1") . '</span>';
            echo '<span style="display: none;" id="vp_text_deleteUser2">' . VPLocale::Get("users.delete-warning-2") . '</span>';
            echo '<span style="display: none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved_changes") . '</span>';
            echo '<span style="display: none;" id="vp_text_noPermissions">' . VPLocale::Get("users.permissions.empty") . '</span>';
            echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_userEdit.js"></script>';
        }

        require_once 'vp_statistics.php';
        VPStatsPage::ShowStats($userData->ID);

        {
            echo '<div class="vp_edit_box"><div class="vp_edit_box_header">' . VPLocale::Get("users.permissions.table") . '</div><div class="vp_edit_box_body"><p>';
            $permissions = VPPermissions::FromUserData($userData);
            $permissions->EchoPermissionTable();
            echo '</p></div></div>';
        }
    }

}