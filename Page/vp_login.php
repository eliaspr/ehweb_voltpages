<?php

require_once 'vp_logger.php';

if (isset($_POST['vp_UserID']) && isset($_POST['vp_Password'])) {
    VPLogger::GetLogger()->LogUserActivity("attempted login");
    VPLogin::AttemptLogin();
}

?>

<div class="vp_login_form_container">
    <?php
    $credit = VPConfig::$VP_LOGIN_PAGE_BG_CREDIT;
    if($credit != null && strlen($credit) > 0) {
        echo '<p class="vp_login_form_img_credit">'.$credit.'</p>';
    }
    ?>

    <div class="vp_login_form">
        <div class="vp_login_form_img_container">
            <a href="/">
                <img src="<?php echo VPConfig::$VP_USER_ICON; ?>"/>
            </a>
        </div>

        <form class="vp_login_form_form" method="post" action="<?php echo VPConfig::$VP_REDIRECT_URL; ?>/login">
            <div class="vp_login_form_info"><?php echo VPLocale::Get("authentication.login-id") ?></div>
            <input class="vp_login_form_input" name="vp_UserID" type="text"
                   value="<?php echo isset($_GET['userID']) ? $_GET['userID'] : ''; ?>" required/>

            <div class="vp_login_form_info"
                 style="margin-top: 15px;"><?php echo VPLocale::Get("authentication.password") ?></div>
            <input class="vp_login_form_input" name="vp_Password" type="password" required/>


            <table style="margin-top: 30px;">
                <tr>
                    <td style="width: 100%;">
                        <table>
                            <tr>
                                <td colspan="3">
                                    <span style="font-size: 13px;"><a style="color: slategray;"
                                                                      href="/"><?php echo VPLocale::Get("general.back-website") ?></a></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size: 13px; color: white;">VoltPages v<?php echo VoltPages::$VP_VERSION ?></span>
                                </td>
                                <td></td>
                                <td><a href="https://www.github.com/eliaspr/VoltPages" target="_blank">
                                        <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/github.png"
                                             style="width: 14px; height: 14px;"/>
                                    </a></td>
                            </tr>
                        </table>
                    </td>
                    <td style="text-align: right;">
                        <button class="vp_login_form_button"
                                type="submit"><?php echo VPLocale::Get("authentication.login") ?></button>
                    </td>
                </tr>
            </table>

            <?php
            if (isset($_GET['error'])) {
                $errcode = $_GET['error'];
                if ($errcode == "unknown_user") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.unknown-user") . '</p>';
                } else if ($errcode == "wrong_password") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.wrong-password") . '</p>';
                } else if ($errcode == "login_expired") {
                    echo '<p class="vp_login_form_error">' . VPLocale::Get("authentication.error.timeout", array("30")) . '</p>';
                }
            }
            ?>
        </form>
    </div>
</div>
