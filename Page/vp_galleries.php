<?php

require_once 'vp_logger.php';
require_once 'vp_pageUtil.php';
require_once __DIR__ . '/../vp_galleries.php';

class VPGalleriesPage
{
    public static function ShowGalleriesPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if (sizeof($path) == 0) {
            if ($permissions->CanViewGalleryList()) {
                VPGalleriesPage::ShowOverviewPage($path, $permissions);
            } else {
                VPPermissions::NoPermissionMessage();
            }
        } else {
            $page = $path[0];

            if ($page == "create") {
                if ($permissions->CanCreateGallery()) {
                    VPGalleriesPage::CreateGallery();
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else if ($page == "edit") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditGallery($path[1])) {
                        VPGalleriesPage::ShowEditPage($path[1]);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/galleries");
                    exit;
                }
            } else if ($page == "delete") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditGallery($path[1])) {
                        VPGalleriesPage::ShowOverviewPage($path, $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/galleries");
                    exit;
                }
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/galleries");
                exit;
            }
        }
    }

    private static function ShowOverviewPage($path, VPPermissions $permissions)
    {
        if ($permissions->CanCreateGallery()) {
            VPPageUtil::CreateNewGalleryButton();
        }

        $dateFrom = isset($_GET['since']) ? $_GET['since'] : "";
        $galleryCount = isset($_GET['count']) ? $_GET['count'] : "";
        $textSearch = isset($_GET['search']) ? $_GET['search'] : "";

        if ($dateFrom . $galleryCount . $textSearch == "") {
            if (isset($_SESSION['vp_gallery_search'])) {
                $searchInfo = $_SESSION['vp_gallery_search'];
                $spl = explode(";", $searchInfo);
                switch ($spl[0]) {
                    case "since":
                        $dateFrom = $spl[1];
                        break;
                    case "count":
                        $galleryCount = $spl[1];
                        break;
                    case "search":
                        $textSearch = $spl[1];
                        break;
                }
            } else {
                // No user specified search
                $galleryCount = "10";
            }
        }

        echo '<h1>' . VPLocale::Get("galleries.header") . '</h1>';

        if (sizeof($path) >= 2 && $path[0] == "delete") {
            $galleryID = VPDatabaseConn::EscapeSQLString($path[1]);

            $galleryInfo = VPGallery::GetGalleryData($galleryID);
            $deleteSQL = "DELETE FROM vp_galleries WHERE ID = '$galleryID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($deleteSQL);
            echo '<p class="vp_info_box">' . VPLocale::Get("galleries.deleted", [$galleryInfo->Title]) . '</p>';
            VPLogger::GetLogger()->LogUserActivity("deleted gallery {ID=$galleryID}");

            $sql = "SELECT ID FROM vp_images WHERE GalleryID = '$galleryID'";
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            require_once 'vp_photos.php';
            while ($row = mysqli_fetch_assoc($result)) {
                VPPhotosPage::DeleteImageFromDB($row['ID'], false);
            }
        }

        $formAction = VPConfig::$VP_REDIRECT_URL . '/galleries';
        echo '<div class="vp_search_panel"><table><tr>';
        echo '  <td style="width: 33%;"><form method="get" ' . (strlen($dateFrom) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                    ' . VPLocale::Get("galleries.search.since", array('<input type="date" name="since" value="' . $dateFrom . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>
                    </form></td>';
        echo '  <td style="width: 34%;"><form method="get" ' . (strlen($galleryCount) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                    ' . VPLocale::Get("galleries.search.newest", array('<input type="number" name="count" style="width: 50px;" value="' . ($galleryCount == 0 ? "10" : $galleryCount) . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>
                    </form></td>';
        echo '  <td style="width: 33%;"><form method="get" ' . (strlen($textSearch) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                    ' . VPLocale::Get("galleries.search.text", array('<input type="text" name="search" value="' . $textSearch . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.search") . '</button>
                    </form></td>';
        echo '</tr></table></div>';

        if ($dateFrom != "") {
            $dateFrom = VPDatabaseConn::EscapeSQLString($dateFrom);
            $sql = "SELECT * FROM `vp_galleries` WHERE `Timestamp` IS NOT NULL AND `Timestamp` > '$dateFrom' ORDER BY `Timestamp` DESC LIMIT 100";
            $searchInfo = "since;$dateFrom";
        } else if ($galleryCount != "") {
            if ($galleryCount < 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.negative") . '</h2>';
                return;
            }
            $galleryCount = VPDatabaseConn::EscapeSQLString($galleryCount);
            $sql = "SELECT * FROM `vp_galleries` ORDER BY `Timestamp` DESC LIMIT $galleryCount";
            $searchInfo = "count;$galleryCount";
        } else if ($textSearch != "") {
            $textSearch = trim($textSearch);
            if (strlen($textSearch) == 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.empty") . '</h2>';
                return;
            }
            $textSearch = VPDatabaseConn::EscapeSQLString($textSearch);
            $sql = "SELECT * FROM `vp_galleries` WHERE `Title` LIKE '%$textSearch%' OR `Info` LIKE '%$textSearch%' ORDER BY `Timestamp` DESC LIMIT 100";
            $searchInfo = "search;$textSearch";
        } else {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.null") . '</h2>';
            return;
        }

        $_SESSION['vp_gallery_search'] = $searchInfo;

        $timer = new VPTimer();
        $timer->Start();
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
        $searchTime = $timer->Stop();

        $resultCount = mysqli_num_rows($result);
        if ($resultCount == 0) {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.no-results") . '</h2>';
        } else {
            echo '<p class="vp_search_result_count">' . VPLocale::Get($resultCount == 1 ? "search.result-count-one" : "search.result-count", [mysqli_num_rows($result), $searchTime]) . '</p>';

            echo '<table><tr>';
            $index = 0;
            while ($galleryRow = mysqli_fetch_assoc($result)) {
                if ($index++ >= 3) {
                    echo '</tr><tr>';
                    $index = 1;
                }

                $galleryData = VPGallery::GetGalleryDataFromRow($galleryRow);
                $canEdit = $permissions->CanEditGallery($galleryData->ID);

                echo '<td><div class="vp_photo_list_table_cell">';
                if ($canEdit) echo '<a style="text-decoration: none;" href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryData->ID . '">';

                if (($imgCount = sizeof($galleryData->ImageIDs)) == 0) {
                    $imageURL = VPConfig::$VP_INSTALL_DIR . 'Image/img-placeholder'.(VPConfig::IsDarkModeEnabled() ? '-dark' : '').'.png';
                } else {
                    $firstImage = VPImage::GetImageData($galleryData->ImageIDs[0]);
                    $imageURL = '/' . $firstImage->PreviewFile;
                }

                echo '<div class="vp_photo_list_box" style="background-image: url(' . htmlspecialchars($imageURL) . ')">';
                echo '</div><div class="vp_photo_list_box_text">';
                echo $galleryData->Title . '<br>' . $galleryData->TimestampFormatted . '<br>' . VPLocale::Get("galleries.photo-count", [$imgCount]);

                if ($canEdit) {
                    echo '<br><span style="text-decoration: underline;">' . VPLocale::Get("general.edit") . '</span>';
                } else {
                    echo '<br><span style="font-size: 11px;">' . VPLocale::Get("galleries.cant-edit") . '</span>';
                }

                echo '</div>';
                if ($canEdit) echo '</a>';
                echo '</div></td>';
            }
            echo '</tr></table>';
        }
    }

    private static function ShowEditPage($galleryID)
    {
        $galleryID = VPDatabaseConn::EscapeSQLString($galleryID);

        echo '<h1>' . VPLocale::Get("galleries.edit") . '</h1>';
        echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries">' . VPLocale::Get("general.back-overview") . '</a>';
        if (VPConfig::$VP_WEBSITE_PAGE_GALLERY != null) {
            echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
            echo '<a target="_blank" href="' . str_replace("{id}", $galleryID, VPConfig::$VP_WEBSITE_PAGE_GALLERY) . '">' . VPLocale::Get("galleries.view-website") . '</a>';
        }
        echo '</p>';

        if (isset($_POST['gallery_update']) && $_POST['gallery_update'] == "true") {
            $g_title = VPDatabaseConn::EscapeSQLString($_POST['gallery_title']);
            $g_info = VPDatabaseConn::EscapeSQLString($_POST['gallery_info']);

            $sql = "UPDATE `vp_galleries` SET `Title` = '$g_title', `Info` = '$g_info' WHERE `ID` = '$galleryID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            echo '<p class="vp_info_box">' . VPLocale::Get("galleries.message.updated") . '</p>';
            VPLogger::GetLogger()->LogUserActivity("modified gallery {ID=$galleryID}");
        }

        if (isset($_GET['moveImageDown'])) {
            $image = $_GET['moveImageDown'];

            $selected = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_images WHERE GalleryID = '$galleryID' AND GalleryIndex = $image");
            $below = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_images WHERE GalleryID = '$galleryID' AND GalleryIndex > $image ORDER BY GalleryIndex ASC LIMIT 1");

            if ($selected != null && $below != null) {
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_images SET GalleryIndex = " . ($image) . " WHERE ID = '" . $below['ID'] . "'");
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_images SET GalleryIndex = " . ($image + 1) . " WHERE ID = '" . $selected['ID'] . "'");

                VPLogger::GetLogger()->LogUserActivity("modified gallery: moved image #$image down (galleryID=$galleryID)");
            }
        }
        if (isset($_GET['moveImageUp'])) {
            $image = $_GET['moveImageUp'];

            $selected = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_images WHERE GalleryID = '$galleryID' AND GalleryIndex = $image");
            $above = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_images WHERE GalleryID = '$galleryID' AND GalleryIndex < $image ORDER BY GalleryIndex DESC LIMIT 1");

            if ($selected != null && $above != null) {
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_images SET GalleryIndex = " . ($image) . " WHERE ID = '" . $above['ID'] . "'");
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_images SET GalleryIndex = " . ($image - 1) . " WHERE ID = '" . $selected['ID'] . "'");

                VPLogger::GetLogger()->LogUserActivity("modified gallery: moved image #$image up (galleryID=$galleryID)");
            }
        }
        if (isset($_GET['deleteImage'])) {
            $image = $_GET['deleteImage'];

            $selected = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_images WHERE GalleryID = '$galleryID' AND GalleryIndex = $image");
            require_once 'vp_photos.php';
            VPPhotosPage::DeleteImageFromDB($selected['ID'], false);

            $sql = "SELECT ID FROM vp_images WHERE GalleryID = '$galleryID' ORDER BY GalleryIndex ASC";
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $sql = "UPDATE vp_images SET GalleryIndex = $i WHERE ID = " . $row['ID'];
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
                $i++;
            }

            VPLogger::GetLogger()->LogUserActivity("modified gallery: removed image #$image from gallery (galleryID=$galleryID)");
        }

        $galleryData = VPGallery::GetGalleryData($galleryID);

        echo '<span id="vp_delete_redirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/galleries/delete/' . $galleryID . '</span>';
        echo '<form method="post" id="vp_galleryForm" action="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '">';
        echo '<input name="gallery_update" value="true" type="text" hidden/><div class="vp_edit_box"><div class="vp_edit_box_header">';

        echo VPLocale::Get("galleries.gallery-data") . '<span style="float: right">';
        echo '<span class="vp_delete_button" onClick="vp_galleryDelete()">' . VPLocale::Get("general.delete") . '</span>';
        echo '<span class="vp_save_button" onClick="vp_gallerySave()">' . VPLocale::Get("general.save") . '</span></span>';
        echo '</div><div class="vp_edit_box_body"><table>';

        echo '<tr><td>' . VPLocale::Get("galleries.attribs.created-date") . '</td><td><input type="text" value="' . $galleryData->TimestampFormatted . '" disabled/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("galleries.attribs.created-by") . '</td><td><input type="text" value="' . VPUserData::GetUserName($galleryData->Owner) . '" disabled/></td></tr>';

        echo '<tr><td>' . VPLocale::Get("galleries.attribs.image-count") . '</td><td><input type="text" value="' . sizeof($galleryData->ImageIDs) . '" disabled/>';
        echo '<span style="margin-left: 5px; font-size: 0.8em;">' . VPPageUtil::FormatByteSize($galleryData->CalculateTotalFileSize()) . '</span></td></tr>';

        echo '<tr><td>' . VPLocale::Get("galleries.attribs.title") . '</td><td><input type="text" onInput="vp_galleryInput()" name="gallery_title" value="' . $galleryData->Title . '"/><span style="margin-left: 10px; font-size: 10px;">' . VPLocale::Get("galleries.edit-notice.max-chars", array("200")) . '</span></td></tr>';
        echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("galleries.attribs.info") . '</td><td><textarea id="vp_gallery_info" name="gallery_info" rows="8" cols="50">' . $galleryData->Info . '</textarea><br><span style="font-size: 10px;" id="gallery_info_charLimit"></span></td></tr>';

        echo '</table></div></div></form>';

        echo '<span style="display:none;" id="vp_text_remaningChars">' . VPLocale::Get("galleries.edit-notice.digit-state") . '</span>';
        echo '<span style="display:none;" id="vp_text_tooManyChars">' . VPLocale::Get("galleries.edit-notice.too-many-chars") . '</span>';
        echo '<span style="display:none;" id="vp_text_deleteWarning">' . VPLocale::Get("galleries.delete-warning") . '</span>';
        echo '<span style="display:none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved-changes") . '</span>';

        echo '<h1 style="margin-top: 80px;">' . VPLocale::Get("galleries.photos") . '</h1>';
        echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/photos/upload?gallery=' . $galleryID . '"><button>Bilder hochladen</button></a>&nbsp;&nbsp;&nbsp;&nbsp;';

        $previewSize = 0;
        if (isset($_GET['previewSize'])) {
            $pv = $_GET['previewSize'];
            if ($pv == 0 || $pv == 1 || $pv == 2)
                $previewSize = $pv;
        } else if (isset($_SESSION['vp_gallery_preview'])) {
            $pv = $_SESSION['vp_gallery_preview'];
            if ($pv == 0 || $pv == 1 || $pv == 2)
                $previewSize = $pv;
        }
        $_SESSION['vp_gallery_preview'] = $previewSize;

        echo '<span style="font-size: 13px;">Vorschaubilder:&nbsp&nbsp';
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '?previewSize=0">' . ($previewSize == 0 ? '<strong>' : '') . 'klein' . ($previewSize == 0 ? '</strong>' : '') . '</a>&nbsp&nbsp';
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '?previewSize=1">' . ($previewSize == 1 ? '<strong>' : '') . 'mittel' . ($previewSize == 1 ? '</strong>' : '') . '</a>&nbsp&nbsp';
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '?previewSize=2">' . ($previewSize == 2 ? '<strong>' : '') . 'groß' . ($previewSize == 2 ? '</strong>' : '') . '</a>&nbsp&nbsp';
        echo '</span></p>';

        switch ($previewSize) {
            case 0:
                $previewSizePX = 100;
                break;
            case 1:
                $previewSizePX = 350;
                break;
            case 2:
            default:
                $previewSizePX = 700;
        }

        $sql = "SELECT * FROM vp_images WHERE GalleryID = '$galleryID' ORDER BY GalleryIndex ASC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) > 0) {
            echo '<table>';
            while ($row = mysqli_fetch_assoc($result)) {
                $imageData = VPImage::GetImageDataFromRow($row);
                $editURL = VPConfig::$VP_REDIRECT_URL . '/photos/edit/' . $imageData->ID . '?cameFrom=gallery&galleryID=' . $galleryID;

                if ($previewSizePX > VPConfig::$VP_IMAGE_UPLOAD_WIDTH_PREVIEW)
                    $imageURL = '/' . $imageData->File;
                else
                    $imageURL = '/' . $imageData->PreviewFile;

                echo '<tr><td><a href="' . $editURL . '"><img src="' . $imageURL . '" style="width: ' . $previewSizePX . 'px;"/></a></td><td style="vertical-align: top; padding: 10px;"><p>';
                {
                    echo $imageData->GalleryIndex . '<br><br>';
                    echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '?moveImageUp=' . $imageData->GalleryIndex . '"><button><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Up.png"/></button></a>';
                    echo '<button onclick="vp_galleryRemoveImage(' . $imageData->GalleryIndex . ')"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Delete.png"/></button>';
                    echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '?moveImageDown=' . $imageData->GalleryIndex . '"><button><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Down.png"/></button></a>';
                }
                echo '</p></td></tr>';
            }
            echo '</table>';
        } else {
            echo '<p><em>Keine Bilder vorhanden</em></p>';
        }

        echo '<span id="vp_removeImgRedirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '?deleteImage=</span>';
        echo '<span id="vp_text_removeImageWarning" style="display: none;">' . VPLocale::Get("galleries.remove-img-warning") . '</span>';

        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_galleryEdit.js"></script>';
    }

    private static function CreateGallery()
    {
        $userID = VPLogin::LoggedInUserID();
        $sql = "INSERT INTO vp_galleries (ID, Title, Info, `Owner`, `Timestamp`) VALUES (NULL, '', '', '$userID', NOW())";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        $sql = "SELECT ID FROM vp_galleries ORDER BY ID DESC LIMIT 1";
        $galleryID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['ID'];
        VPLogger::GetLogger()->LogUserActivity("created new gallery {ID=$galleryID}");
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/galleries/edit/$galleryID");
        exit;
    }
}