<?php

require_once 'vp_pageUtil.php';

require_once __DIR__ . "/../Dependencies/Parsedown.php";
require_once __DIR__ . "/../Dependencies/html2text.php";

class VPSearchPage
{

    public static function ShowSearchPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        echo '<h1>' . VPLocale::Get("search-page.header") . '</h1>';

        if (isset($_GET['query'])) {
            $query = $_GET['query'];
        } else if (isset($_SESSION['vp_lastSearch'])) {
            $query = $_SESSION['vp_lastSearch'];
        } else {
            $query = '';
        }
        $_SESSION['vp_lastSearch'] = $query;

        if ($permissions->CanExecuteSQL() && substr($query, 0, 4) === '@sql') {
            $query = trim(substr($query, 4));
            echo '<form id="vp_sqlForm" action="'.VPConfig::$VP_REDIRECT_URL.'/database/query/" method="post">';
            echo '<input name="vp_sqlQuery" id="vp_sqlQuery" value="'.htmlspecialchars($query).'"/></form>';
            echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_executeSQL.js"></script>';
            echo '<script defer>vp_executeSQLButton(true);</script>';
            return;
        }

        $timer = new VPTimer();
        $timer->Start();
        $searchResults = VPSearchEngine::ExecuteSearch($query);
        $searchTime = $timer->Stop();

        usort($searchResults, "CompareResults");

        $resultCount = sizeof($searchResults);
        if ($resultCount == 0) {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.no-results") . '</h2>';
        } else {
            echo '<p class="vp_search_result_count">' . VPLocale::Get($resultCount == 1 ? "search.result-count-one" : "search.result-count", [$resultCount, $searchTime]) . '</p>';

            echo '<div style="height: 10px;"></div>';
            $first = true;
            foreach ($searchResults as $res) {
                if ($first) $first = false;
                else echo '<hr>';
                echo '<p>';
                self::GeneratePreview($res, $permissions);
                echo '</p>';
            }

        }
    }

    private static function GeneratePreview(VPSearchResult $result, VPPermissions $permissions)
    {
        self::ShowTypeIcon($result->Type);

        switch ($result->Type) {
            case 'image':
                $img = VPImage::GetImageData($result->ObjectID);

                if ($permissions->CanEditPhoto($img->ID)) {
                    $imageURL = VPConfig::$VP_REDIRECT_URL . "/photos/edit/" . $img->ID;
                } else if (VPConfig::$VP_WEBSITE_PAGE_PHOTO != null) {
                    $imageURL = str_replace('{id}', $img->ID, VPConfig::$VP_WEBSITE_PAGE_PHOTO);
                } else {
                    $imageURL = '/' . $img->File;
                }

                echo '<a target="_blank" href="' . $imageURL . '"><strong>' . $img->Title . '</strong><br><br>';
                echo '<img src="/' . $img->PreviewFile . '" style="height: 120px; width: auto;"/></a>';
                break;

            case 'gallery':
                $gallery = VPGallery::GetGalleryData($result->ObjectID);
                $img = VPImage::GetImageData($gallery->ImageIDs[0]);

                $editURL = null;
                if ($permissions->CanEditGallery($gallery->ID)) {
                    $editURL = VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $gallery->ID;
                } else if (VPConfig::$VP_WEBSITE_PAGE_GALLERY != null) {
                    $editURL = str_replace('{id}', $gallery->ID, VPConfig::$VP_WEBSITE_PAGE_GALLERY);
                }

                if ($editURL != null) echo '<a target="_blank" href="' . $editURL . '">';

                echo '<strong>' . $gallery->Title . '</strong><br><br>';
                echo '<img src="/' . $img->PreviewFile . '" style="height: 120px; width: auto;"/>';
                if ($editURL != null) echo '</a>';
                break;

            case 'article':
                $article = VPArticle::GetArticle($result->ObjectID);

                if ($permissions->CanEditArticle($article->ID)) {
                    $articleURL = VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $article->ID;
                } else if (VPConfig::$VP_WEBSITE_PAGE_ARTICLE != null) {
                    $articleURL = $article->HumanLink;
                } else {
                    $articleURL = null;
                }

                if ($articleURL != null) echo '<a target="_blank" href="' . $articleURL . '">';
                echo '<strong>' . $article->Title . '</strong>';
                if ($articleURL != null) echo '</a>';

                $markdownParser = new Parsedown;
                $parsedText = $markdownParser->text($article->Text);
                $plainText = convert_html_to_text($parsedText);
                if (strlen($plainText) > 250) {
                    $plainText = substr($plainText, 0, 247) . "...";
                }
                echo '<div class="vp_article_list_text_teaser" style="margin: 0;"><p style="margin: 0;">' . $plainText . '</p></div>';
                break;

            case 'comment':
                $comment = VPComment::GetComment($result->ObjectID);
                $user = $comment->GetUserName();

                $commentURL = null;
                if ($permissions->CanEditComment($comment->ID)) {
                    $commentURL = VPConfig::$VP_REDIRECT_URL . '/comments/edit/' . $comment->ID;
                } else {
                    $articleID = $comment->ArticleID;
                    if ($permissions->CanEditArticle($articleID)) {
                        $commentURL = VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $articleID . '#comments';
                    } else if (VPConfig::$VP_WEBSITE_PAGE_ARTICLE != null) {
                        $commentURL = str_replace('{id}', $articleID, VPConfig::$VP_WEBSITE_PAGE_ARTICLE);
                    }
                }

                if ($commentURL != null) echo '<a target="_blank" href="' . $commentURL . '">';
                echo '<strong>' . VPLocale::Get("search-page.comment", [$user]) . '</strong>';
                if ($commentURL != null) echo '</a>';
                $markdownParser = new Parsedown;
                $parsedText = $markdownParser->text($comment->Text);
                $plainText = convert_html_to_text($parsedText);
                if (strlen($plainText) > 250) {
                    $plainText = substr($plainText, 0, 247) . "...";
                }
                echo '<div class="vp_article_list_text_teaser" style="margin: 0;"><p style="margin: 0;">' . $plainText . '</p></div>';
                break;

            case 'calendar':
                $calendar = VPCalendar::GetCalendar($result->ObjectID);

                $calendarURL = $permissions->CanViewCalendarList() ? (VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendar->ID) : null;

                if ($calendarURL != null) echo '<a target="_blank" href="' . $calendarURL . '">';
                echo '<strong>' . $calendar->Name . '</strong>';
                if ($calendarURL != null) echo '</a>';

                $dateCount = $calendar->GetDateCount();
                echo '<p>' . VPLocale::Get("search-page.date-count", [$dateCount]) . '</p>';

                break;

            case 'date':
                $date = VPCalendarDate::GetDate($result->ObjectID);
                $calendarID = $date->CalendarID;

                if ($permissions->CanEditCalendar($calendarID)) {
                    $dateURL = VPConfig::$VP_REDIRECT_URL . '/calendars/edit_date/' . $date->ID;
                } else if ($permissions->CanViewCalendarList()) {
                    $dateURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID;
                } else {
                    $dateURL = null;
                }

                if ($dateURL != null) echo '<a target="_blank" href="' . $dateURL . '">';
                echo '<strong>' . VPLocale::Get("search-page.date", [$date->Name, $date->DatetimeFormatted]) . '</strong>';
                if ($dateURL != null) echo '</a>';

                break;

            case 'user':
                $userID = $result->ObjectID;
                $currentUser = VPLogin::LoggedInUserID();

                if ($userID == $currentUser) {
                    $profileURL = VPConfig::$VP_REDIRECT_URL . '/profile';
                    echo '<a target="_blank" href="' . $profileURL . '"><strong>' . $result->Preview . '</strong></a>';
                } else if ($permissions->CanEditUser($userID)) {
                    $profileURL = VPConfig::$VP_REDIRECT_URL . '/users/edit/' . $userID;
                    echo '<a target="_blank" href="' . $profileURL . '"><strong>' . $result->Preview . '</strong></a>';
                } else {
                    echo '<strong>' . $result->Preview . '</strong>';
                }
                break;
        }
    }

    private static function ShowTypeIcon(string $type)
    {
        $imageFile = null;

        switch ($type) {
            case 'image':
                $imageFile = "PhotoDark.png";
                break;
            case 'gallery':
                $imageFile = "GalleryDark.png";
                break;
            case 'article':
                $imageFile = "ArticleDark.png";
                break;
            case 'comment':
                $imageFile = "CommentsDark.png";
                break;
            case 'calendar':
                $imageFile = "CalendarDark.png";
                break;
            case 'date':
                $imageFile = "DateDark.png";
                break;
            case 'user':
                $imageFile = "UserDark.png";
                break;
        }

        if ($imageFile != null) {
            $imageURL = VPConfig::$VP_INSTALL_DIR . 'Image/Icon/' . $imageFile;
            echo '<img src="' . $imageURL . '" style="width: 1em; height: 1em;"/>&nbsp;';
        }
    }

}

function CompareResults(VPSearchResult $a, VPSearchResult $b): int
{
    $sa = $a->Type == VPSearchResult::TYPE_COMMENT ? VPLocale::Get("search-page.comment") : $a->Preview;
    $sb = $b->Type == VPSearchResult::TYPE_COMMENT ? VPLocale::Get("search-page.comment") : $b->Preview;
    return strcmp($sa, $sb);
}