<?php

if (isset($_GET['info'])) {
    $info = $_GET['info'];
    if ($info == "pw_changed") {
        echo '<div class="vp_info_box">' . VPLocale::Get("authentication.password-changed") . '</div>';
    }
    echo '<div style="height: 20px"></div>';
}

require_once 'vp_pageUtil.php';
$permissions = VPPermissions::FromUserID(VPLogin::LoggedInUserID());

?>

    <table class="vp_dash_table">
        <?php
        if ($permissions->CanViewInbox()) {
            require_once 'vp_messages.php';
            if (VPMessagesPage::GetUnreadMessageCount() > 0) {
                echo '
                <tr>
                    <td class="vp_dash_tableCell" colspan="3">
                        <div class="vp_dash_contentHeader">Ungelesene Nachrichten
                        <span><a href="' . VPConfig::$VP_REDIRECT_URL . '/inbox">Zum Postfach</a></span>
                        </div>
                        
                        <div class="vp_dash_contentBox">';

                $sql = "SELECT * FROM `vp_messages` WHERE `Deleted` = '0' AND `Read` = '0' ORDER BY `Timestamp` ASC";
                $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
                while ($row = mysqli_fetch_assoc($result)) {
                    $msg = VPMessage::GetMessageFromRow($row);

                    echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/inbox/message/' . $msg->ID . '?from=dashboard">';
                    echo '"' . $msg->Subject . '" von ' . $msg->Sender['Name'] . '&nbsp;&nbsp;<span style="font-size: 0.6em;">' . $msg->TimestampFormatted . '</span></a>';
                }
                echo '</div></td></tr><tr style="height: 15px;"></tr>';
            }
        }
        ?>

        <tr>
            <td class="vp_dash_tableCell" colspan="3">
                <div class="vp_dash_contentHeader">Artikel
                    <?php
                    if ($permissions->CanCreateArticles()) {
                        VPPageUtil::CheckNewArticleScript();
                        echo '<span><a onclick="vp_createArticle()">Neuer Artikel</a></span>';
                    }
                    ?>
                </div>
                <div class="vp_dash_contentBox">
                    <?php
                    $articles = VPArticle::GetLatestArticles(6, false, false);
                    if (sizeof($articles) == 0) {
                        echo '<em>Keine Artikel vorhanden</em>';
                    } else {
                        foreach ($articles as $a) {
                            $editURL = VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $a->ID;
                            echo '<a href="' . $editURL . '">' . $a->Title . '</a><br>';
                        }
                        $listURL = VPConfig::$VP_REDIRECT_URL . '/articles';
                        echo '<span style="font-size: 0.7em;"><br><a href="' . $listURL . '">Alle Artikel...</a></span>';
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr style="height: 15px;"></tr>

        <tr>
            <td class="vp_dash_tableCell">
                <div class="vp_dash_contentHeader">Bilder
                    <?php
                    if ($permissions->CanUploadPhotos())
                        echo '<span><a href="' . VPConfig::$VP_REDIRECT_URL . '/photos/upload">Hochladen</a></span>';
                    ?>
                </div>
                <div class="vp_dash_contentBox">
                    <?php
                    $sql = "SELECT * FROM vp_images WHERE `GalleryID` = '' ORDER BY `Timestamp` DESC LIMIT 6";
                    $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
                    if (mysqli_num_rows($result) == 0) {
                        echo '<em>Keine Bilder vorhanden</em>';
                    } else {
                        echo '<table style="width: 100%;"><tr>';
                        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
                            $photo = VPImage::GetImageDataFromRow(mysqli_fetch_assoc($result));
                            $imageURL = $photo->PreviewFile;
                            $title = $photo->Title;

                            if ($permissions->CanEditPhoto($photo->ID)) {
                                $editURL = VPConfig::$VP_REDIRECT_URL . '/photos/edit/' . $photo->ID;
                                echo '<td title="' . $title . '" class="vp_dash_imagePreview" style="cursor: pointer; background-image: url(' . htmlspecialchars("/$imageURL") . ')" ';
                                echo 'onclick="window.location.href=\'' . htmlspecialchars($editURL) . '\'"';
                                echo '></td>';
                            } else {
                                echo '<td title="' . $title . '" class="vp_dash_imagePreview" style="background-image: url(' . htmlspecialchars("/$imageURL") . ')"></td>';
                            }

                            if ($i == 2) echo '</tr><tr>';
                        }
                        echo '</tr></table>';

                        $listURL = VPConfig::$VP_REDIRECT_URL . '/photos';
                        echo '<span style="font-size: 0.7em;"><br><a href="' . $listURL . '">Alle Bilder...</a></span>';
                    }
                    ?>
                </div>
            </td>

            <td style="width: 2%;"></td>

            <td class="vp_dash_tableCell">
                <div class="vp_dash_contentHeader">Alben
                    <?php
                    if ($permissions->CanCreateGallery()) {
                        VPPageUtil::CheckNewGalleryScript();
                        echo '<span><a onclick="vp_createGallery()">Erstellen</a></span>';
                    }
                    ?>
                </div>
                <div class="vp_dash_contentBox">
                    <?php
                    $sql = "SELECT * FROM vp_galleries ORDER BY `Timestamp` DESC LIMIT 6";
                    $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
                    if (mysqli_num_rows($result) == 0) {
                        echo '<em>Keine Alben vorhanden</em>';
                    } else {
                        echo '<table style="width: 100%;"><tr>';
                        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
                            $gallery = VPGallery::GetGalleryDataFromRow(mysqli_fetch_assoc($result));
                            if (sizeof($gallery->ImageIDs) > 0) {
                                $thumbnail = VPImage::GetImageData($gallery->ImageIDs[0]);
                                $imageURL = '/' . $thumbnail->PreviewFile;
                            } else {
                                $imageURL = VPConfig::$VP_INSTALL_DIR . 'Image/img-placeholder'.(VPConfig::IsDarkModeEnabled() ? '-dark' : '').'.png';
                            }
                            $title = htmlspecialchars($gallery->Title);

                            if ($permissions->CanEditGallery($gallery->ID)) {
                                $editURL = VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $gallery->ID;
                                echo '<td title="' . $title . '" class="vp_dash_imagePreview" style="cursor: pointer; background-image: url(' . htmlspecialchars("$imageURL") . ')" ';
                                echo 'onclick="window.location.href=\'' . htmlspecialchars($editURL) . '\'">';
                            } else {
                                echo '<td title="' . $title . '" class="vp_dash_imagePreview" style="background-image: url(' . htmlspecialchars("$imageURL") . ')">';
                            }
                            echo '<span style="background: rgba(0, 0, 0, 0.9); color: white; font-size: 0.6em; padding: 2px;">' . sizeof($gallery->ImageIDs) . '</span>';
                            echo '</td>';

                            if ($i == 2) echo '</tr><tr>';
                        }
                        echo '</tr></table>';

                        $listURL = VPConfig::$VP_REDIRECT_URL . '/galleries';
                        echo '<span style="font-size: 0.7em;"><br><a href="' . $listURL . '">Alle Alben...</a></span>';
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr style="height: 15px;"></tr>

        <tr>
            <td colspan="3" style="text-align: left;">
                <?php if ($permissions->CanViewCalendarList()): ?>
                    <a class="vp_dash_button" href="<?php echo VPConfig::$VP_REDIRECT_URL ?>/calendars/"
                       title="Kalender & Termine">
                        <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/Icon/CalendarDash.png"/>
                    </a>
                <?php endif; ?>

                <?php if ($permissions->CanViewCommentList()): ?>
                    <a class="vp_dash_button" href="<?php echo VPConfig::$VP_REDIRECT_URL ?>/comments/"
                       title="Kommentare">
                        <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/Icon/CommentsDash.png"/>
                    </a>
                <?php endif; ?>

                <?php if ($permissions->CanViewUserList()): ?>
                    <a class="vp_dash_button" href="<?php echo VPConfig::$VP_REDIRECT_URL ?>/users/" title="Benutzer">
                        <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/Icon/Users.png"/>
                    </a>
                <?php endif; ?>

                <?php if ($permissions->CanViewLogbook()): ?>
                    <a class="vp_dash_button" href="<?php echo VPConfig::$VP_REDIRECT_URL ?>/log/"
                       title="Admin-Logbuch">
                        <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/Icon/Log.png"/>
                    </a>
                <?php endif; ?>

                <?php if ($permissions->CanManageAPIKeys() || $permissions->CanExecuteSQL()): ?>
                    <a class="vp_dash_button" href="<?php echo VPConfig::$VP_REDIRECT_URL ?>/database/"
                       title="Datenkbank & API">
                        <img src="<?php echo VPConfig::$VP_INSTALL_DIR ?>Image/Icon/Database.png"/>
                    </a>
                <?php endif; ?>
            </td>
        </tr>

        <tr style="height: 15px;"></tr>

        <tr>
            <td colspan="3" style="text-align: left;">
                <?php
                foreach (VoltPages::GetPluginManager()->GetPluginList() as $plugin) {
                    foreach ($plugin->GetMenuButtons() as $button) {
                        if ($permissions->CheckPermission($button->GetRequiredPermission())) {
                            $buttonURL = VPConfig::$VP_REDIRECT_URL . '/plugin/' . $plugin->GetID() . '/' . $button->GetLink();
                            echo '<a class="vp_dash_button" href="' . $buttonURL . '" title="' . $plugin->GetName() . ': ' . $button->GetName() . '">';
                            echo '<img src="' . $plugin->GetResource($button->GetIcon()) . '"/>';
                            echo '</a>';
                        }
                    }
                }
                ?>
            </td>
        </tr>
    </table>