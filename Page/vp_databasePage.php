<?php

require_once 'vp_logger.php';
require_once 'vp_pageUtil.php';

class VPDatabasePage
{

    public static function ShowDatabasePage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if ($permissions->CanExecuteSQL()) {
            if (sizeof($path) == 0) {
                self::ShowSQLPage();
                echo '<hr style="margin: 20px 0 20px;">';
                self::ShowSQLBackup();
            } else {
                $page = $path[0];
                if ($page == "query") {
                    self::ShowSQLResults();
                    return;
                } else if ($page == "create_backup") {
                    self::CreateBackup();
                    return;
                }
            }
        }
        if ($permissions->CanManageAPIKeys()) {
            if (sizeof($path) == 0) {
                if ($permissions->CanManageAPIKeys())
                    echo '<hr style="margin: 20px 0 20px;">';
                self::ShowAPIKeyList();
            } else {
                $page = $path[0];
                if ($page == "new_key") {
                    self::NewAPIKey();
                } else if (sizeof($path) >= 2 && $page == "delete_key") {
                    self::DeleteAPIKey($path[1]);
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/database");
                }
            }
        }

        if (!$permissions->CanExecuteSQL() && !$permissions->CanManageAPIKeys()) {
            VPPermissions::NoPermissionMessage();
        }
    }

    private static function ShowSQLBackup()
    {
        echo '<h1>' . VPLocale::Get("database.sql-backup") . '</h1>';
        echo '<p>' . VPLocale::Get("database.backup-info") . '<br>';
        $submitURL = VPConfig::$VP_REDIRECT_URL . '/database/create_backup';
        echo '<a href="' . $submitURL . '" target="_blank"><button>' . VPLocale::Get('database.create-backup') . '</button></a></p>';
    }

    private static function CreateBackup()
    {
        ob_clean();
        header("Content-Type: text/plain");
        echo VPDatabaseConn::GetDatabaseConnection()->CreateBackupSQL();
        ob_flush();
        exit;
    }

    private static function ShowAPIKeyList()
    {
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/database/new_key">';
        echo '<button class="vp_dashboard_button"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Add.png"/>' . VPLocale::Get("apikeys.create.button") . '</button></a>';

        echo '<h1>' . VPLocale::Get("apikeys.header") . '</h1>';

        $sql = "SELECT * FROM vp_apiKeys ORDER BY `CreatedDate` DESC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

        echo '<p class="vp_search_result_count">' . VPLocale::Get("apikeys.search-result", [mysqli_num_rows($result)]) . '</p>';
        echo '<table class="vp_fancy_table"><tr> 
                            <th>' . VPLocale::Get("apikeys.table.id") . '</th> 
                            <th>' . VPLocale::Get("apikeys.table.key") . '</th> 
                            <th>' . VPLocale::Get("apikeys.table.owner") . '</th> 
                            <th>' . VPLocale::Get("apikeys.table.created-date") . '</th> 
                            <th>' . VPLocale::Get("apikeys.table.expire-date") . '</th> 
                            <th>' . VPLocale::Get("apikeys.table.options") . '</th> 
                        </tr>';

        if (mysqli_num_rows($result) == 0) {
            echo '<tr><td colspan="6" style="text-align: center;"><em>' . VPLocale::Get("apikeys.no-keys") . '</em></td>';
        } else {
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<tr>';
                echo '<td>' . ($id = $row['ID']) . '</td>';
                echo '<td>' . $row['Key'] . '</td>';
                echo '<td>' . VPUserData::GetUserName($row['Owner']) . '</td>';

                $time = strtotime($row['CreatedDate']);
                echo '<td>' . strftime(VPConfig::$VP_DATETIME_FORMAT, $time) . '</td>';

                if ($row['ExpireDate'] == null) {
                    echo '<td style="text-align: center;">-</td>';
                } else {
                    $time = strtotime($row['ExpireDate']);
                    if (time() > $time)
                        echo '<td><strong style="color: red;">' . strftime(VPConfig::$VP_DATETIME_FORMAT, $time) . '</strong></td>';
                    else
                        echo '<td>' . strftime(VPConfig::$VP_DATETIME_FORMAT, $time) . '</td>';
                }

                echo '<td style="text-align: center;">';
                echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/database/delete_key/' . $id . '"><button>' . VPLocale::Get("general.delete") . '</button></a>';
                echo '</td>';

                echo '</tr>';
            }
        }

        echo '</table>';
    }

    private static function NewAPIKey()
    {
        if (isset($_POST['vp_keyExecute']) && $_POST['vp_keyExecute'] == 'do') {
            $userID = VPDatabaseConn::EscapeSQLString($_POST['vp_userID']);
            $expireDate = "NULL";
            if (isset($_POST['vp_keyExpires']) && $_POST['vp_keyExpires'] == 'on') {
                $expireDate = "'" . VPDatabaseConn::EscapeSQLString($_POST['vp_expireDate']) . "'";
            }

            do {
                $apiKey = self::generateAPIKey();
                $keyFound = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_apiKeys WHERE `Key` = '$apiKey'") != null;
            } while ($keyFound);
            $sql = "INSERT INTO vp_apiKeys (`ID`, `Key`, `Owner`, `CreatedDate`, `ExpireDate`) VALUES (NULL, '$apiKey', '$userID', NOW(), $expireDate)";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

            VPLogger::GetLogger()->LogUserActivity("created API key {Key=$apiKey, OwnerID=$userID, ExpireDate=$expireDate}");

            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/database");
            return;
        }

        echo '<h1>' . VPLocale::Get("apikeys.create.button") . '</h1>';
        echo '<form method="post"><input type="hidden" name="vp_keyExecute" value="do"/><table>';

        echo '<tr><td>' . VPLocale::Get("apikeys.create.owner") . '</td><td>';
        require_once 'vp_pageUtil.php';
        VPPageUtil::CreateUserDropdown("vp_userID");
        echo '</td></tr>';

        echo '<tr><td></td> <td><input type="checkbox" id="vp_expireChbx" name="vp_keyExpires" onclick="vpApikeyChbx();"/> ' . VPLocale::Get("apikeys.create.set-expire-date") . '</td></tr>';

        echo '<tr style="display: none;" id="vp_expireDateRow"><td>' . VPLocale::Get("apikeys.create.expire-date") . ':</td> <td><input id="vp_expireDate" name="vp_expireDate" type="datetime-local"/></td>';

        echo '<tr><td></td><td><button>' . VPLocale::Get("apikeys.create.confirm") . '</button></td></tr>';
        echo '</table></form>';

        echo '<script>function vpApikeyChbx() {
                    if(document.getElementById(\'vp_expireChbx\').checked) {
                        document.getElementById(\'vp_expireDateRow\').style.display = \'table-row\';
                        document.getElementById(\'vp_expireDate\').required = true;
                    } else {
                        document.getElementById(\'vp_expireDateRow\').style.display = \'none\';
                        document.getElementById(\'vp_expireDate\').required = false;
                    }
                }vpApikeyChbx();</script>';
    }

    private static function DeleteAPIKey($keyID)
    {
        $sql = "SELECT * FROM vp_apiKeys WHERE ID = '$keyID'";
        $info = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);

        if ($info == null) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/database");
            exit;
        }

        echo '<h1>' . VPLocale::Get("apikeys.delete.header") . '</h1>';

        if (isset($_POST['vp_delKeyExecute']) && $_POST['vp_delKeyExecute'] == 'do') {
            $sql = "DELETE FROM vp_apiKeys WHERE ID = '$keyID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

            $apiKey = $info['Key'];
            $ownerID = $info['Owner'];
            VPLogger::GetLogger()->LogUserActivity("deleted API key {ID=$keyID, Key=$apiKey, OwnerID=$ownerID}");

            echo '<p>' . VPLocale::Get("apikeys.delete.done") . '</p>';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/database"><button>' . VPLocale::Get("general.back-overview") . '</button></a>';
            return;
        }

        echo '<p>' . VPLocale::Get("apikeys.delete.warning") . '</p>';

        echo '<table>';
        echo '<tr><td><strong>' . VPLocale::Get("apikeys.delete.id") . '</strong></td><td>' . $keyID . '</td>';
        echo '<tr><td><strong>' . VPLocale::Get("apikeys.delete.key") . '</strong></td><td>' . $info['Key'] . '</td>';
        echo '<tr><td><strong>' . VPLocale::Get("apikeys.delete.owner") . '</strong></td><td>' . VPUserData::GetUserName($info['Owner']) . '</td>';
        echo '<tr><td><strong>' . VPLocale::Get("apikeys.delete.created-date") . '</strong></td><td>' . strftime(VPConfig::$VP_DATETIME_FORMAT, strtotime($info['CreatedDate'])) . '</td>';
        echo $info['ExpireDate'] == null ? '' : '<tr><td><strong>' . VPLocale::Get("apikeys.delete.expire-date") . '</strong></td><td>' . strftime(VPConfig::$VP_DATETIME_FORMAT, strtotime($info['ExpireDate'])) . '</td>';
        echo '</table>';

        echo '<form method="post" style="margin-top: 20px;"><input name="vp_delKeyExecute" value="do" type="hidden"/>';
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/database"><button type="button">' . VPLocale::Get("general.back") . '</button></a>&nbsp;<button>' . VPLocale::Get("general.delete") . '</button>';
        echo '</form>';
    }

    private static function generateAPIKey()
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 20; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private static function ShowSQLPage()
    {
        $actionURL = VPConfig::$VP_REDIRECT_URL . '/database/query';
        echo '<h1>' . VPLocale::Get("database.sql-query") . '</h1>';
        echo '<form onsubmit="vp_formExecute()" id="vp_sqlForm" action="' . $actionURL . '" method="post">';
        echo '<input style="width: 500px;" name="vp_sqlQuery" id="vp_sqlQuery" placeholder="SQL-Anfrage hier eingeben"/>&nbsp;';
        echo '<button type="button" onclick="vp_executeSQLButton()">' . VPLocale::Get("database.execute") . '</button>';
        echo '</form>';
        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_executeSQL.js"></script>';
    }

    private static function ShowSQLResults()
    {
        if (isset($_POST['vp_sqlQuery'])) {
            $query = $_POST['vp_sqlQuery'];
            $conn = VPDatabaseConn::GetDatabaseConnection()->GetMySQLiConnection();

            $actionURL = VPConfig::$VP_REDIRECT_URL . '/database/query';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/database">' . VPLocale::Get("general.back") . '</a>';
            echo '<h1>' . VPLocale::Get("database.sql-query") . '</h1>';
            echo '<form onsubmit="vp_formExecute()" id="vp_sqlForm" action="' . $actionURL . '" method="post">';
            echo '<input style="width: 500px;" name="vp_sqlQuery" id="vp_sqlQuery" value="' . htmlspecialchars($query) . '" placeholder="SQL-Anfrage hier eingeben" autofocus/>&nbsp;';
            echo '<button type="button" onclick="vp_executeSQLButton()">' . VPLocale::Get("database.execute") . '</button>';
            echo '</form>';
            echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_executeSQL.js"></script>';

            $timer = new VPTimer();
            $timer->Start();
            $result = mysqli_query($conn, $query);
            $searchTime = $timer->Stop();

            echo '<p>' . mysqli_info($conn) . '</p>';
            echo '<p class="vp_search_result_count">' . mysqli_affected_rows($conn) . ' rows affected. Query took ' . $searchTime . '</p>';

            $fields = mysqli_fetch_fields($result);
            echo '<table class="vp_fancy_table"><tr>';
            $fieldNames = array();
            foreach ($fields as $f) {
                echo '<th>' . $f->name . '</th>';
                $fieldNames[] = $f->name;
            }
            echo '</tr>';
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<tr>';
                foreach ($fieldNames as $f) {
                    echo '<td>' . htmlspecialchars(utf8_encode($row[$f])) . '</td>';
                }
                echo '</tr>';
            }

            echo '</table>';
        } else {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/database");
        }
    }

}