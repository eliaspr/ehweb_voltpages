<?php

require_once __DIR__ . '/../vp_config.php';
require_once __DIR__ . '/../vp_login.php';

class VPLogger
{

    private static $logger;

    public static function GetLogger()
    {
        return VPLogger::$logger != null ? VPLogger::$logger : (VPLogger::$logger = new VPLogger);
    }

    private $filePath;

    private function __construct()
    {
        if (!VPConfig::$VP_ENABLE_LOGGING) return;

        $directory = VPConfig::GetLogDirectory();
        if (!file_exists($directory)) mkdir($directory);
        $filename = date("Y-M-d");
        $this->filePath = $directory . $filename;
    }

    private function LogLine($line)
    {
        if (!VPConfig::$VP_ENABLE_LOGGING) return;

        file_put_contents($this->filePath, $line . PHP_EOL, FILE_APPEND);
    }

    public function LogUserActivity($description)
    {
        if (!VPConfig::$VP_ENABLE_LOGGING) return;

        $line = strftime(VPConfig::$VP_DATETIME_FORMAT);
        if (VPLogin::IsUserLoggedIn()) {
            $userID = VPLogin::LoggedInUserID();
            $line .= ": User ID $userID (" . VPUserData::GetUserName($userID) . "): ";
        } else {
            $line .= ": <no user logged in>: ";
        }
        $line .= $description;
        if (VPConfig::$VP_LOGGER_SAVE_IP) $line .= " [" . $_SERVER['REMOTE_ADDR'] . "]";
        $this->LogLine($line);
    }

}