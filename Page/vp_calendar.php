<?php

require_once 'vp_logger.php';

class VPCalendarPage
{

    private static $MULTIPLE_DATE_COUNT = 15;

    public static function ShowCalendarPage($path)
    {
        $permissions = VPPermissions::FromUserID(VPLogin::LoggedInUserID());
        if (sizeof($path) == 0) {
            if ($permissions->CanViewCalendarList()) {
                self::ShowOverviewPage($permissions);
            } else {
                VPPermissions::NoPermissionMessage();
            }
        } else {
            $page = $path[0];

            if ($page == "create") {
                if ($permissions->CanCreateCalendars()) {
                    self::CreateNewCalendar();
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else if ($page == "view") {
                if (sizeof($path) >= 2) {
                    $calendarID = $path[1];
                    if ($permissions->CanViewCalendarList() || $permissions->CanEditCalendar($calendarID)) {
                        self::ShowViewPage($calendarID, $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
                }
            } else if ($page == "add_date") {
                if (sizeof($path) >= 2) {
                    $calendarID = $path[1];
                    if ($permissions->CanEditCalendar($calendarID)) {
                        self::ShowAddDatePage($calendarID);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
                }
            } else if ($page == "add_dates") {
                if (sizeof($path) >= 2) {
                    $calendarID = $path[1];
                    if ($permissions->CanEditCalendar($calendarID)) {
                        self::ShowAddDatesPage($calendarID);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
                }
            } else if ($page == "edit_date") {
                if (sizeof($path) >= 2) {
                    $dateID = $path[1];
                    if ($permissions->CanEditCalendar($dateID)) {
                        self::ShowEditDatePage($dateID);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
                }
            } else if ($page == "delete_date") {
                if (sizeof($path) >= 2) {
                    $dateID = $path[1];
                    if ($permissions->CanEditCalendar($dateID)) {
                        self::DeleteDate($dateID);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
                }
            } else if ($page == "delete") {
                if (sizeof($path) >= 2) {
                    $calendarID = $path[1];
                    if ($permissions->CanEditCalendar($calendarID)) {
                        self::ShowDeletePage($calendarID);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
                }
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
            }
        }
    }

    private static function ShowOverviewPage(VPPermissions $permissions)
    {
        if ($permissions->CanCreateCalendars()) {
            echo '<span style="display: none;" id="vp_newCalendarPrompt">' . VPLocale::Get("calendars.name-prompt") . '</span>';
            echo '<span style="display: none;" id="vp_newCalendarURL">' . VPConfig::$VP_REDIRECT_URL . '/calendars/create</span>';
            $createURL = VPConfig::$VP_REDIRECT_URL . '/calendars/create';
            echo '<a href="' . $createURL . '"><button class="vp_dashboard_button"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Calendar.png"/>' . VPLocale::Get("calendars.create-new") . '</button></a>';
        }

        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_calendarList.js"></script>';
        echo '<h1>' . VPLocale::Get("calendars.header") . '</h1>';

        if (isset($_GET['status'])) {
            $status = $_GET['status'];
            switch ($status) {
                case 'calendar_created':
                    echo '<p class="vp_info_box">' . VPLocale::Get("calendars.created") . '</p>';
                    break;
                case 'date_added':
                    echo '<p class="vp_info_box">' . VPLocale::Get("calendars.date-added") . '</p>';
                    break;
                case 'dates_added':
                    echo '<p class="vp_info_box">' . VPLocale::Get("calendars.dates-added") . '</p>';
                    break;
            }
        }

        $sql = "SELECT * FROM vp_calendars ORDER BY Name ASC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            echo '<p style="margin-top: 50px;">' . VPLocale::Get("calendars.no-results") . '</p>';
        } else {
            echo '<table class="vp_fancy_table"><tr>';
            {
                echo '<th>' . VPLocale::Get("calendars.attribs.name") . '</th>';
                echo '<th>' . VPLocale::Get("calendars.attribs.dates") . '</th>';
                echo '<th>' . VPLocale::Get("calendars.attribs.options") . '</th>';
            }
            echo '</tr>';
            while ($row = mysqli_fetch_assoc($result)) {
                $calendar = VPCalendar::GetCalendarFromRow($row);
                $calendarID = $calendar->ID;
                $dateCount = mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery("SELECT ID FROM vp_dates WHERE CalendarID = '$calendarID'"));

                echo '<tr>';
                echo '<td>' . $calendar->Name . '</td>';
                echo '<td style="text-align: center;">' . $dateCount . '</td>';
                echo '<td>';
                {
                    $openURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID;
                    echo '<a href="' . $openURL . '"><button>' . VPLocale::Get("calendars.open-calendar") . '</button></a>&nbsp;&nbsp;';

                    if ($permissions->CanEditCalendar($calendarID)) {
                        $addImgPath = VPConfig::$VP_INSTALL_DIR . 'Image/Icon/AddDark.png';
                        $addDateURL = VPConfig::$VP_REDIRECT_URL . '/calendars/add_date/' . $calendarID . '?from=list';
                        echo '<a href="' . $addDateURL . '"><button>' . VPLocale::Get("calendars.add-date", array('<img style="width: 10px;" src="' . $addImgPath . '"/>')) . '</button></a>';
                        echo '&nbsp;&nbsp;';

                        $trashIcon = VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Trash.png';
                        $trashURL = VPConfig::$VP_REDIRECT_URL . '/calendars/delete/' . $calendarID;
                        echo '<a href="' . $trashURL . '"><button>' . VPLocale::Get("calendars.delete.button", array('<img style="width: 10px;" src="' . $trashIcon . '"/>')) . '</button></a>';
                    }
                }
                echo '</td></tr>';
            }
            echo '</table>';
        }
    }

    private static function CreateNewCalendar()
    {
        if (isset($_POST['vp_calendarName'])) {
            $ownerID = VPLogin::LoggedInUserID();
            $name = VPDatabaseConn::EscapeSQLString($_POST['vp_calendarName']);
            $sql = "INSERT INTO vp_calendars (ID, Name, OwnerID) VALUES (NULL, '$name', '$ownerID')";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            $calendarID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_calendars ORDER BY ID DESC LIMIT 1")['ID'];
            VPLogger::GetLogger()->LogUserActivity("created calendar {ID=$calendarID,Name=\"$name\"}");
            header("Location: " . VPConfig::$VP_REDIRECT_URL . '/calendars?status=calendar_created');
        } else {
            echo '<h1>' . VPLocale::Get("calendars.new-calendar.header") . '</h1>';
            $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars';
            echo '<p><a href="' . $backURL . '">' . VPLocale::Get("general.back-overview") . '</a></p>';
            echo '<form method="post"><div style="margin-top: 30px; border: 1px solid black; padding: 7px; display: inline-block;">';
            echo VPLocale::Get("calendars.new-calendar.name") . '&nbsp;<input name="vp_calendarName" required/>&nbsp;';
            echo '<button>' . VPLocale::Get("calendars.new-calendar.submit") . '</button>';
            echo '</div></form>';
        }
    }

    private static function ShowDeletePage($calendarID)
    {
        $sql = "SELECT * FROM vp_calendars WHERE ID = '$calendarID'";
        $info = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);

        if ($info == null) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/calendars");
            exit;
        }

        echo '<h1>' . VPLocale::Get("calendars.delete.header") . '</h1>';

        if (isset($_POST['vp_confirmDelete']) && $_POST['vp_confirmDelete'] == 'do') {
            $sql = "DELETE FROM vp_calendars WHERE ID = '$calendarID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

            $sql = "DELETE FROM vp_dates WHERE CalendarID = '$calendarID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

            $calendarName = $info['Name'];
            $ownerID = $info['OwnerID'];
            VPLogger::GetLogger()->LogUserActivity("deleted calendar {ID=$calendarID, Name=\"$calendarName\", OwnerID=$ownerID}");

            echo '<p>' . VPLocale::Get("calendars.delete.done") . '</p>';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/calendars"><button>' . VPLocale::Get("general.back-overview") . '</button></a>';
            return;
        }

        $datesFuture = mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery("SELECT * FROM vp_dates WHERE `CalendarID` = '$calendarID' AND `Date` > NOW()"));
        $datesPast = mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery("SELECT * FROM vp_dates WHERE `CalendarID` = '$calendarID' AND `Date` <= NOW()"));

        echo '<p>' . VPLocale::Get("calendars.delete.warning") . '</p>';

        echo '<table>';
        echo '<tr><td><strong>' . VPLocale::Get("calendars.delete.name") . '</strong></td><td>' . $info['Name'] . '</td></tr>';
        echo '<tr><td><strong>' . VPLocale::Get("calendars.delete.owner") . '</strong></td><td>' . VPUserData::GetUserName($info['OwnerID']) . '</td></tr>';
        echo '<tr></tr>';
        echo '<tr><td><strong>' . VPLocale::Get("calendars.delete.date-count") . '</strong></td><td>' . ($datesFuture + $datesPast) . '</td></tr>';
        echo '<tr style="font-size: 0.8em;"><td><strong style="padding-left: 20px;">' . VPLocale::Get("calendars.delete.date-count-past") . '</strong></td><td>' . $datesPast . '</td></tr>';
        echo '<tr style="font-size: 0.8em;"><td><strong style="padding-left: 20px;">' . VPLocale::Get("calendars.delete.date-count-future") . '</strong></td><td>' . $datesFuture . '</td></tr>';
        echo '</table>';

        echo '<form method="post" style="margin-top: 20px;"><input name="vp_confirmDelete" value="do" type="hidden"/>';
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/calendars"><button type="button">' . VPLocale::Get("general.back") . '</button></a>&nbsp;<button>' . VPLocale::Get("general.delete") . '</button>';
        echo '</form>';
    }

    private static function ShowViewPage($calendarID, VPPermissions $permissions)
    {
        $canEdit = $permissions->CanEditCalendar($calendarID);
        $calendar = VPCalendar::GetCalendar($calendarID);
        echo '<h1>' . VPLocale::Get("calendars.view.header", [$calendar->Name]) . '</h1>';

        $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars';
        echo '<a href="' . $backURL . '">' . VPLocale::Get("general.back-overview") . '</a>';

        if (isset($_GET['status'])) {
            $status = $_GET['status'];
            switch ($status) {
                case 'date_added':
                    echo '<p class="vp_info_box">' . VPLocale::Get("calendars.date-added") . '</p>';
                    break;
                case 'date_deleted':
                    echo '<p class="vp_info_box">' . VPLocale::Get("calendars.date-deleted") . '</p>';
                    break;
            }
        }

        if ($canEdit) {
            $addDateURL = VPConfig::$VP_REDIRECT_URL . '/calendars/add_date/' . $calendarID . '?from=calender';
            echo '<p><a href="' . $addDateURL . '"><button>' . VPLocale::Get("calendars.view.add-date") . '</button></a></p>';
        }

        $sessionStorage = 'vp_date_search_' . $calendarID;
        if (isset($_GET['year'])) {
            $currentYear = $_GET['year'];
        } else if (isset($_SESSION[$sessionStorage])) {
            $currentYear = $_SESSION[$sessionStorage];
        } else {
            $currentYear = date("Y");
        }
        $_SESSION[$sessionStorage] = $currentYear;

        echo '<div style="height: 15px;"><div style="background: #BBBBBB; display: inline-block; margin: 20px 0 0 0; padding: 5px;">';
        for ($i = $currentYear - 3; $i <= $currentYear + 3; $i++) {
            if ($i == $currentYear) {
                echo "<strong>$i</strong>";
            } else {
                $yearURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID . '?year=' . $i;
                echo '<a href="' . $yearURL . '"><button>' . $i . '</button></a>';
            }
            if ($i <= $currentYear + 2) echo '&nbsp;&nbsp;&nbsp;';
        }
        echo '</div>';

        $fromTime = VPDatabaseConn::EscapeSQLString($currentYear . "-01-01 00:00:00");
        $toTime = VPDatabaseConn::EscapeSQLString($currentYear . "-12-31 23:59:59");

        $sql = "SELECT * FROM vp_dates WHERE CalendarID = '$calendarID' AND `Date` > '$fromTime' AND `Date` < '$toTime' ORDER BY `Date` ASC";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

        if (mysqli_num_rows($result) == 0) {
            echo '<p style="color: red; margin: 40px 0 0 20px;"><em>' . VPLocale::Get("calendars.view.no-found", [$currentYear]) . '</em></p>';
        } else {
            echo '<div style="height: 30px;"></div>';
            $lastMonthID = -1;
            while ($row = mysqli_fetch_assoc($result)) {
                $date = VPCalendarDate::GetDateFromRow($row);
                $monthID = date("m", $date->Timestamp);
                if ($monthID != $lastMonthID) {
                    if ($lastMonthID > 0) echo '</table>';
                    $lastMonthID = $monthID;
                    $monthName = VPLocale::Get("general.months." . $monthID);
                    echo '<div style="height: 30px;"></div><hr><h2 style="margin-top: 0;">' . $monthName . '</h2><table>';
                }
                echo '<tr>';
                echo '<td style="width: 350px;">' . $date->DatetimeFormatted . '</td>';
                if ($canEdit) {
                    $editURL = VPConfig::$VP_REDIRECT_URL . "/calendars/edit_date/" . $date->ID;
                    $deleteURL = VPConfig::$VP_REDIRECT_URL . "/calendars/delete_date/" . $date->ID;
                    echo '<td>';
                    echo '<a href="' . $editURL . '"><img title="' . VPLocale::Get("general.edit") . '" style="width: 12px; margin-right: 7px;" src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Pencil.png' . '"/></a>';
                    echo '<a href="' . $deleteURL . '"><img title="' . VPLocale::Get("general.delete") . '" style="width: 12px; margin-right: 7px;" src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Delete.png' . '"/></a>';
                    echo '</td>';
                }
                echo '<td style="width: 250px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">' . $date->Name . '</td>';
                echo '<td style="width: 300px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; display: block;" title="' . htmlspecialchars($date->Info) . '">' . $date->Info . '</td>';
                echo '</tr>';
            }
            echo '</table><div style="height: 30px;"></div>';
        }
    }

    private static function ShowAddDatesPage($calendarID)
    {
        $fromOverview = isset($_GET['from']) && $_GET['from'] == 'list';

        if (isset($_POST['dates_add']) && $_POST['dates_add'] == 'true') {
            for ($i = 0; $i < VPCalendarPage::$MULTIPLE_DATE_COUNT; $i++) {
                $i_date = "date" . $i . "_datetime";
                $i_name = "date" . $i . "_name";
                if (isset($_POST[$i_date]) && isset($_POST[$i_name])) {
                    $date = trim($_POST[$i_date]);
                    $name = trim($_POST[$i_name]);
                    if (strlen($date) > 0 && strlen($name) > 0) {
                        $date = VPDatabaseConn::EscapeSQLString($date);
                        $name = VPDatabaseConn::EscapeSQLString($name);

                        $currentUserID = VPLogin::LoggedInUserID();
                        $sql = "INSERT INTO vp_dates (CalendarID, Name, Date, Info, CreatedBy) VALUES ('$calendarID', '$name', '$date', '', '$currentUserID')";
                        VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));

                        $dateID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_dates ORDER BY ID DESC LIMIT 1")['ID'];
                        VPLogger::GetLogger()->LogUserActivity("added date to calendar {ID=$dateID,CalendarID=$calendarID,Name=\"$name\",Date=\"$date\"}");
                    }
                }
            }
            if ($fromOverview) {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . '/calendars?status=dates_added');
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID . '?status=dates_added');
            }
            exit;
        }

        $calendar = VPCalendar::GetCalendar($calendarID);
        echo '<h1>' . VPLocale::Get("calendars.add_dates.header", [$calendar->Name]) . '</h1>';

        if ($fromOverview) {
            $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars';
            echo '<p><a href="' . $backURL . '">' . VPLocale::Get("general.back-overview") . '</a></p>';
        } else {
            $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID;
            echo '<p><a href="' . $backURL . '">' . VPLocale::Get("calendars.add_date.back") . '</a></p>';
        }

        echo '<form id="vp_newDatesForm" method="post"><input type="hidden" name="dates_add" value="true"/>';
        echo '<div class="vp_edit_box"><div class="vp_edit_box_header">';
        echo VPLocale::Get("calendars.add_dates.new-dates") . '<span style="float: right">';
        echo '<span class="vp_save_button" onClick="document.forms[\'vp_newDatesForm\'].submit();">' . VPLocale::Get("calendars.add_dates.finish") . '</span></span>';
        echo '</div><div class="vp_edit_box_body">';
        echo '<p>' . VPLocale::Get("calendars.add_dates.notice") . '</p>';
        $now = date("Y-m-d") . 'T18:00';

        echo '<table border="1"><tr>';
        echo '<th>' . VPLocale::Get("calendars.add_dates.attribs.date") . '</th>';
        echo '<th>' . VPLocale::Get("calendars.add_dates.attribs.name") . '</th>';
        echo '</tr>';
        for ($i = 0; $i < VPCalendarPage::$MULTIPLE_DATE_COUNT; $i++) {
            echo '<tr>';
            echo '<td><input type="datetime-local" name="date' . $i . '_datetime" value="' . $now . '"' . ($i == 0 ? ' autofocus' : '') . '/></td>';
            echo '<td><input type="text" name="date' . $i . '_name"/></td>';
            echo '</tr>';
        }
        echo '</table>';

        echo '</form>';
    }

    private static function ShowAddDatePage($calendarID)
    {
        $fromOverview = isset($_GET['from']) && $_GET['from'] == 'list';

        if (isset($_POST['date_add']) && $_POST['date_add'] == 'true' && isset($_POST['date_datetime']) && isset($_POST['date_name'])) {
            $date = ($_POST['date_datetime']);
            $name = ($_POST['date_name']);

            if (strlen($date) > 0 && strlen($name) > 0) {
                $date = VPDatabaseConn::EscapeSQLString($date);
                $name = VPDatabaseConn::EscapeSQLString($name);

                $userID = VPLogin::LoggedInUserID();
                $sql = "INSERT INTO vp_dates (CalendarID, Name, Date, Info, CreatedBy) VALUES ('$calendarID', '$name', '$date', '', '$userID')";
                VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));

                $dateID = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT ID FROM vp_dates ORDER BY ID DESC LIMIT 1")['ID'];
                VPLogger::GetLogger()->LogUserActivity("added date to calendar {ID=$dateID,CalendarID=$calendarID,Name=\"$name\",Date=\"$date\"}");

                if ($fromOverview) {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . '/calendars?status=date_added');
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID . '?status=date_added');
                }
                exit;
            }
        }

        $calendar = VPCalendar::GetCalendar($calendarID);
        echo '<h1>' . VPLocale::Get("calendars.add_date.header", [$calendar->Name]) . '</h1>';

        if ($fromOverview) {
            $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars';
            echo '<p><a href="' . $backURL . '">' . VPLocale::Get("general.back-overview") . '</a></p>';
        } else {
            $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID;
            echo '<p><a href="' . $backURL . '">' . VPLocale::Get("calendars.add_date.back") . '</a></p>';
        }

        echo '<form id="vp_newDateForm" method="post"><input type="hidden" name="date_add" value="true"/>';
        echo '<div class="vp_edit_box"><div class="vp_edit_box_header">';
        echo VPLocale::Get("calendars.add_date.new-date") . '<span style="float: right">';
        $multipleLink = VPConfig::$VP_REDIRECT_URL . '/calendars/add_dates/' . $calendarID . '?from=' . ($fromOverview ? 'list' : 'calendar');
        echo '<a class="vp_answer_button" style="margin-right: 7px; text-decoration: none;" href="' . $multipleLink . '">' . VPLocale::Get("calendars.add_date.multiple") . '</a>';
        echo '<span class="vp_save_button" onClick="document.forms[\'vp_newDateForm\'].submit();">' . VPLocale::Get("calendars.add_date.finish") . '</span></span>';
        echo '</div><div class="vp_edit_box_body"><table>';

        $now = date("Y-m-d") . 'T18:00';
        echo '<tr><td>' . VPLocale::Get("calendars.add_date.attribs.date") . '</td><td><input type="datetime-local" name="date_datetime" value="' . $now . '" autofocus required/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("calendars.add_date.attribs.name") . '</td><td><input type="text" name="date_name" required/></td></tr>';

        echo '</table></form>';
    }

    private static function DeleteDate($dateID)
    {
        $dateInfo = VPCalendarDate::GetDate($dateID);

        if (isset($_GET['execute']) && $_GET['execute'] == 'true') {
            $sql = "DELETE FROM vp_dates WHERE ID = '$dateID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

            VPLogger::GetLogger()->LogUserActivity("deleted date {ID=$dateID,Name=\"$dateInfo->Name\",Date=\"$dateInfo->DatetimeFormatted\"}");

            $redirectURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $dateInfo->CalendarID . '?status=date_deleted';
            header("Location: $redirectURL");
            exit;
        }

        echo '<h1>' . VPLocale::Get("calendars.delete-date.header") . '</h1>';
        echo '<p><strong style="color: red;">' . VPLocale::Get("calendars.delete-date.message") . '</strong></p>';

        echo '<table style="border: 1px solid black; padding: 5px;"">';
        echo '<tr><td>' . VPLocale::Get("calendars.delete-date.attribs.calendar") . '</td><td>' . VPCalendar::GetCalendarName($dateInfo->CalendarID) . '</td></tr>';
        echo '<tr><td>' . VPLocale::Get("calendars.delete-date.attribs.datetime") . '</td><td>' . $dateInfo->DatetimeFormatted . '</td></tr>';
        echo '<tr><td>' . VPLocale::Get("calendars.delete-date.attribs.name") . '</td><td>' . $dateInfo->Name . '</td></tr>';
        echo '</table>';

        $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $dateInfo->CalendarID;
        if (isset($_GET['from'])) {
            $from = $_GET['from'];
            if ($from == 'edit') {
                $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars/edit_date/' . $dateInfo->ID;
            }
        }

        echo '<p>';
        echo '<a href="' . $backURL . '">' . VPLocale::Get("general.back") . '</a>&nbsp;&nbsp;';
        echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/calendars/delete_date/' . $dateInfo->ID . '?execute=true"><button>' . VPLocale::Get("general.delete") . '</button></a>';
        echo '</p>';
    }

    private static function ShowEditDatePage($dateID)
    {
        $date = VPCalendarDate::GetDate($dateID);
        $calendarID = $date->CalendarID;
        $calendar = VPCalendar::GetCalendar($calendarID);

        echo '<h1>' . VPLocale::Get("calendars.edit-date.header", [$calendar->Name]) . '</h1>';

        $backURL = VPConfig::$VP_REDIRECT_URL . '/calendars/view/' . $calendarID;
        echo '<p><a href="' . $backURL . '">' . VPLocale::Get("calendars.edit-date.back") . '</a></p>';

        if (isset($_POST['vp_dateUpdate']) && $_POST['vp_dateUpdate'] && isset($_POST['vp_dateDatetime']) && isset($_POST['vp_dateName']) && isset($_POST['vp_dateInfo'])) {
            $date = VPDatabaseConn::EscapeSQLString(trim($_POST['vp_dateDatetime']));
            $name = VPDatabaseConn::EscapeSQLString(trim($_POST['vp_dateName']));
            $info = VPDatabaseConn::EscapeSQLString(trim($_POST['vp_dateInfo']));

            $sql = "UPDATE vp_dates SET `Date` = '$date', `Name` = '$name', Info = '$info' WHERE ID = '$dateID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
            echo '<p class="vp_info_box">' . VPLocale::Get("calendars.edit-date.updated") . '</p>';

            $date = VPCalendarDate::GetDate($dateID);
            VPLogger::GetLogger()->LogUserActivity("updated date information {ID=$dateID,Name=\"$date->Name\",Date=\"$date->DatetimeFormatted\"}");
        }

        echo '<span style="display: none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved-changes") . '</span>';
        echo '<span style="display: none;" id="vp_dateDeleteURL">' . VPConfig::$VP_REDIRECT_URL . '/calendars/delete_date/' . $dateID . '?from=edit</span>';

        echo '<form id="vp_editDateForm" method="post"><input type="hidden" name="vp_dateUpdate" value="true"/>';
        echo '<div class="vp_edit_box"><div class="vp_edit_box_header">';
        echo VPLocale::Get("calendars.edit-date.date-settings") . '<span style="float: right">';
        echo '<span class="vp_delete_button" onClick="vp_dateDeleteButton();">' . VPLocale::Get("general.delete") . '</span>';
        echo '<span class="vp_save_button" onClick="vp_dateSubmitButton();">' . VPLocale::Get("general.save") . '</span></span>';
        echo '</div><div class="vp_edit_box_body"><table>';

        $currentDatetime = date("Y-m-d", $date->Timestamp) . 'T' . date("H:i", $date->Timestamp);
        echo '<tr><td>' . VPLocale::Get("calendars.edit-date.attribs.calendar") . '</td><td><input value="' . VPCalendar::GetCalendarName($date->CalendarID) . '" disabled/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("calendars.edit-date.attribs.date") . '</td><td><input type="datetime-local" name="vp_dateDatetime" value="' . $currentDatetime . '" required oninput="vp_dateInput()"/></td></tr>';
        echo '<tr><td>' . VPLocale::Get("calendars.edit-date.attribs.name") . '</td><td><input name="vp_dateName" value="' . $date->Name . '" required oninput="vp_dateInput()"/></td></tr>';
        echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("calendars.edit-date.attribs.info") . '</td><td><textarea cols="35" rows="6" style="font-family: sans-serif;" name="vp_dateInfo">' . $date->Info . '</textarea></td></tr>';

        echo '</table></form>';
        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_dateEdit.js"></script>';
    }

}