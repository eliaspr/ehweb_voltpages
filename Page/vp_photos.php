<?php

require_once 'vp_logger.php';
require_once 'vp_pageUtil.php';

class VPPhotosPage
{

    public static function ShowPhotosPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if (sizeof($path) == 0) {
            if ($permissions->CanViewPhotoList()) {
                VPPhotosPage::ShowOverviewPage($path, $permissions);
            } else {
                VPPermissions::NoPermissionMessage();
            }
        } else {
            $page = $path[0];

            if ($page == "upload") {
                if ($permissions->CanUploadPhotos()) {
                    VPPhotosPage::ShowUploadPage();
                } else {
                    VPPermissions::NoPermissionMessage();
                }
            } else if ($page == "edit") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditPhoto($path[1])) {
                        VPPhotosPage::ShowEditPage($path[1], $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/photos");
                    exit;
                }
            } else if ($page == "delete") {
                if (sizeof($path) >= 2) {
                    if ($permissions->CanEditPhoto($path[1])) {
                        VPPhotosPage::ShowOverviewPage($path, $permissions);
                    } else {
                        VPPermissions::NoPermissionMessage();
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . "/photos");
                    exit;
                }
            } else {
                header("Location: " . VPConfig::$VP_REDIRECT_URL . "/photos");
                exit;
            }
        }
    }

    private static function ShowUploadPage()
    {
        if (empty($_FILES)) {
            require 'vp_photoUploadForm.php';
        } else {
            VPPhotosPage::ProcessImageUpload();
        }
    }

    private static function ShowEditPage($imageID, VPPermissions $permissions)
    {
        echo '<h1>' . VPLocale::Get("photos.edit") . '</h1>';

        if (isset($_GET['cameFrom'])) {
            if ($_GET['cameFrom'] == "article" && isset($_GET['articleID'])) {
                $articleID = $_GET['articleID'];
                echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $articleID . '">' . VPLocale::Get("photos.back-article") . '</a></p>';
            } else if ($_GET['cameFrom'] == "gallery" && isset($_GET['galleryID'])) {
                $galleryID = $_GET['galleryID'];
                echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/galleries/edit/' . $galleryID . '">' . VPLocale::Get("photos.back-gallery") . '</a></p>';
            }
        } else {
            echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/photos">' . VPLocale::Get("general.back-overview") . '</a>';
            if (VPConfig::$VP_WEBSITE_PAGE_PHOTO != null) {
                echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
                echo '<a target="_blank" href="' . str_replace("{id}", $imageID, VPConfig::$VP_WEBSITE_PAGE_PHOTO) . '">' . VPLocale::Get("photos.view-website") . '</a>';
            }
            echo '</p>';
        }

        if (isset($_POST['img_title']) || isset($_POST['img_photograph']) || isset($_POST['img_info'])) {
            $title = VPDatabaseConn::EscapeSQLString($_POST['img_title']);
            $photograph = VPDatabaseConn::EscapeSQLString($_POST['img_photograph']);
            $info = VPDatabaseConn::EscapeSQLString($_POST['img_info']);

            $updateSQL = "UPDATE `vp_images` SET `Title` = '$title', `Photographer` = '$photograph', `Info` = '$info' WHERE `ID` = '$imageID'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($updateSQL));

            echo '<p class="vp_info_box">' . VPLocale::Get("photos.updated") . '</p>';
            VPLogger::GetLogger()->LogUserActivity("updated image {ID=$imageID}");
        }

        if (isset($_POST['img_transform'])) {
            self::PerformTransformation($imageID, $_POST['img_transform']);
        }

        $imageData = VPImage::GetImageData($imageID);

        { // main edit panel
            echo '<span id="vp_delete_redirect" style="display: none;">' . VPConfig::$VP_REDIRECT_URL . '/photos/delete/' . $imageID . '</span>';
            echo '<form method="post" id="vp_imgForm"><div class="vp_edit_box"><div class="vp_edit_box_header">';
            echo VPLocale::Get("photos.photo-data") . '<span style="float: right"><span class="vp_delete_button" onClick="vp_imgDelete()">' . VPLocale::Get("general.delete") . '</span>';
            echo '<span class="vp_save_button" onClick="vp_imgSave()">' . VPLocale::Get("general.save") . '</span></span>';
            echo '</div><div class="vp_edit_box_body"><table>';

            echo '<tr><td>' . VPLocale::Get("photos.attribs.path") . '</td><td><input type="text" value="' . $imageData->File . '" disabled style="width: 350px;"/>';
            echo '<span style="margin-left: 5px; font-size: 0.8em;">' . VPPageUtil::FormatByteSize(filesize($imageData->File)) . '</span></td></tr>';

            echo '<tr><td>' . VPLocale::Get("photos.attribs.preview-path") . '</td><td><input type="text" value="' . $imageData->PreviewFile . '" disabled style="width: 350px;"/>';
            echo '<span style="margin-left: 5px; font-size: 0.8em;">' . VPPageUtil::FormatByteSize(filesize($imageData->PreviewFile)) . '</span></td></tr>';

            echo '<tr><td>' . VPLocale::Get("photos.attribs.uploaded-date") . '</td><td><input type="text" value="' . $imageData->TimestampFormatted . '" disabled/></td></tr>';
            echo '<tr><td>' . VPLocale::Get("photos.attribs.uploaded-by") . '</td><td><input type="text" value="' . VPUserData::GetUserName($imageData->Owner) . '" disabled/></td></tr>';
            if ($imageData->GalleryID != '') {
                require_once __DIR__ . '/../vp_galleries.php';
                $gallery = VPGallery::GetGalleryData($imageData->GalleryID);
                $galleryName = $gallery->Title;
                echo '<tr><td>' . VPLocale::Get("photos.attribs.gallery") . '</td><td><input type="text" value="' . $galleryName . '" disabled/></td></tr>';
            }
            echo '<tr><td>' . VPLocale::Get("photos.attribs.title") . '</td><td><input type="text" onInput="vp_imgInput()" name="img_title" value="' . $imageData->Title . '"/><span style="margin-left: 10px; font-size: 10px;">' . VPLocale::Get("photos.edit-notice.max-chars", array("200")) . '</span></td></tr>';

            require_once 'vp_pageUtil.php';
            echo '<tr><td>' . VPLocale::Get("photos.attribs.photograph") . '</td><td>';
            VPPageUtil::CreateUserDropdown("img_photograph", true, $imageData->Photographer, "vp_input_photograph");
            echo '<span style="margin-left: 10px; font-size: 12px;">' . VPLocale::Get("photos.edit-notice.photograph") . '</span></td></tr>';

            echo '<tr><td style="vertical-align: top;">' . VPLocale::Get("photos.attribs.info") . '</td><td><textarea id="vp_img_info" type="text" name="img_info" rows="8" cols="50">' . $imageData->Info . '</textarea><br><span style="font-size: 10px;" id="img_info_charLimit"></span></td></tr>';

            echo '</table></div></div></form>';
        }

        { // rotate and flip
            echo '<div class="vp_edit_box"><div class="vp_edit_box_header">';
            echo VPLocale::Get("photos.transform.header");
            echo '</div><div class="vp_edit_box_body">';

            $buttons = array(
                array("icon" => "CounterClockwise.png", "text" => "rotate-ccw", "param" => "rotate-ccw"),
                array("icon" => "Clockwise.png", "text" => "rotate-cw", "param" => "rotate-cw"),
                array("icon" => "Horizontal.png", "text" => "flip-hor", "param" => "flip-hor"),
                array("icon" => "Vertical.png", "text" => "flip-ver", "param" => "flip-ver")
            );

            if (isset($_POST['img_transform']))
                echo '<span class="vp_search_result_count" style="float: right; margin-bottom: 4px; color: red; font-weight: bold;">' . VPLocale::Get("photos.transform.quality-warning") . '</span>';

            foreach ($buttons as $button) {
                echo '<form method="post" style="display: inline; margin-right: 4px;" id="form_' . $button['param'] . '"><button type="button" ';
                echo 'onclick="vp_img_transformButton(\'' . $button['param'] . '\')" ';
                echo 'title="' . htmlspecialchars(VPLocale::Get("photos.transform.action." . $button['text'])) . '">';
                echo '<img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/' . $button['icon'] . '" style="width: 18px; height: 18px;"/>';
                echo '<input type="hidden" value="' . $button['param'] . '" name="img_transform"/>';
                echo '</button></form>';
            }

            echo '</div></div>';
        }

        echo '<a href="/' . $imageData->File . '" target="_blank">';
        echo '<img style="margin: 0 0 1em 0; width: 800px; height: auto;" src="/' . $imageData->File . '"/></a>';

        { // used by...
            $textImpl = VPDatabaseConn::EscapeSQLString('@' . VPConfig::$VP_ARTICLE_INCLUDE_PHOTO_KEYWORD . ':' . $imageID);
            $sql = "SELECT `ID` FROM vp_articles WHERE `Thumbnail` = '$imageID' OR `Text` LIKE '%$textImpl%'";
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
            if (mysqli_num_rows($result) > 0) {
                echo '<div class="vp_edit_box"><div class="vp_edit_box_header">';
                echo VPLocale::Get("photos.used-by.header");
                echo '</div><div class="vp_edit_box_body">';

                echo '<table>';
                while ($row = mysqli_fetch_assoc($result)) {
                    $article = VPArticle::GetArticle($row['ID']);

                    echo '<tr><td><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/ArticleDark.png' . '" style="width: 1em; height: 1em;"/></trtd>';

                    if ($permissions->CanEditArticle($article->ID)) {
                        $articleURL = VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $article->ID;
                    } else if (VPConfig::$VP_WEBSITE_PAGE_ARTICLE != null) {
                        $articleURL = $article->HumanLink;
                    } else {
                        $articleURL = null;
                    }

                    echo '<td>';
                    if ($articleURL != null) echo '<a target="_blank" href="' . $articleURL . '">';
                    echo '<strong>' . $article->Title . '</strong>';
                    if ($articleURL != null) echo '</a>';
                    echo '</td></tr>';
                }
                echo '</table>';

                echo '</div></div>';
            }
        }

        echo '<span style="display: none;" id="vp_text_remaningChars">' . VPLocale::Get("photos.edit-notice.digit-state") . '</span>';
        echo '<span style="display: none;" id="vp_text_tooManyChars">' . VPLocale::Get("photos.edit-notice.too-many-chars") . '</span>';
        echo '<span style="display: none;" id="vp_text_deleteWarning">' . VPLocale::Get("photos.delete-warning") . '</span>';
        echo '<span style="display: none;" id="vp_text_unsavedChanges">' . VPLocale::Get("general.unsaved-changes") . '</span>';
        echo '<span style="display: none;" id="vp_text_saveFirst">' . VPLocale::Get("photos.transform.save-first") . '</span>';

        echo '<script type="text/javascript" src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_photoEdit.js"></script>';
    }

    private static function PerformTransformation($imageID, $transform)
    {
        $imageData = VPImage::GetImageData($imageID);

        do {
            $targetFile = VPPhotosPage::RandomString(8) . ".jpg";
            $targetPath = VPConfig::$VP_IMAGE_UPLOAD_DIR . '/' . $targetFile;
        } while (file_exists($targetPath));

        $success = self::PerformTransformationOnImage($imageData->File, $imageID, $transform, false, $targetFile);
        $success &= self::PerformTransformationOnImage($imageData->PreviewFile, $imageID, $transform, true, $targetFile);

        if ($success) echo '<p class="vp_info_box">' . VPLocale::Get('photos.transform.success') . '</p>';
    }

    private static function PerformTransformationOnImage($imageFile, $imageID, $transform, $preview, $targetFile)
    {
        $extension = substr($imageFile, strrpos($imageFile, '.') + 1);

        switch (strtolower($extension)) {
            case "jpeg":
                $imageData = imagecreatefromjpeg($imageFile);
                break;
            case "jpg":
                $imageData = imagecreatefromjpeg($imageFile);
                break;
            case "png":
                $imageData = imagecreatefrompng($imageFile);
                break;
            case "gif":
                $imageData = imagecreatefromgif($imageFile);
                break;
            case "bmp":
                $imageData = imagecreatefrombmp($imageFile);
                break;
            default:
                echo '<p style="color: red; font-weight: bold;">Failed to transform image: Unknown extension \'' . $extension . '\'</p>';
                return false;
        }

        list($width, $height) = getimagesize($imageFile);
        $destImage = null;

        switch ($transform) {
            case 'rotate-ccw':
                $destImage = imagerotate($imageData, 90, 0);
                break;
                break;
            case 'rotate-cw':
                $destImage = imagerotate($imageData, -90, 0);
                break;
            case 'flip-hor':
                imageflip($imageData, IMG_FLIP_HORIZONTAL);
                $destImage = $imageData;
                $imageData = null;
                break;
            case 'flip-ver':
                imageflip($imageData, IMG_FLIP_VERTICAL);
                $destImage = $imageData;
                $imageData = null;
                break;
        }

        if ($destImage != null) {
            $directory = ($preview ? VPConfig::$VP_IMAGE_UPLOAD_DIR_PREVIEW : VPConfig::$VP_IMAGE_UPLOAD_DIR);
            if (!is_dir($directory)) {
                mkdir($directory);
            }
            $targetFile = $directory . '/' . $targetFile;

            $quality = $preview ? VPConfig::$VP_IMAGE_UPLOAD_QUALITY_PREVIEW : VPConfig::$VP_IMAGE_UPLOAD_QUALITY;
            $quality += 5;
            if ($quality > 100) $quality = 100;

            imagejpeg($destImage, $targetFile, $quality);
            imagedestroy($destImage);

            unlink($imageFile);

            $sql = "UPDATE vp_images SET `" . ($preview ? "PreviewFile" : "File") . "` = '$targetFile' WHERE `ID` = '" . $imageID . "'";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);

            VPLogger::GetLogger()->LogUserActivity("\"" . $imageFile . '\" --[' . $transform . ']--> \"' . $targetFile . '\"');
        }

        if ($imageData != null)
            imagedestroy($imageData);

        return $destImage != null;
    }

    private static function ShowOverviewPage($path, $permissions)
    {
        $forArticle = null;
        if (isset($_GET['for']) && isset($_GET['articleID']) && $_GET['for'] == "article") {
            $forArticle = $_GET['articleID'];
            $forArticleData = VPArticle::GetArticle($forArticle);
            if (!$forArticleData->WasArticleFound()) {
                $forArticle == null;
            }
        }

        if ($permissions->CanUploadPhotos() && $forArticle == null) {
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/photos/upload"><button class="vp_dashboard_button"><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Upload.png"/>' . VPLocale::Get("photos.upload") . '</button></a>';
        }

        $dateFrom = isset($_GET['since']) ? $_GET['since'] : "";
        $imageCount = isset($_GET['count']) ? $_GET['count'] : "";
        $textSearch = isset($_GET['search']) ? $_GET['search'] : "";

        if ($dateFrom . $imageCount . $textSearch == "") {
            if (isset($_SESSION['vp_img_search'])) {
                $searchInfo = $_SESSION['vp_img_search'];
                $spl = explode(";", $searchInfo);
                switch ($spl[0]) {
                    case "since":
                        $dateFrom = $spl[1];
                        break;
                    case "count":
                        $imageCount = $spl[1];
                        break;
                    case "search":
                        $textSearch = $spl[1];
                        break;
                }
            } else {
                // No user specified search
                $imageCount = "20";
            }
        }

        if ($forArticle != null) {
            echo '<h1>' . VPLocale::Get("photos.header.select-thumbnail", array($forArticleData->Title)) . '</h1>';
            echo '<a href="' . VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $forArticle . '">' . VPLocale::Get("photos.back-article") . '</a>';
            $forArticleButtons = '<input hidden style="display:none" name="for" value="article"/><input hidden style="display:none" name="articleID" value="' . $forArticle . '"/>';
        } else {
            echo '<h1>' . VPLocale::Get("photos.header.default") . '</h1>';
            $forArticleButtons = "";
        }

        if (sizeof($path) >= 2 && $path[0] == "delete") {
            $imageID = $path[1];
            self::DeleteImageFromDB($imageID);
        }

        $formAction = VPConfig::$VP_REDIRECT_URL . '/photos';
        echo '<div class="vp_search_panel"><table><tr>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($dateFrom) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                    ' . VPLocale::Get("photos.search.since", array('<input type="date" name="since" value="' . $dateFrom . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>' . $forArticleButtons . '
                    </form></td>';
        echo '  <td style="width: 34%;"><form method="get"' . (strlen($imageCount) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                    ' . VPLocale::Get("photos.search.newest", array('<input type="number" name="count" style="width: 50px;" value="' . ($imageCount == 0 ? "10" : $imageCount) . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.show") . '</button>' . $forArticleButtons . '
                    </form></td>';
        echo '  <td style="width: 33%;"><form method="get"' . (strlen($textSearch) > 0 ? ' style="border: 3px dashed red; padding: 6px;"' : '') . ' action="' . $formAction . '">
                    ' . VPLocale::Get("photos.search.text", array('<input type="text" name="search" value="' . $textSearch . '" required>')) . '<br>
                        <button type="submit">' . VPLocale::Get("general.search") . '</button>' . $forArticleButtons . '
                    </form></td>';
        echo '</tr></table></div>';

        if ($dateFrom != "") {
            $dateFrom = VPDatabaseConn::EscapeSQLString($dateFrom);
            $sql = "SELECT * FROM `vp_images` WHERE `GalleryID` = '' AND `Timestamp` IS NOT NULL AND `Timestamp` > '$dateFrom' ORDER BY `Timestamp` DESC LIMIT 100";
            $searchInfo = "since;$dateFrom";
        } else if ($imageCount != "") {
            if ($imageCount < 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.negative") . '</h2>';
                return;
            }
            $imageCount = VPDatabaseConn::EscapeSQLString($imageCount);
            $sql = "SELECT * FROM `vp_images` WHERE `GalleryID` = '' ORDER BY `Timestamp` DESC LIMIT $imageCount";
            $searchInfo = "count;$imageCount";
        } else if ($textSearch != "") {
            $textSearch = trim($textSearch);
            if (strlen($textSearch) == 0) {
                echo '<h2 style="color: red;">' . VPLocale::Get("search.empty") . '</h2>';
                return;
            }
            $textSearch = VPDatabaseConn::EscapeSQLString($textSearch);
            $sql = "SELECT * FROM `vp_images` WHERE `GalleryID` = '' AND `Title` LIKE '%$textSearch%' OR `Info` LIKE '%$textSearch%' ORDER BY `Timestamp` DESC LIMIT 100";
            $searchInfo = "search;$textSearch";
        } else {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.null") . '</h2>';
            return;
        }

        $_SESSION['vp_img_search'] = $searchInfo;

        $timer = new VPTimer();
        $timer->Start();
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
        $searchTime = $timer->Stop();

        $resultCount = mysqli_num_rows($result);
        if ($resultCount == 0) {
            echo '<h2 style="color: red;">' . VPLocale::Get("search.no-results") . '</h2>';
        } else {
            echo '<p class="vp_search_result_count">' . VPLocale::Get($resultCount == 1 ? "search.result-count-one" : "search.result-count", [mysqli_num_rows($result), $searchTime]) . '</p>';

            echo '<table><tr>';
            $index = 0;
            while ($imageRow = mysqli_fetch_assoc($result)) {
                if ($index++ >= 3) {
                    echo '</tr><tr>';
                    $index = 1;
                }

                $imageData = VPImage::GetImageDataFromRow($imageRow);
                $canEdit = $permissions->CanEditPhoto($imageData->ID);

                echo '<td><div class="vp_photo_list_table_cell">';

                if ($forArticle == null) {
                    if ($canEdit) echo '<a style="text-decoration: none;" href="' . VPConfig::$VP_REDIRECT_URL . '/photos/edit/' . $imageData->ID . '">';
                } else {
                    echo '<a style="text-decoration: none;" href="' . VPConfig::$VP_REDIRECT_URL . '/articles/edit/' . $forArticle . '?thumbnailID=' . $imageData->ID . '">';
                }

                echo '<div class="vp_photo_list_box" style="background-image: url(/' . htmlspecialchars($imageData->PreviewFile) . ')">';
                echo '</div><div class="vp_photo_list_box_text">';
                echo $imageData->Title . '<br>' . $imageData->TimestampFormatted;
                if ($forArticle == null) {
                    if ($canEdit) {
                        echo '<br><span style="text-decoration: underline;">' . VPLocale::Get("general.edit") . '</span>';
                    } else {
                        echo '<br><span style="font-size: 11px;">' . VPLocale::Get("photos.cant-edit") . '</span>';
                    }
                } else {
                    echo '<br><span style="text-decoration: underline;">' . VPLocale::Get("general.select") . '</span>';
                }
                echo '</div>';
                if ($canEdit) echo '</a>';
                echo '</div></td>';
            }
            echo '</tr></table>';
        }
    }

    public static function DeleteImageFromDB($imageID, $printInfoMessage = true)
    {
        $imageInfo = VPImage::GetImageData($imageID);
        if (file_exists($imageInfo->File)) unlink($imageInfo->File);
        if (file_exists($imageInfo->PreviewFile)) unlink($imageInfo->PreviewFile);
        $deleteSQL = "DELETE FROM `vp_images` WHERE `ID` = '$imageID'";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery($deleteSQL);
        if ($printInfoMessage)
            echo '<p class="vp_info_box">' . VPLocale::Get("photos.deleted", [$imageInfo->Title]) . '</p>';
        VPLogger::GetLogger()->LogUserActivity("deleted image {ID=$imageID}");
    }

    private static function ProcessImageUpload()
    {
        try {
            $fileInfo = pathinfo($_FILES['file']['name']);

            $extension = "";
            if (isset($fileInfo['extension'])) {
                $extension = "." . $fileInfo['extension'];
            } else {
                VPLogger::GetLogger()->LogUserActivity("image upload error: unknown image file extension: " . $extension);
                die("Unknown image file extension: " . $extension);
            }

            if (!is_dir(VPConfig::$VP_IMAGE_UPLOAD_DIR)) {
                mkdir(VPConfig::$VP_IMAGE_UPLOAD_DIR);
            }
            if (!is_dir(VPConfig::$VP_IMAGE_UPLOAD_DIR_PREVIEW)) {
                mkdir(VPConfig::$VP_IMAGE_UPLOAD_DIR_PREVIEW);
            }

            do {
                $targetFile = VPPhotosPage::RandomString(8) . ".jpg";
                $targetPath = VPConfig::$VP_IMAGE_UPLOAD_DIR . '/' . $targetFile;
                $targetPreviewPath = VPConfig::$VP_IMAGE_UPLOAD_DIR_PREVIEW . '/' . $targetFile;
            } while (file_exists($targetPath));

            if (!move_uploaded_file($_FILES['file']['tmp_name'], $targetPath)) {
                VPLogger::GetLogger()->LogUserActivity("image upload error: failed to copy image [" . $_FILES['file']['tmp_name'] . "] file to " . $targetPath);
                VPLogger::GetLogger()->LogUserActivity("maybe the file size limit for POST is < 20 MB?");
                die("failed to copy image file to " . $targetPath);
            }

            switch (strtolower(substr($extension, 1))) {
                case "jpeg":
                    $srcImage = imagecreatefromjpeg($targetPath);
                    break;
                case "jpg":
                    $srcImage = imagecreatefromjpeg($targetPath);
                    break;
                case "png":
                    $srcImage = imagecreatefrompng($targetPath);
                    break;
                case "gif":
                    $srcImage = imagecreatefromgif($targetPath);
                    break;
                case "bmp":
                    $srcImage = imagecreatefrombmp($targetPath);
                    break;
                default:
                    VPLogger::GetLogger()->LogUserActivity("image upload error: Unsupported file type: " . $extension);
                    die("Unsupported file type: " . $extension);
                    break;
            }

            list($width, $height) = getimagesize($targetPath);

            VPPhotosPage::ResizeImage($srcImage, $targetPreviewPath, $width, $height, VPConfig::$VP_IMAGE_UPLOAD_WIDTH_PREVIEW, VPConfig::$VP_IMAGE_UPLOAD_QUALITY_PREVIEW);
            VPPhotosPage::ResizeImage($srcImage, $targetPath, $width, $height, VPConfig::$VP_IMAGE_UPLOAD_WIDTH, VPConfig::$VP_IMAGE_UPLOAD_QUALITY);
            imagedestroy($srcImage);

            if (isset($_GET['gallery'])) {
                $galleryID = VPDatabaseConn::EscapeSQLString($_GET['gallery']);

                $sql = "SELECT ID FROM vp_images WHERE GalleryID = '$galleryID'";
                $count = mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql));
                $galleryIndex = $count;
            } else {
                $galleryID = '';
                $galleryIndex = 0;
            }

            $targetPath = VPDatabaseConn::EscapeSQLString($targetPath);
            $targetPreviewPath = VPDatabaseConn::EscapeSQLString($targetPreviewPath);
            $currentUserID = VPLogin::LoggedInUserID();
            $imageSQL = "INSERT INTO `vp_images` (`ID`, `File`, `PreviewFile`, `Title`, `Info`, `Photographer`, `Owner`, `Timestamp`, `GalleryID`, `GalleryIndex`) VALUES (NULL, '$targetPath', '$targetPreviewPath', '', '', '$currentUserID', '$currentUserID', NOW(), '$galleryID', '$galleryIndex')";
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($imageSQL));

            $sql = "SELECT `ID` FROM `vp_images` ORDER BY `ID` DESC LIMIT 1";
            $newestID = mysqli_fetch_assoc(VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql))['ID'];
            VPLogger::GetLogger()->LogUserActivity("uploaded image {ID=$newestID Width=$width Height=$height File=$targetFile Gallery=$galleryID}");
        } catch (Exception $e) {
            VPLogger::GetLogger()->LogUserActivity("image upload error: PHP exception: " . $e->getMessage());
        }
    }

    private static function RandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private static function ResizeImage($srcImage, $destFile, $width, $height, $newwidth, $quality)
    {
        $ratio = $width / $height;
        $newheight = $newwidth / $ratio;

        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $srcImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($dst, $destFile, $quality);

        imagedestroy($dst);
    }

}