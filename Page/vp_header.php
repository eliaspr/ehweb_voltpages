<html lang="de">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?php echo VPConfig::$VP_INSTALL_DIR; ?>Page/<?php if(VPConfig::IsDarkModeEnabled()) echo 'vp_styleDark'; else echo 'vp_style';?>.css">
    <style>
        input[type=checkbox].vp_fancy_checkbox:checked + label.vp_fancy_checkbox_label {
            background: #ffffff url("<?php echo VPConfig::$VP_INSTALL_DIR . 'Image/checkbox.png'?>") center;
            background-size: contain;
        }

        .vp_login_form_container {
            margin: 0;
            height: 100%;
            background: <?php echo VPConfig::GetLoginPageBackground(); ?>;
            background-size: cover;
        }

        .vp_sidebar_button > div, .vp_sidebar_button_active > div {
            padding: <?php
                    $userID = VPLogin::LoggedInUserID();
                    $sql = "SELECT `SidebarSpacing` FROM vp_users WHERE `ID` = '$userID'";
                    $spacing = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['SidebarSpacing'];
                    echo $spacing;
                 ?>px;
        }

        .nav_bar_search > button {
            border: 0;
            padding: 0;
            margin: 0;
            background: url("<?php echo VPConfig::$VP_INSTALL_DIR . 'Image/Icon/Search.png'?>") center;
            background-size: contain;
            height: 20px;
            width: 20px;
            display: inline-block;
            line-height: 30px;
            vertical-align: middle;
        }
    </style>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo VPConfig::$VP_INSTALL_DIR; ?>Image/eliaspr.png">
    <title><?php echo VPConfig::$VP_HTML_TITLE; ?></title>
</head>

<body>
<!-- offers protection against the average user with disabled JS -->
<noscript><h1
            style="color: red; margin-top: 200px; margin-left: 200px;"><?php echo VPLocale::Get("enable-javascript") ?></h1>
</noscript>
<div id="vp_body" style="display: none;">
    <script>
        document.getElementById("vp_body").style.display = "block";
    </script>
