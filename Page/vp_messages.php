<?php

require_once 'vp_logger.php';

class VPMessagesPage
{

    private static $VP_MESSAGES_PER_PAGE = 25;

    public static function ShowMessagesPage($path)
    {
        $userID = VPLogin::LoggedInUserID();
        $permissions = VPPermissions::FromUserID($userID);

        if ($permissions->CanViewInbox()) {
            if (sizeof($path) > 0) {
                $page = $path[0];
                if ($page == "message") {
                    if (sizeof($path) > 1) {
                        $messageID = $path[1];
                        if (sizeof($path) > 2 && $path[2] == 'delete') {
                            if ($permissions->CanDeleteMessages()) {
                                self::DeleteMessage($messageID);
                            } else {
                                VPPermissions::NoPermissionMessage();
                            }
                        } else {
                            self::ShowViewMessagePage($messageID, $permissions);
                        }
                    } else {
                        header("Location: " . VPConfig::$VP_REDIRECT_URL . '/inbox');
                    }
                } else {
                    header("Location: " . VPConfig::$VP_REDIRECT_URL . '/inbox');
                }
            } else {
                VPMessagesPage::ShowOverviewPage($permissions);
            }
        } else {
            echo '<h2 style="color: red;">' . VPLocale::Get("messages.no-permission") . '</h2>';
        }
    }

    public static function GetUnreadMessageCount()
    {
        $sql = "SELECT * FROM `vp_messages` WHERE `Deleted` = '0' AND `Read` = '0'";
        return mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql));
    }

    private static function ShowOverviewPage($permissions)
    {
        echo '<h1>' . VPLocale::Get("messages.header") . '</h1>';
        echo '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_messenger.js"></script>';

        if (isset($_GET['deleted']) && $_GET['deleted'] == 'true')
            echo '<p class="vp_info_box">' . VPLocale::Get("messages.message-deleted") . '</p>';

        $userID = VPLogin::LoggedInUserID();

        $pageNr = isset($_GET['page']) ? $_GET['page'] : (isset($_SESSION['inboxPageID']) ? $_SESSION['inboxPageID'] : 1);
        if ($pageNr <= 0) $pageNr = 1;
        $offset = ($pageNr - 1) * self::$VP_MESSAGES_PER_PAGE;

        $sql = "SELECT * FROM `vp_messages` WHERE `Deleted` = '0' ORDER BY `Timestamp` DESC";

        $totalMessageCount = mysqli_num_rows(VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql));
        $sql .= " LIMIT " . self::$VP_MESSAGES_PER_PAGE . " OFFSET $offset";
        $maxPageCount = ceil($totalMessageCount / self::$VP_MESSAGES_PER_PAGE);

        echo '<span style="display: none;" id="vp_messageViewRedirect">' . VPConfig::$VP_REDIRECT_URL . '/inbox/message/</span>';
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        $messageCount = mysqli_num_rows($result);

        if ($messageCount == 0) {
            if ($pageNr <= 1) {
                echo '<p><em>' . VPLocale::Get("messages.no-results") . '</em></p>';
            } else {
                echo '<p><em>' . VPLocale::Get("messages.no-results") . '&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-size: 13px;" href="?page=1">' . VPLocale::Get("messages.back-to-page-one") . '</a></em></p>';
            }
        } else {
            echo '<p style="font-size: 12px; margin-top: 60px;">' . VPLocale::Get("messages.page-index", [$pageNr, $maxPageCount]) . '</p>';
            echo '<table class="vp_fancy_table" style="width: 100%">';
            while ($row = mysqli_fetch_assoc($result)) {
                $message = VPMessage::GetMessageFromRow($row);

                echo '<tr class="vp_message_row" onclick="vp_openMessagePage(' . $message->ID . ')"' . ($message->Read ? '' : ' style="font-weight: bold;"') . '">';

                echo '<td><img style="width: 13px; height: auto;" src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/MailDark.png"/></td>';
                echo '<td><em>' . $message->Sender['Name'];
                $senderMail = htmlspecialchars($message->Sender['Mail']);
                echo '<span style="margin-left: 7px; font-size: 11px;"><a href="mailto:' . $senderMail . '" target="_blank">' . $senderMail . '</a></span>';
                echo '</em></td>';

                echo '<td>' . $message->Subject . '</td>';

                $previewMsg = htmlspecialchars(str_replace(PHP_EOL, ' ', $message->Message));
                if (strlen($previewMsg) > 100) $previewMsg = substr($previewMsg, 0, 100) . '...';
                echo '<td style="font-size: 12px; width: 100%;"><em>' . $previewMsg . '</em></td>';

                echo '<td style="font-size: 12px; text-align: right;">' . $message->TimestampFormatted . '</td>';
            }
            echo '</table>';

            echo '<p style="font-size: 13px; margin-top: 50px;">';
            // echo self::$VP_MESSAGES_PER_PAGE.' Nachrichten pro Seite&nbsp;&nbsp;|&nbsp;&nbsp;';
            echo VPLocale::Get("messages.showing-messages", [$offset + 1, min(($offset + self::$VP_MESSAGES_PER_PAGE), $messageCount + ($pageNr - 1) * self::$VP_MESSAGES_PER_PAGE), $totalMessageCount]);
            echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
            for ($i = 1; $i <= $maxPageCount; $i++) {
                if ($i == $pageNr) {
                    echo '<strong>' . $i . '</strong>&nbsp;&nbsp;';
                } else {
                    echo '<a href="?page=' . $i . '">' . $i . '</a>&nbsp;&nbsp;';
                }
            }
        }

        $_SESSION['inboxPageID'] = $pageNr;
    }

    private static function ShowViewMessagePage($messageID, VPPermissions $permissions)
    {
        if (isset($_GET['from']) && $_GET['from'] == 'dashboard')
            echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/dashboard">' . VPLocale::Get("general.back") . '</a></p>';
        else
            echo '<p><a href="' . VPConfig::$VP_REDIRECT_URL . '/inbox">' . VPLocale::Get("messages.back-inbox") . '</a></p>';

        $messageData = VPMessage::GetMessage($messageID);
        if (!$messageData->WasMessageFound()) {
            echo '<p style="color: red;"><strong>' . VPLocale::Get("messages.not-found") . '</strong></p>';
            return;
        }
        if (!$messageData->Read) {
            VPLogger::GetLogger()->LogUserActivity("read message {MessageID=$messageData->ID,MessageSubject=\"$messageData->Subject\",SenderName=\"" . $messageData->Sender['Name'] . "\"}");
        }
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_messages SET `Read` = 1 WHERE ID = $messageID");

        echo '<div class="vp_edit_box"><div class="vp_edit_box_header"><table><tr>';
        echo '<td><img src="' . VPConfig::$VP_INSTALL_DIR . 'Image/Icon/MailDark.png" style="width: 18px; height: auto; margin-right: 5px;"/></td>';
        echo '<td style="width: 100%;">' . $messageData->Subject . '<span style="float: right;">';
        if ($permissions->CanDeleteMessages()) {
            echo '<script src="' . VPConfig::$VP_INSTALL_DIR . 'Scripts/vp_messenger.js"></script>';
            echo '<p style="display: none;"><span id="vp_confirmMsgDelete">' . VPLocale::Get("messages.confirm-delete") . '</span>';
            echo '<span id="vp_redirectMsgDelete">' . VPConfig::$VP_REDIRECT_URL . '/inbox/message/' . $messageID . '/delete</span></p>';
            echo '<span class="vp_delete_button" onclick="vp_deleteMessageButton()">' . VPLocale::Get("messages.delete") . '</span>';
        }
        echo '<a href="mailto:' . $messageData->Sender['Mail'] . '" target="_blank" style="text-decoration: none;"><span class="vp_answer_button">' . VPLocale::Get("messages.answer-contact") . '</span></a>';
        echo '</span></td>';
        echo '</tr></table></div><div class="vp_edit_box_body">';
        echo '<div style="font-size: 11px; color: #444444;">' . $messageData->TimestampFormatted;
        echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $messageData->Sender['Name'];
        echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $messageData->Sender['Mail'];
        echo '<span style="float: right; color: red; font-weight: bold;">' . VPLocale::Get("messages.contact-form-notice") . '</span>';
        echo '</div>';
        echo '<p>' . str_replace(PHP_EOL, '<br>', $messageData->Message) . '</p>';
        echo '</div></div>';
        echo '<div style="height: 20px;"></div>';
    }

    private static function DeleteMessage($messageID)
    {
        $sql = "UPDATE vp_messages SET `Deleted` = '1' WHERE ID = '$messageID'";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        header("Location: " . VPConfig::$VP_REDIRECT_URL . "/inbox?deleted=true");
        exit;
    }

}