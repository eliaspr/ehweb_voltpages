<?php

class VPMessage
{

    private $MessageFound;

    public $ID;
    public $Timestamp;
    public $TimestampFormatted;
    public $Subject;
    public $Message;
    public $Read;

    // associative array, indices depend on message type:
    //  type = private: ID
    //  type = contact: Name, Mail
    public $Sender;

    private function LoadFromDatabase($messageID)
    {
        $messageID = VPDatabaseConn::EscapeSQLString($messageID);
        $sql = "SELECT * FROM `vp_messages` WHERE `ID` = '$messageID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->MessageFound = false;
        } else {
            $this->MessageFound = true;
            $messageRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($messageRow);
        }
    }

    private function LoadFromRow($messageRow)
    {
        $this->ID = $messageRow['ID'];
        $this->Subject = utf8_encode($messageRow['Subject']);
        $this->Message = utf8_encode($messageRow['Message']);
        $this->Read = $messageRow['Read'] == '1';

        $this->Sender = [
            'Name' => utf8_encode($messageRow['SenderName']),
            'Mail' => utf8_encode($messageRow['SenderEMail'])
        ];

        $this->Timestamp = $messageRow['Timestamp'];
        $time = strtotime($this->Timestamp);
        $this->TimestampFormatted = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $time));
    }

    public function WasMessageFound()
    {
        return $this->MessageFound;
    }

    public static function GetMessage($messageID): VPMessage
    {
        $msg = new VPMessage();
        $msg->LoadFromDatabase($messageID);
        return $msg;
    }

    public static function GetMessageFromRow($messageRow): VPMessage
    {
        $msg = new VPMessage();
        $msg->LoadFromRow($messageRow);
        return $msg;
    }

    public static function SendMessageFromContactForm($senderName, $senderMail, $subject, $message)
    {
        $senderName = VPDatabaseConn::EscapeSQLString($senderName);
        $senderMail = VPDatabaseConn::EscapeSQLString($senderMail);
        $subject = VPDatabaseConn::EscapeSQLString($subject);
        $message = VPDatabaseConn::EscapeSQLString($message);

        $sql = "INSERT INTO vp_messages (`ID`, `SenderName`, `SenderEMail`, `Timestamp`, `Subject`, `Message`, `Read`, `Deleted`) 
                      VALUES (NULL, '$senderName', '$senderMail', NOW(), '$subject', '$message', '0', '0')";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery(utf8_decode($sql));
    }

}