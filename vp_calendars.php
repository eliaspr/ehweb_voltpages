<?php

class VPCalendarDate
{

    private $DateFound;

    public $ID;
    public $CalendarID;
    public $Name;
    public $Info;
    public $Timestamp;
    public $DateFormatted;
    public $DatetimeFormatted;

    private function LoadFromDatabase($dateID)
    {
        $dateID = VPDatabaseConn::EscapeSQLString($dateID);
        $sql = "SELECT * FROM vp_dates WHERE `ID` = '$dateID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->DateFound = false;
        } else {
            $this->DateFound = true;
            $dateRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($dateRow);
        }
    }

    private function LoadFromRow($dateRow)
    {
        $this->ID = $dateRow['ID'];
        $this->CalendarID = $dateRow['CalendarID'];
        $this->Name = utf8_encode($dateRow['Name']);
        $this->Timestamp = $dateRow['Date'];
        $this->Info = utf8_encode($dateRow['Info']);

        $this->Timestamp = strtotime($dateRow['Date']);
        $this->DateFormatted = trim(strftime(VPConfig::$VP_DATE_FORMAT, $this->Timestamp));
        $this->DatetimeFormatted = trim(strftime(VPConfig::$VP_DATETIME_FORMAT, $this->Timestamp));
    }

    public function WasDateFound(): bool
    {
        return $this->DateFound;
    }

    public static function GetDate($dateID): VPCalendarDate
    {
        $date = new VPCalendarDate();
        $date->LoadFromDatabase($dateID);
        return $date;
    }

    public static function GetDateFromRow($dateRow): VPCalendarDate
    {
        $date = new VPCalendarDate();
        $date->LoadFromRow($dateRow);
        return $date;
    }

}

class VPCalendar
{

    private $CalendarFound;

    public $ID;
    public $Name;
    public $OwnerID;

    private function LoadFromDatabase($calendarID)
    {
        $calendarID = VPDatabaseConn::EscapeSQLString($calendarID);
        $sql = "SELECT * FROM vp_calendars WHERE `ID` = '$calendarID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->CalendarFound = false;
        } else {
            $this->CalendarFound = true;
            $calendarRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($calendarRow);
        }
    }

    private function LoadFromRow($calendarRow)
    {
        $this->ID = $calendarRow['ID'];
        $this->Name = utf8_encode($calendarRow['Name']);
        $this->OwnerID = $calendarRow['OwnerID'];
    }

    public function WasCalendarFound(): bool
    {
        return $this->CalendarFound;
    }

    public static function GetCalendar($calendarID): VPCalendar
    {
        $calendar = new VPCalendar();
        $calendar->LoadFromDatabase($calendarID);
        return $calendar;
    }

    public static function GetCalendarFromRow($calendarRow): VPCalendar
    {
        $calendar = new VPCalendar();
        $calendar->LoadFromRow($calendarRow);
        return $calendar;
    }

    public static function GetCalendarName($calendarID): string
    {
        $calendarID = VPDatabaseConn::EscapeSQLString($calendarID);
        $sql = "SELECT `Name` FROM vp_calendars WHERE ID = '$calendarID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            return null;
        } else {
            return utf8_encode(mysqli_fetch_assoc($result)['Name']);
        }
    }

    public function GetUpcomingDates($count): array
    {
        $dates = [];

        $sql = "SELECT * FROM vp_dates WHERE `CalendarID` = '$this->ID' AND `Date` > NOW() ORDER BY `Date` ASC LIMIT $count";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result))
            $dates[] = VPCalendarDate::GetDateFromRow($row);

        return $dates;
    }

    public function GetPastDates($count): array
    {
        $dates = [];

        $sql = "SELECT * FROM vp_dates WHERE `CalendarID` = '$this->ID' AND `Date` < NOW() ORDER BY `Date` DESC LIMIT $count";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result))
            $dates[] = VPCalendarDate::GetDateFromRow($row);

        return $dates;
    }

    public function GetDates($timestampSince, $timestampTo, $sort = "DESC"): array
    {
        $timestampSince = VPDatabaseConn::EscapeSQLString($timestampSince);
        $timestampTo = VPDatabaseConn::EscapeSQLString($timestampTo);

        $dates = [];

        $sql = "SELECT * FROM vp_dates WHERE `CalendarID` = '$this->ID' AND `Date` > '$timestampSince' AND `Date` < '$timestampTo' ORDER BY `Date` $sort";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result))
            $dates[] = VPCalendarDate::GetDateFromRow($row);

        return $dates;
    }

    public function GetAllDates(): array
    {
        $dates = [];

        $sql = "SELECT * FROM vp_dates WHERE `CalendarID` = '$this->ID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        while ($row = mysqli_fetch_assoc($result))
            $dates[] = VPCalendarDate::GetDateFromRow($row);

        return $dates;
    }

    public function GetDateCount()
    {
        $sql = "SELECT `ID` FROM vp_dates WHERE `CalendarID` = '$this->ID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        return mysqli_num_rows($result);
    }

}