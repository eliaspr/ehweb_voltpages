<?php

require_once 'Dependencies/linguer.php';

class VPConfig
{

    static $VP_DATABASE_SERVER = "127.0.0.1";
    static $VP_DATABASE_USER = "root";
    static $VP_DATABASE_PASS = "";
    static $VP_DATABASE_NAME = "voltpages";

    // see strftime()
    static $VP_DATE_FORMAT = "%e. %B %Y";
    static $VP_DATETIME_FORMAT = "%e. %B %Y %H:%M:%S";

    static $VP_PUBLIC_WEBSITE_URL = "http://127.0.0.1";
    static $VP_ENABLE_BETTER_ARTICLE_LINKS = true;
    static $VP_INSTALL_DIR = "/VoltPages/";
    static $VP_HTML_TITLE = "VoltPages";
    static $VP_REDIRECT_URL = "/admin";
    static $VP_USER_ICON = "";
    static $VP_DEFAULT_LANGUAGE = "English";
    static $VP_DASHBOARD_PHP = null;

    static $VP_LOGIN_PAGE_BACKGROUND = null;
    static $VP_LOGIN_PAGE_BG_CREDIT = "";

    static $VP_IMAGE_UPLOAD_DIR = "Image/Uploads";
    static $VP_IMAGE_UPLOAD_QUALITY = 80;
    static $VP_IMAGE_UPLOAD_WIDTH = 2500;

    static $VP_IMAGE_UPLOAD_DIR_PREVIEW = "Image/Uploads/Preview";
    static $VP_IMAGE_UPLOAD_QUALITY_PREVIEW = 60;
    static $VP_IMAGE_UPLOAD_WIDTH_PREVIEW = 500;

    static $VP_ENABLE_LOGGING = true;
    static $VP_LOG_DIRECTORY = 'Log/';
    static $VP_LOGGER_SAVE_IP = false;

    static $VP_ARTICLE_INCLUDE_PHOTO_KEYWORD = "photo";
    static $VP_ARTICLE_INCLUDE_GALLERY_KEYWORD = "gallery";

    public static function GetLogDirectory()
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/' . self::$VP_LOG_DIRECTORY;
    }

    public static function GetLoginPageBackground()
    {
        return self::$VP_LOGIN_PAGE_BACKGROUND == null ? ('url(' . self::$VP_INSTALL_DIR . 'Image/background.jpg) no-repeat center;') : self::$VP_LOGIN_PAGE_BACKGROUND;
    }

    public static function IsDarkModeEnabled(): bool
    {
        if(VPLogin::IsUserLoggedIn()) {
            $userID = VPLogin::LoggedInUserID();
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `DarkMode` FROM `vp_users` WHERE `ID` = '$userID'");
            if ($result !== false) {
                $darkMode = $result['DarkMode'];
                return $darkMode == '1';
            }
        } else {
            return false;
        }
    }

    static $VP_WEBSITE_PAGE_ARTICLE = null; // e.g. /articles/{id}
    static $VP_WEBSITE_PAGE_PHOTO = null; // e.g. /photos/{id}
    static $VP_WEBSITE_PAGE_GALLERY = null; // e.g. /galleries/{id}

}

class VPLocale
{

    private static $instance;
    private $language, $languageName;

    public static function GetLocale()
    {
        return VPLocale::$instance != null ? VPLocale::$instance : VPLocale::$instance = new VPLocale;
    }

    private function __construct()
    {
        $this->languageName = VPConfig::$VP_DEFAULT_LANGUAGE;

        if (VPLogin::IsUserLoggedIn()) {
            $userID = VPLogin::LoggedInUserID();
            $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch("SELECT `PreferredLanguage` FROM `vp_users` WHERE `ID` = '$userID'");
            if ($result !== false) {
                $preferred = $result['PreferredLanguage'];
                if (strlen($preferred) > 0)
                    $this->languageName = $preferred;
            }
        }

        $filename = $this->languageName . '.json';
        $filepath = __DIR__ . '/Locale/' . $filename;
        $this->language = LinguerPHP\Language::LoadLanguage($filepath);
    }

    public function GetLanguageName()
    {
        return $this->languageName;
    }

    public function GetValue($path, $replacements = array())
    {
        return $this->language->GetValue($path, $replacements);
    }

    public static function Get($path, $replacements = array())
    {
        return VPLocale::GetLocale()->GetValue($path, $replacements);
    }

    public static function GetAvailableLanguages()
    {
        return array("Deutsch", "English");
    }

}