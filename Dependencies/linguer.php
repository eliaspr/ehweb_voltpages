<?php

    namespace LinguerPHP;

    class Language {

        private $name, $root;

        public static function LoadLanguage($file) {
            $fileContents = \file_get_contents($file);
            $json = json_decode($fileContents, true);
            
            $result = new Language;
            $result->name = $json['name'];

            $result->root = new Node(Node::$TYPE_GROUP, "root", array());
            Language::ParseNode($json['root'], $result->root);
            
            return $result;
        }

        private static function ParseNode($json, $parentNode) {
            foreach($json as $key => $value) {
                if(is_array($value)) {
                    $groupNode = new Node(Node::$TYPE_GROUP, $key, array());
                    $parentNode->AddChild($groupNode);
                    Language::ParseNode($value, $groupNode);
                } else {
                    $entryNode = new Node(Node::$TYPE_ENTRY, $key, $value);
                    $parentNode->AddChild($entryNode);
                }
            }
        }

        public function GetName() {
            return $this->name;
        }

        public function GetValue($path, $replacements = array()) {
            $elements = explode(".", $path);
            $container = $this->root;

            for($i = 0; $i < sizeof($elements) - 1; $i++) {
                $element = $elements[$i];
                $child = $container->GetChild($element);
                if($child == null || $child->GetType() != Node::$TYPE_GROUP) {
                    return $path;
                } else {
                    $container = $child;
                }
            }

            $lastElement = $elements[sizeof($elements) - 1];
            $child = $container->GetChild($lastElement);

            if($child != null && $child->GetType() == Node::$TYPE_ENTRY) {
                $result = $child->GetValue();
                for($i = 0; $i < sizeof($replacements); $i++) {
                    $result = \str_replace('{'.$i.'}', $replacements[$i], $result);
                }
                return $result;
            } else {
                return $path;
            }
        }

    }

    class Node {

        public static $TYPE_ENTRY = 1;
        public static $TYPE_GROUP = 2;

        public function __construct($type, $key, $value) {
            $this->type = $type;
            $this->key = $key;
            $this->value = $value;
        }

        private $type;
        private $key;
        private $value;

        public function GetKey() {
            return $this->key;
        }

        public function GetValue() {
            if($this->type == Node::$TYPE_GROUP) {
                return "group[".sizeof($this->value).']';
            } else {
                return $this->value;
            }
        }

        public function GetType() {
            return $this->type;
        }

        public function AddChild($node) {
            if($this->type == Node::$TYPE_GROUP) {
                $this->value[] = $node;
            }
        }

        public function GetChildCount() {
            if($this->type == Node::$TYPE_GROUP) {
                return sizeof($this->value);
            } else {
                return 1;
            }
        }

        public function GetChild($key) {
            if($this->type == Node::$TYPE_GROUP) {
                foreach($this->value as $child) {
                    if($child->key == $key) return $child;
                }
            }
            return null;
        }

        public function GetChildByIndex($index) {
            if($this->type == Node::$TYPE_GROUP) {
                if($index >= 0 && $index < sizeof($this->value))
                    return $this->value[$index];
            }
            return null;
        }

    }

?>