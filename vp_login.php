<?php

require_once "vp_config.php";

class VPUserData
{

    private $UserFound;

    public $ID;
    public $Name;
    public $EMail;
    public $Telephone;
    public $LoginID;
    public $PasswordHash;
    public $Permissions;
    public $ResetPassword;

    private function __construct()
    {
    }

    private function LoadFromRow($userRow)
    {
        $this->ID = $userRow['ID'];
        $this->Name = utf8_encode($userRow['Name']);
        $this->EMail = utf8_encode($userRow['E-Mail']);
        $this->Telephone = utf8_encode($userRow['Telephone']);
        $this->LoginID = utf8_encode($userRow['Login']);
        $this->PasswordHash = $userRow['Password'];
        $this->Permissions = utf8_encode($userRow['Permissions']);
        $this->ResetPassword = $userRow['ResetPW'] != 0;
    }

    private function LoadFromDatabase($userID, $idField)
    {
        $userID = VPDatabaseConn::EscapeSQLString($userID);
        $sql = "SELECT * FROM `vp_users` WHERE `$idField` = '$userID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
        if (mysqli_num_rows($result) == 0) {
            $this->UserFound = false;
        } else {
            $this->UserFound = true;
            $userRow = mysqli_fetch_assoc($result);
            $this->LoadFromRow($userRow);
        }
    }

    public function WasUserFound()
    {
        return $this->UserFound;
    }

    public static function GetUserData($userID)
    {
        $data = new VPUserData;
        $data->LoadFromDatabase($userID, "ID");
        return $data;
    }

    public static function GetUserDataFromLoginID($loginID)
    {
        $data = new VPUserData;
        $data->LoadFromDatabase($loginID, "Login");
        return $data;
    }

    public static function GetUserDataFromRow($userRow)
    {
        $data = new VPUserData;
        $data->LoadFromRow($userRow);
        return $data;
    }

    public static function GetUserName($userID)
    {
        $userID = VPDatabaseConn::EscapeSQLString($userID);
        $sql = "SELECT `Name` FROM `vp_users` WHERE `ID` = '$userID'";
        $result = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql);
        if ($result !== false) {
            return utf8_encode($result['Name']);
        } else {
            return null;
        }
    }

}

class VPLogin
{

    public static function IsUserLoggedIn()
    {
        return isset($_SESSION['vp_LoggedIn']) && $_SESSION['vp_LoggedIn'] === true;
    }

    public static function IsUserLoggedInCheckTimeout()
    {
        return isset($_SESSION['vp_LoggedIn']) && $_SESSION['vp_LoggedIn'] === true && time() <= $_SESSION['vp_Timeout'];
    }

    public static function LoggedInUserID()
    {
        return VPLogin::IsUserLoggedIn() ? $_SESSION['vp_User'] : null;
    }

    public static function AttemptLogin()
    {
        require_once 'Page/vp_logger.php';

        if (!isset($_POST['vp_UserID']) || !isset($_POST['vp_Password'])) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL);
            exit;
        }

        $userName = $_POST['vp_UserID'];
        $password = hash("sha512", $_POST['vp_Password']);

        if (strlen($userName) == 0) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/login/?error=unknown_user");
            VPLogger::GetLogger()->LogUserActivity("login > unknown user id");
            exit;
        }

        $userData = VPUserData::GetUserDataFromLoginID($userName);
        if (!$userData->WasUserFound()) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/login/?error=unknown_user&userID=$userName");
            VPLogger::GetLogger()->LogUserActivity("login > unknown user id");
            exit;
        }

        if ($userData->PasswordHash === $password) {
            $_SESSION['vp_LoggedIn'] = true;
            $_SESSION['vp_User'] = $userData->ID;
            $_SESSION['vp_Timeout'] = time() + 30 * 60;
            $_SESSION['vp_lastTime'] = time();

            $sql = "SELECT TimesLoggedIn FROM vp_users WHERE ID = " . $userData->ID;
            $newCount = VPDatabaseConn::GetDatabaseConnection()->PerformAndFetch($sql)['TimesLoggedIn'] + 1;
            VPDatabaseConn::GetDatabaseConnection()->PerformQuery("UPDATE vp_users SET TimesLoggedIn = $newCount WHERE ID = " . $userData->ID);

            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/dashboard/");
            VPLogger::GetLogger()->LogUserActivity("login > success");
            exit;
        } else {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/login/?error=wrong_password&userID=$userName");
            VPLogger::GetLogger()->LogUserActivity("login > wrong password");
            exit;
        }
    }

    public static function AttemptLogout($redirect = true)
    {
        require_once 'Page/vp_logger.php';
        VPLogger::GetLogger()->LogUserActivity("logged out");

        $_SESSION['vp_LoggedIn'] = false;
        $_SESSION['vp_User'] = null;
        $_SESSION['vp_Timeout'] = 0;
        unset($_SESSION['vp_lastTime']);

        if ($redirect) {
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/login");
            exit;
        }
    }

    public static function CheckTimeout()
    {
        if (!VPLogin::IsUserLoggedIn()) {
            VPLogin::AttemptLogout(false);
            header("Location: " . VPConfig::$VP_REDIRECT_URL);
            exit;
        }

        $now = time();
        if ($now > $_SESSION['vp_Timeout']) {
            VPLogin::AttemptLogout(false);
            header("Location: " . VPConfig::$VP_REDIRECT_URL . "/login/?error=login_expired");
            exit;
        } else {
            $_SESSION['vp_Timeout'] = $now + 30 * 60;
        }
    }

    public static function AttemptPasswordChange($userID, $newPassword)
    {
        $passwordHash = hash("sha512", $newPassword);
        $sql = "UPDATE `vp_users` SET `Password` = '$passwordHash', `ResetPW` = '0' WHERE `ID` = '$userID'";
        VPDatabaseConn::GetDatabaseConnection()->PerformQuery($sql);
    }

}